Unless explicitly stated, any piece of code under this repository 
is licensed under the Apache License 2.0. The full text of the
license can be found in the file LICENSE.apache.txt

Some portions of the code are licensed under different but compatible
licenses, specifically:

- `listanalchem/tools/extreme_currents_copasi.py` is licensed under the
  Artistic License 1.0
