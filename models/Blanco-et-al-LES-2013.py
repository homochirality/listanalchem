# Blanco, C., Ribó, J. M., Crusats, J., El-Hachemi, Z., Moyano, A., & Hochberg, D. (2013). Mirror symmetry breaking with limited enantioselective autocatalysis and temperature gradients: A stability survey. Physical Chemistry Chemical Physics, 15(5), 1546–1556. https://doi.org/10.1039/c2cp43488a
# Run with: python -m listanalchem --model models/Blanco-et-al-LES-2013.py > outputs/Blanco-et-al-LES-2013.out
modelname = 'Blanco-et-al-LES-2013'
species = ['L', 'D', 'A']
reactions = [
    '    A <-> L    ', #  0 & 1
    '    A <-> D    ', #  2 & 3
    'A + L <-> 2L   ', #  4 & 5
    'A + D <-> 2D   ', #  6 & 7
    'A + L <-> L + D', #  8 & 9
    'A + D <-> D + L', # 10 & 11  
]

dual_pairs = [(0,2),(1,3),(4,6),(5,7),(4,8),(4,10),(5,9),(5,11)] # Autocatalysis = LES. In this case SMSB is NOT possible.
# dual_pairs = [(0,2),(1,3),(4,6),(5,7),(8,10),(9,11)]           # Autocatalysis != LES. In this case SMSB is possible.

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # "characteristic-polynomial", "trace-determinant",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 2, # 5: Se demora pero lo hace.
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blanco-et-al-LES-2013-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blanco-et-al-LES-2013-5A', # None, #
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blanco-et-al-LES-2013-6A',
    }
}
