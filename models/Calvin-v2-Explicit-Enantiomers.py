modelname='Calvin-v2-Explicit-Enantiomers'
species = ["L-B", "D-B", "L-A", "D-A"]
reactions = [
    "  -> L-A",
    " L-A ->",
    "  -> D-A",
    " D-A ->",
    " L-A -> L-B",
    " L-B -> L-A",
    " D-A -> D-B",
    " D-B -> D-A",
    " L-A + L-B  -> 2L-B",
    " 2L-B -> L-A + L-B",
    " D-A + D-B  -> 2D-B ",
    " 2D-B -> D-A + D-B",
    ]
dual_pairs = [(0,2),(1, 3), (4, 6), (5, 7), (8, 10),(9, 11)]
