# Clarke model from reference Bruce Clarke 1988 Stoichiometric Network Analysis Cell Biophysics 12 237 - 253
# Protocolo 1 - Ruben. Only X y Y. ## La matriz estequiométrica resultante tiene intercambiadas las columnas E3 y E5 respecto de la que pone Ruben en el protocolo 1. Esto implica que el signo del determinante cambiará.
modelname='Clarke model 1988 - Example - Only X and Y'
species = ['X', 'Y']
reactions = [
    ' -> X',
    ' -> Y',
    'Y <-> X',
    'X ->  ',
    'Y ->  ',
]
