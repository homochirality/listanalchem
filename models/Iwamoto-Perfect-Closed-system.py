# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Perfect-Closed-system.py > Iwamoto-Perfect-Closed.out

modelname = 'Iwamoto perfect in closed system'
species = ["L", "D", "E-L", "E-D", "A", "P", "Z-L", "Z-D", "Q"]
reactions = [
    "P <-> A",         # 0 & 1.
    "A + L <-> 2L",    # 2 & 3.
    "A + D <-> 2D",    # 4 & 5.
    "L + E-L <-> Z-L", # 6 & 7.
    "D + E-D <-> Z-D", # 8 & 9.
    "Z-L <-> E-L + Q", # 10 & 11.
    "Z-D <-> E-D + Q"  # 12 & 13.
]
dual_pairs = [(2, 4), (3, 5), (6, 8), (7, 9), (10, 12), (11,13)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-SNA-Closed',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3), (6, 7)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-pq-Closed',
    }
}
