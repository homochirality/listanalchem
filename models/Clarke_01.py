# Clarke model from reference Bruce Clarke 1988 Stoichiometric Network Analysis Cell Biophysics 12 237 - 253
# Protocolo 1 - Ruben. All species. As usual it does not work.
modelname='Clarke model 1988 - Example - All species'
species = ['A', 'X', 'Y', 'B']
reactions = [
    'A -> X',
    'A -> Y',
    'Y <-> X',
    'X -> B ',
    'Y -> B ',
]
