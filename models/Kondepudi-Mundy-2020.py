# Kondepudi, D., & Mundy, Z. (2020). Spontaneous chiral symmetry breaking and entropy production in a closed system. Symmetry, 12(5), 769. https://doi.org/10.3390/SYM12050769
# Run with: python -m listanalchem --model models/Kondepudi-Mundy-2020.py > Kondepudi-Mundy-2020.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem, to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Mundy-2020.py > outputs/Kondepudi-Mundy-2020.out

modelname = 'Kondepudi - Mundy, 2020'
species   = ['XL', 'XD', 'S', 'Pi', 'TE', 'T', 'P', 'W']
#reactions = [
#    ' T + Pi <-> TE'   , #  0 &  1
#    '      T <-> TE'   , #  2 &  3
#    ' S + TE <-> XL  ' , #  4 &  5 * These constants must be equal to the product of **
#    ' S + TE <-> XD  ' , #  6 &  7
#    ' S + XL <-> SL  ' , #  8 &  9 ** k8*k10=k4, k9*k11=k5 and the rest of them according to reference.
#    'SL + TE <-> 2XL'  , # 10 & 11 **
#    ' S + XD <-> SD  ' , # 12 & 13
#    'SD + TE <-> 2XD'  , # 14 & 15
#    'XL + XD <-> P + W', # 16 & 17
#    '      P <-> 2S'   , # 18 & 19
#    '      W <-> 2T'   , # 20 & 21
#]
#dual_pairs = [(4,6),(5,7),(8,12),(9,13),(10,14),(11,15)]
reactions = [
    '       T + Pi <-> TE'   , #  0 &  1
    '            T <-> TE'   , #  2 &  3
#    '  S + TE <-> XL' , # * These constants must be equal to the product of ** 
#    '  S + XL <-> SL' , # **  
#    ' SL + TE <-> 2XL', # ** 
    '2S + 2TE + XL <-> 3XL'  , # 4 & 5
#    '  S + TE <-> XD' , #  
#    '  S + XD <-> SD' , # 
#    ' SD + TE <-> 2XD', # 
    '2S + 2TE + XD <-> 3XD'  , # 6 & 7 ***
    '      XL + XD <-> P + W', # 8 & 9
    '            P <-> 2S'   , # 10 & 11
    '            W <-> 2T'   , # 12 & 13
#    '         -> Pi'     # 14
]
dual_pairs = [(4,6),(5,7)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", #"characteristic-polynomial",# or:  "trace-determinant", 
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 4,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Mundy-2020-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Mundy-2020-5A', # or None
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Mundy-2020-6A',
    }
}
