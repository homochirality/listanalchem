modelname = 'Calvin - pseudoquiral'
species = ['L1', 'D1', 'L2', 'D2']
reactions = [
    'L1 + L2 -> 2L2',  # 0
    'D1 + D2 -> 2D2',  # 1
    '2L2 -> L1 + L2',  # 2
    '2D2 -> D1 + D2',  # 3
    'L1 -> D1',  # 4
    'D1 -> L1',  # 5
    'L1 -> L2',  # 6
    'D1 -> D2',  # 7
    'L2 -> L1',  # 8
    'D2 -> D1',  # 9
]

dual_pairs = [(0, 1), (2, 3), (6, 7), (8, 9)]
