# Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin-2.py > outputs/Calvin-2.out
modelname='Calvin version 2'
species = ["L-B", "D-B", "L-A", "D-A", "NR3", "Allyl-X"]
reactions = [
    "NR3 + Allyl-X <-> L-A",  #  0 & 1.
    "NR3 + Allyl-X <-> D-A",  #  2 & 3.
    "          L-A <-> L-B",  #  4 & 5.
    "          D-A <-> D-B",  #  6 & 7.
    "    L-A + L-B <-> 2L-B", #  8 & 9.
    "    D-A + D-B <-> 2D-B"  # 10 & 11.
    ]
dual_pairs = [(0,2),(1, 3), (4, 6), (5, 7), (8, 10),(9, 11)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2', #None
        "samples-for-proportion": 10000
    },    
    "frank-pseudoquiral"  : {   ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2',
    }
}
