# Blackmond, D. G. (2020). Autocatalytic Models for the Origin of Biological Homochirality. Chemical Reviews, 120(11), 4831–4847. https://doi.org/10.1021/acs.chemrev.9b00557
# Run with: python -m listanalchem --model models/Blackmond-Rev-2020.py > Blackmond-Rev-2020.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem, to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Blackmond-Rev-2020.py > outputs/Blackmond-Rev-2020.out

modelname = 'Blackmond-Rev-2020 model'
species   = ['R', 'S', 'RR', 'SS', 'SR', 'A', 'Z']
reactions = [
    'A + Z      <-> R     ', #  0 &  1
    'A + Z      <-> S     ', #  2 &  3
    'A + Z + R  <-> R + R ', #  4 &  5 # Monomer model autocatalysis.
    'A + Z + S  <-> S + S ', #  6 &  7 # Monomer model autocatalysis.
    '     R + R <-> RR    ', #  8 &  9  # Added.
    '     S + S <-> SS    ', # 10 & 11  # Added.
    '     R + S <-> SR    ', # 12 & 13  # Added.
    'A + Z + RR <-> R + RR', # 14 & 15 # Dimer model autocatalysis.
    'A + Z + SS <-> S + SS', # 16 & 17 # Dimer model autocatalysis.
    'A + Z + SR <-> R + SR', # 18 & 19
    'A + Z + SR <-> S + SR', # 20 & 21
]
dual_pairs = [(0,2),(1,3),(4,6),(5,7),(8,10),(9,11),(14,16),(15,17),(18,20),(19,21)] #(8,12),(9,13),(14,18),(14,20),(15,19),(15,21).# Si se incluyen estas = Estable.

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blackmond-Rev-2020-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : False
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blackmond-Rev-2020-5A', # or None
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1),(2,3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Blackmond-Rev-2020-6A',
    }
}
