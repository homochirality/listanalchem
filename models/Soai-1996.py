modelname='Soai-1996+Inhibition'
species = ["S-P", "R-P","S-Zn-A", "R-Zn-A"]
reactions = [
    " -> S-Zn-A ",
    " S-Zn-A ->  ",
    " -> R-Zn-A ",
    " R-Zn-A -> ",
    " S-P -> S-Zn-A ",
    " S-Zn-A -> S-P ",
    " R-P -> R-Zn-A ",
    " R-Zn-A -> R-P ",
    " S-Zn-A + S-P -> 2 S-P ",
    " 2 S-P -> S-Zn-A + S-P ",
    " R-Zn-A + R-P <-> 2 R-P ",
    " 2 R-P -> R-Zn-A + R-P ",
    ]
dual_pairs = [(0,2),(1,3),(4,6),(5,7),(8,10),(9,11)]
