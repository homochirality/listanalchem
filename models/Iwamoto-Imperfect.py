# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Imperfect.py > outputs/Iwamoto-Imperfect.out

modelname = 'Iwamoto imperfect'
species = ["L", "D", "E-L", "E-D", "A"]
reactions = [
    "        <-> A",     # 0 & 1.   # P is constant.
    "  A + L <-> 2L",    # 2 & 3.
    "  A + L <-> L + D", # 4 & 5.   # Added - Imperfect.
    "  A + D <-> 2D",    # 6 & 7.
    "  A + D <-> D + L", # 8 & 9.   # Added - Imperfect.
    "L + E-L <-> ",      # 10 & 11. # Z-L is constant. Imply [L] and [E-L] entrance and output.
    "L + E-D <-> ",      # 12 & 13. # Added - Imperfect.
    "D + E-D <-> ",      # 14 & 15. # Z-D is constant. Imply [D] and [E-D] entrance and output.
    "D + E-L <-> ",      # 16 & 17. # Added - Imperfect.
    "         -> E-L",   # 18.      # Z-L is constant. Imply [E-L] entrance. Q is irrelevant.
    "         -> E-D"    # 19.      # Z-D is constant. Imply [E-D] entrance. Q is inert.
]
dual_pairs = [(2,6),(4,8),(3,7),(5,9),(10,12),(14,16),(11,13),(15,17),(18,19),(12,14),(13,15),(4,6),(5,7)] # 

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-Imperfect-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-perfect-5A', # None, #
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-Imperfect-6A',
    }
}
