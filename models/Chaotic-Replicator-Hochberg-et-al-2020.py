# Hochberg, D., Sánchez Torralba, A., & Morán, F. (2020). Chaotic oscillations, dissipation and mirror symmetry breaking in a chiral catalytic network. Physical Chemistry Chemical Physics, 22(46), 27214–27223. https://doi.org/10.1039/d0cp05109h
# Run with: python -m listanalchem --model models/Chaotic-Replicator-Hochberg-et-al-2020.py > Chaotic-Replicator-Hochberg-et-al-2020.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem, to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Chaotic-Replicator-Hochberg-et-al-2020.py > outputs/Chaotic-Replicator-Hochberg-et-al-2020.out

modelname = 'Chaotic replicator Hochberg, Torralba, Morán 2020.'
species   = ['L1', 'D1','L2', 'D2','L3', 'D3','L4', 'D4','S']
reactions = [
    '            -> S  ',      # 0
    '     S + L1 -> 2L1',      # 1
    '     S + D1 -> 2D1',      # 2
    '     S + L2 -> 2L2',      # 3
    '     S + D2 -> 2D2',      # 4
    '     S + L3 -> 2L3',      # 5
    '     S + D3 -> 2D3',      # 6
    '     S + L4 -> 2L4',      # 7
    '     S + D4 -> 2D4',      # 8
    'S + L1 + L1 -> 2L1 + L1', # 9
    'S + L1 + L2 -> 2L1 + L2', # 10
    'S + L1 + L3 -> 2L1 + L3', # 11
    'S + L1 + L4 -> 2L1 + L4', # 12
    'S + L2 + L1 -> 2L2 + L1', # 13
    'S + L2 + L2 -> 2L2 + L2', # 14
    'S + L2 + L3 -> 2L2 + L3', # 15
    'S + L2 + L4 -> 2L2 + L4', # 16
    'S + L3 + L1 -> 2L3 + L1', # 17
    'S + L3 + L2 -> 2L3 + L2', # 18
    'S + L3 + L3 -> 2L3 + L3', # 19
    'S + L3 + L4 -> 2L3 + L4', # 20
    'S + L4 + L1 -> 2L4 + L1', # 21
    'S + L4 + L2 -> 2L4 + L2', # 22
    'S + L4 + L3 -> 2L4 + L3', # 23
    'S + L4 + L4 -> 2L4 + L4', # 24
    'S + D1 + D1 -> 2D1 + D1', # 25 # Start D group.
    'S + D1 + D2 -> 2D1 + D2', # 26
    'S + D1 + D3 -> 2D1 + D3', # 27
    'S + D1 + D4 -> 2D1 + D4', # 28
    'S + D2 + D1 -> 2D2 + D1', # 29
    'S + D2 + D2 -> 2D2 + D2', # 30
    'S + D2 + D3 -> 2D2 + D3', # 31
    'S + D2 + D4 -> 2D2 + D4', # 32
    'S + D3 + D1 -> 2D3 + D1', # 33
    'S + D3 + D2 -> 2D3 + D2', # 34
    'S + D3 + D3 -> 2D3 + D3', # 35
    'S + D3 + D4 -> 2D3 + D4', # 36
    'S + D4 + D1 -> 2D4 + D1', # 37
    'S + D4 + D2 -> 2D4 + D2', # 38
    'S + D4 + D3 -> 2D4 + D3', # 39
    'S + D4 + D4 -> 2D4 + D4', # 40
    '          S -> ',         # 41
    '         L1 -> ',         # 42
    '         D1 -> ',         # 43
    '         L2 -> ',         # 44
    '         D2 -> ',         # 45
    '         L3 -> ',         # 46
    '         D3 -> ',         # 47
    '         L4 -> ',         # 48
    '         D4 -> ',         # 49
]
dual_pairs = [(1,2),(3,4),(5,6),(7,8),(25,26),(25,27),(25,28),(25,29),(25,30),(25,31),(25,32),(25,33),(25,9),(10,26),(11,27),(12,28),(13,29),(14,30),(15,31),(16,32),(17,33),(18,34),(19,35),(20,36),(21,37),(22,38),(23,39),(24,40),(41,42),(41,43),(41,44),(41,45),(41,46),(41,47),(41,48),(41,49)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 2,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Chaotic-Replicator-Hochberg-et-al-2020-SNA',	
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : False
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Chaotic-Replicator-Hochberg-et-al-2020-5A', # or None
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1),(2, 3),(4, 5),(6, 7)],
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Chaotic-Replicator-Hochberg-et-al-2020-6A',
    }
}
