# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Imperfect-a.py > Iwamoto-Imperfect-a.out

modelname = 'Iwamoto-Imperfect a' # a de alterno.
species = ["L", "D", "E-L", "E-D", "A"]
reactions = [
    " <-> A",           # 0 & 1.
    "A + L <-> 2L",     # 2 & 3.
    "A + L <-> L + D",  # 4 & 5.
    "A + D <-> 2D",     # 6 & 7.
    "A + D <-> D + L",  # 8 & 9.
    "L + E-L <-> ",     # 10 & 11.
    "D + E-D <-> ",     # 12 & 13.
    "D + E-L <-> ",     # 14 & 15.
    "L + E-D <-> ",     # 16 & 17.
    " -> E-L",          # 18.
    " -> E-D"           # 19.
]
dual_pairs = [(2,6),(4,8),(3,7),(5,9),(10,16),(12,14),(11,17),(13,15),(18,19)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-Imperfect-SNA-a',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-Imperfect-pq-a',
    }
}
