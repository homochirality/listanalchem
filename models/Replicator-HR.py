# Hochberg, D., & Ribó, J. (2019). Entropic Analysis of Mirror Symmetry Breaking in Chiral Hypercycles. Life, 9(1), 28. https://doi.org/10.3390/life9010028
# Run with: python -m listanalchem --model models/Replicator-HR.py > outputs/Replicator-HR.out

modelname = 'Replicator Hochberg-Ribo'
species = ['R1D', 'R1L', 'R2D', 'R2L', 'A']
reactions = [
    'A + R1D + R2D <-> 2 R1D + R2D', # 0 & 1
    'A + R1L + R2L <-> 2 R1L + R2L', # 2 & 3
    'A + R2D + R1D <-> 2 R2D + R1D', # 4 & 5
    'A + R2L + R1L <-> 2 R2L + R1L', # 6 & 7
    '          R1D  ->',             # 8
    '          R2D  ->',             # 9
    '          R1L  ->',             # 10
    '          R2L  ->',             # 11
    '               -> A',           # 12
    '            A  ->'              # 13
]
dual_pairs = [(0, 2), (1, 3), (0, 6), (1, 7),(0,4),(1,5), (8, 9), (8, 10), (8, 11),(8,13)] # (2,4)=(0,4), (3,5)=(1,5), etc.

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Replicator-SNA-Equals',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Replicator-3A',
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True,
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Replicator-5A', # or None
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Replicator-6A',
    }
}
