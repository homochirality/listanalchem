# Frank, F. C. (1953) Biochim. Biophys. Acta, 11, 459–463.
# Frank model modified by the inclusion of the reverse reactions and added with an explicit product and two precursors in an open system, a CSTR reactor.
# Run with: python -m listanalchem --model models/Frank-Rev-P-AB-CSTR.py > Frank-Rev-P-AB-CSTR.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Frank-Rev-P-AB-CSTR.py > outputs/Frank-Rev-P-AB-CSTR.out

modelname = 'Frank reversible model plus explicit product and two precursors in a CSTR' #  Independent steps
species   = ['L', 'D', 'A', 'B', 'P']
reactions = [
    '       -> A ',    #  0     # kf0 = kf12*[A]0
    '       -> B ',    #  1     # kf1 = kf12*[B]0
    'A + B <-> L ',    #  2 & 3
    'A + B <-> D ',    #  4 & 5
    'A + L <-> 2L',    #  6 & 7    #     L -> 2L
    'A + D <-> 2D',    #  8 & 9    #     D -> 2D
    'L + D <-> P ',    # 10 & 11   # L + D ->  
    '    A  ->   ',    # 12     # kf12 = flow / V
    '    B  ->   ',    # 13     # kf12 = flow / V
    '    L  ->   ',    # 14     # kf12 = flow / V
    '    D  ->   ',    # 15     # kf12 = flow / V
    '    P  ->   ',    # 16     # kf12 = flow / V
]
dual_pairs = [(2,4),(3,5),(6,8),(7,9),(12,13),(12,14),(12,15),(12,16)] # (12,13),(13,14),(14,15),(15,16) no funciona porque no está definido el último término. Entonces, se debe decir en la ayuda que para estos casos de CSTR se debe usar este formato. El formato (12,13),(13,14),(15,14),(16,15), que debería funcionar, tampoco funciona pero ya no sale el error k13 sini otro ... interesante pero debo seguir. Luego miro esos detalles de programación que por ahora no importan.
                                    
analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
	"num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True, # Add rows to the Stoichiometric Matrix encoding the dual pairs.
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-CSTR-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True, # Add rows to the Stoichiometric Matrix encoding the dual pairs.
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-CSTR-5A', # None,
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-CSTR-6A',
    }
}
