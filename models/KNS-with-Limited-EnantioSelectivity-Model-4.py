# Kondepudi-Nelson-Strecker-with-Limited-EnantioSelectivity-Model 4. Only the enantiomers.
# # KSIC-LES según Rubén #
# Run with: python -m listanalchem --model models/Kondepudi-Nelson-Strecker-with-Limited-EnantioSelectivity-Model-4.py > KNS-LES-Model-4.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Nelson-Strecker-with-Limited-EnantioSelectivity-Model-4.py > outputs/KNS-LES-Model-4.out

modelname = 'Kondepudi-Nelson-Strecker with Limited EnantioSelectivity: Model 4.'
species = ['L-CN', 'D-CN']
reactions = [
    '            <-> L-CN',        #  0 &  1
    '            <-> D-CN',        #  2 &  3
    'L-CN        <-> 2 L-CN',      #  4 &  5
    'D-CN        <-> 2 D-CN',      #  6 &  7
    'L-CN        <-> L-CN + D-CN', #  8 &  9
    'D-CN        <-> D-CN + L-CN', # 10 & 11
    'L-CN + D-CN  -> '             # 12 & 13
]

analyses = {
    "trace-determinant": {	######### First algorithm.  #########
        "enabled": True,
        "2by2-jacobian": True,
        "num-samples": 10,
        "plot": True
    },
    "sna": {			######### Second algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-LES-Model-4',
    },
    "six-categories": {		######### Third algorithm.  #########
        "enabled": True,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-LES-Model-4'
    },
    "frank-ineq-nonlinear": {	######### Fourth algorithm.  #########
        "enabled": False
    },
    "frank-ineq-linear": {	######### Fifth algorithm.  #########	
        "enabled": False,
        "dual-pairs-in-ec": True,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-LES-Model-4',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {	######### Six algorithm.  #########	
        "enabled": False,
        "enantiomeric-pairs": [(0, 1)],
        # "dual-pairs-in-ec": True,
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-LES-Model-4',
    }
}
