# Hochberg, D., & Ribó, J. (2019). Entropic Analysis of Mirror Symmetry Breaking in Chiral Hypercycles. Life, 9(1), 28. https://doi.org/10.3390/life9010028
# Run with: python -m listanalchem --model models/Replicator-HR-Equals.py > Replicator-HR-Equals.out

modelname = 'Replicator Hochberg-Ribo equals'
species = ['R1D', 'R1L', 'R2D', 'R2L', 'A']
reactions = [
    'A + R1D + R2D -> 2 R1D + R2D',  # 0
    '2 R1D + R2D -> A + R1D + R2D',  # 1
    'A + R1L + R2L -> 2 R1L + R2L',  # 2
    '2 R1L + R2L -> A + R1L + R2L',  # 3
    'A + R2D + R1D -> 2 R2D + R1D',  # 4
    '2 R2D + R1D -> A + R2D + R1D',  # 5
    'A + R2L + R1L -> 2 R2L + R1L',  # 6
    '2 R2L + R1L -> A + R2L + R1L',  # 7
    'R1D ->',  # 8
    'R2D ->',  # 9
    'R1L ->',  # 10
    'R2L ->',  # 11
    '-> A',    # 12
    'A ->'     # 13
]
dual_pairs = [(0, 2), (2, 4), (4, 6), (1, 3), (3, 5), (5, 7), (8, 9), (9, 10), (10, 11),(11,13)
]

analyses = {
    "trace-determinant": {
        "enabled": False,
        "2by2-jacobian": True
    },
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Replicator-SNA-Equals',
    },
    "six-categories": {
        "enabled": False,
        "num-samples": 10,
        "samples-folder": 'samples/Replicator-6c',
    },
    "frank-ineq-nonlinear": {
        "enabled": False
    },
    "frank-ineq-linear": {
        "enabled": False,
        "dual-pairs-in-ec": True,
        "num-samples": 1,
        "samples-folder": None,
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Replicator-pq-Equals',
    }
}
