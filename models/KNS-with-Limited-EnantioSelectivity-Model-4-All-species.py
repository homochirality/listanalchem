modelname = "Kondepudi-Nelson-Strecker with Limited EnantioSelectivity. Model 4: All species."
species = ['L-CN', 'D-CN', 'HCN', 'INH', 'ADD-CNL']
reactions = [
    'HCN + INH <-> L-CN',
    'HCN + INH <-> D-CN',
    'L-CN + HCN + INH <-> 2 L-CN',
    'D-CN + HCN + INH <-> 2 D-CN',
    'L-CN + HCN + INH <-> L-CN + D-CN',
    'D-CN + HCN + INH <-> D-CN + L-CN',
    'L-CN + D-CN -> ADD-CNL'
]
