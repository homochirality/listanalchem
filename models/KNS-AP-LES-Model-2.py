# KNS-AP-LES - Model 2.
# Kondepudi-Nelson-Strecker-Amino acids Production Limited Enantio Selectivity -
# Run with: python -m listanalchem --model models/Kondepudi-Nelson-Strecker-Amino-acids-Production-Limited-Enantio-Selectivity-KNS-AP-LES-Model-2.py > KNS-AP-LES-Model-2.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Nelson-Strecker-Amino-acids-Production-Limited-Enantio-Selectivity-KNS-AP-LES-Model-2.py > outputs/KNS-AP-LES-Model-2.out

modelname = 'KNS-AP-LES: Model 2.'
species = ['L-CN', 'D-CN']
reactions = [
    "     <-> L-CN",        #  0 & 1
    "     <-> D-CN",        #  2 & 3
    "L-CN <-> 2 L-CN",      #  4 & 5
    "D-CN <-> 2 D-CN",      #  6 & 7
    "D-CN <-> L-CN + D-CN", #  8 & 9
    "L-CN <-> D-CN + L-CN", # 10 & 11
    "L-CN  -> ",            # 12
    "D-CN  -> "             # 13
]

analyses = {
    "trace-determinant": {	######### First algorithm.  #########
        "enabled": True,
        "2by2-jacobian": True,
        "num-samples": 10,
        "plot": True
    },
    "sna": {			######### Second algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-AP-LES-Model-2',
    },
    "six-categories": {		######### Third algorithm.  #########
        "enabled": True,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-AP-LES-Model-2'
    },
    "frank-ineq-nonlinear": {	######### Fourth algorithm.  #########
        "enabled": False
    },
    "frank-ineq-linear": {	######### Fifth algorithm.  #########	
        "enabled": False,
        "dual-pairs-in-ec": True,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-AP-LES-Model-2',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {	######### Six algorithm.  #########	
        "enabled": False,
        "enantiomeric-pairs": [(0, 1)],
        # "dual-pairs-in-ec": True,
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/KNS-AP-LES-Model-2',
    }
}

