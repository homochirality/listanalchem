# Frank, F. C. (1953) Biochim. Biophys. Acta, 11, 459–463.
# Frank model modified by the inclusion of the reverse reactions and added with an explicit product and two precursors.
# Run with: python -m listanalchem --model models/Frank-Rev-P-AB.py > Frank-Rev-P-AB.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Frank-Rev-P-AB.py > outputs/Frank-Rev-P-AB.out

modelname = 'Frank reversible model plus explicit product and two precursors'
species   = ['L', 'D', 'A', 'B', 'P']
reactions = [
    'A + B <-> L ', # 0 & 1
    'A + B <-> D ', # 2 & 3
    'A + L <-> 2L', # 4 & 5 # Frank original # L     -> 2L
    'A + D <-> 2D', # 6 & 7                  # D     -> 2D
    'L + D <-> P '  # 8 & 9                  # L + D ->
]
# dual_pairs = [(0, 1)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-5A', #None,
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Frank-Rev-P-AB-6A',
    }
}
