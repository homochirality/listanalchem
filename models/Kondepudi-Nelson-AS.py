# Kondepudi, D. K., & Nelson, G. W. (1983). Chiral symmetry breaking in nonequilibrium systems. Physical Review Letters, 50(14), 1023–1026. https://doi.org/10.1103/PhysRevLett.50.1023. All species are explicit.
# Run with: python -m listanalchem --model models/Kondepudi-Nelson-AS.py > Kondepudi-Nelson-AS.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Nelson-AS.py > outputs/Kondepudi-Nelson-AS.out

modelname = 'Kondepudi-Nelson all species'
species = ['L', 'D', 'A', 'B', 'P']
reactions = [
    "A + B     <-> L",   # 0 & 1.
    "A + B     <-> D",   # 2 & 3.
    "A + B + L <-> 2 L", # 4 & 5.
    "A + B + D <-> 2 D", # 6 & 7.
    "L + D      -> P",   # 8.
]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/KN-AS-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/KN-AS-3A'
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########	
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/KN-AS-5A',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########	
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/KN-AS-6A',
    }
}
