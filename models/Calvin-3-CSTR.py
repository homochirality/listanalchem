# Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin-3-CSTR.py > outputs/Calvin-3-CSTR.out

modelname='Calvin version 3 in a CSTR'
species = ["L-B", "D-B", "L-A", "D-A", "L-dP", "D-dP"]
reactions = [
    '             -> L-A + D-A', # 0
    '             -> L-dP',      # 1
    '             -> D-dP',      # 2
    '        L-A <-> D-A',       # 3 & 4.
    ' L-A + L-dP <-> L-B',       # 5 & 6.
    ' D-A + D-dP <-> D-B',       # 7 & 8.
    ' L-dP + L-B <-> 2L-B',      # 9 & 10.
    ' D-dP + D-B <-> 2D-B ',     # 11 & 12.
    '      L-A    -> ',          # 13
    '      D-A    -> ',          # 14
    '      L-B    -> ',          # 15
    '      D-B    -> ',          # 16
    '      L-dP   -> ',          # 17
    '      D-dP   -> '           # 18
    ]
dual_pairs = [(1,2),(3,4),(5,7),(6,8),(9,11),(10,12),(13,14),(13,15),(13,16),(13,17),(13,18)] # (13,14),(14,15),(15,16),(16,17),(17,18)

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-3-CSTR',
    },
    "six-categories"      : {	######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-3-CSTR', #None
        "samples-for-proportion": 10000
    },    
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-3-CSTR',
    }
}
