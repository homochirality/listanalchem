# Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin-2-CSTR-LES-Chaos.py > outputs/Calvin-2-CSTR-LES-Chaos.out
modelname='Calvin version 2 in a CSTR with LES (limited enantioselectivity) under conditios to produce chaos'
species = ["L-B", "D-B", "L-A", "D-A", "NR3", "Allyl-X"]
reactions = [
    "               -> NR3",       #  0.
    "               -> Allyl-X",   #  1.
    "NR3 + Allyl-X <-> L-A",       #  2 & 3.
    "NR3 + Allyl-X <-> D-A",       #  4 & 5.
    "          L-A <-> L-B",       #  6 & 7.
    "          D-A <-> D-B",       #  8 & 9.
    "    L-A + L-B <-> 2L-B",      # 10 & 11.
    "    D-A + D-B <-> 2D-B",      # 12 & 13.
    '          L-A  -> ',          # 14
    '          D-A  -> ',          # 15
    '          L-B  -> ',          # 16
    '          D-B  -> ',          # 17
    '          NR3  -> ',          # 18
    '      Allyl-X  -> ',          # 19
    '    L-A + L-B <-> L-B + D-B', # 20 & 21
    '    D-A + D-B <-> L-B + D-B'  # 22 & 23
    ]
dual_pairs = [(2,4),(3,5),(6,8),(7,9),(10,12),(11,13),(14,15),(14,16),(14,17),(14,18),(14,19),(20,22),(21,23)] #  Must be equals (12,20) (13,21),
# (14,15),(15,16),(16,17),(17,18),(18,19) # This form to write the dual-pairs does not work!

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2-CSTR-LES-Chaos',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2-CSTR-LES-Chaos', #None
        "samples-for-proportion": 10000
    },    
    "frank-pseudoquiral"  : {   ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-2-CSTR-LES-Chaos',
    }
}
