modelname = 'Soai-1996-ajustado-I-1+inh'
species = ["S-P", "R-P", "S-Zn-A", "R-Zn-A"]
reactions = [
    ' -> S-Zn-A',
    ' -> R-Zn-A',
    'S-Zn-A -> ',
    'R-Zn-A -> ',
    'S-P -> S-Zn-A',
    'R-P -> R-Zn-A',
    'S-Zn-A -> S-P',
    'R-Zn-A -> R-P',
    'S-P + S-Zn-A -> 2 S-P',
    'R-P + R-Zn-A -> 2 R-P',
    '2 S-P -> S-P + S-Zn-A',
    '2 R-P -> R-P + R-Zn-A',
    'S-P + R-P -> ']
dual_pairs = [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11)]
