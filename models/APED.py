# Plasson, R., Bersini, H., & Commeyras, A. (2004). Recycling Frank: Spontaneous emergence of homochirality in noncatalytic systems. Proceedings of the National Academy of Sciences of the United States of America, 101(48), 16733–16738. https://doi.org/10.1073/pnas.0405293101
# Run with: python -m listanalchem --model models/APED.py > outputs/APED.out
modelname = 'APED'
species = ['L', 'D', 'La', 'Da', 'LL', 'DD', 'DL', 'LD']
reactions = [
    '     L <-> La',    # 0 & 1 - a & b
    '     D <-> Da',    # 2 & 3 - a & b
    'La + L  -> LL',    # 4  - p
    'Da + L  -> DL',    # 5  - alfap *     Si bifurcación.
    'La + D  -> LD',    # 6  - alfap *     Si bifurcación.
    'Da + D  -> DD',    # 7  - p     * 
    '    LL  -> L + L', # 8  - h     **    
    '    DL  -> L + D', # 9  - betah **
    '    LD  -> L + D', # 10 - betah **
    '    DD  -> D + D', # 11 - h     **   
    '    LD <-> DD',    # 12 & 13 - e & gammae ***  
    '    DL <-> LL'     # 14 & 15 - e & gammae ***  
]
# Según artículo.
dual_pairs = [(0,2),(1,3),(4,7),(5,6),(8,11),(9,10),(12,14),(13,15)] # Must be included ,(4,5),(8,9),(12,13)

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "characteristic-polynomial", # "mineurs", # or:  "trace-determinant",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 2, # 5: Se demora pero lo hace.
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-5A', # None, #
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3), (4, 5), (6, 7)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-6A',
    }
}
