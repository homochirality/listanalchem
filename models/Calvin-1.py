# Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin-1.py > outputs/Calvin-1.out
modelname = "Calvin version 1"
species = ['L-B', 'D-B', 'L-A', 'D-A']
reactions = [
    '      L-A <-> D-A',    # 0 & 1 #     S' <-> R'
    '      L-A <-> L-B',    # 2 & 3 #     S' <-> S
    '      D-A <-> D-B',    # 4 & 5 #     R' <-> R 
    'L-A + L-B <-> 2 L-B',  # 6 & 7 # S' + S <-> 2S
    'D-A + D-B <-> 2 D-B'   # 8 & 9 # R' + R <-> 2R
]
dual_pairs = [(0,1), (2, 4), (3, 5), (6, 8), (7, 9)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1', #None
        "samples-for-proportion": 10000
    },    
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1',
    }
}
