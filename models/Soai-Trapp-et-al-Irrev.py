# Saoi model by Trapp, Lamour, Maier, Siegle, Zawatzky and Straub. 2020.
# Run with: python3 -m listanalchem --model models/Soai-Trapp-et-al-Irrev.py > outputs/Soai-Trapp-et-al-Irrev.out
modelname = 'Soai-TLMSZS-2020'
species = ['R-1','S-1','R-2','S-2','RR-3','SS-3','RR-5','SS-5','RR-6','SS-6','RRR-7','SSS-7','RRRR-8','SSSS-8','ZnR2','RS-3d','RS-3s','Cuatro','SR-5','RS-5','RS-6','SR-6','RSS-7','SRR-7','RSSS-8',
'SRRR-8']
# Species nomenclature                                                Abreviation
# 1 (R)‐2‐(tert‐butylacetylene‐1‐yl)-pyrimidyl‐5‐(iso‐butan‐1‐ol) (1) R‐1
# 2 (S)‐2‐(tert‐butylacetylene‐1‐yl)-pyrimidyl‐5‐(iso‐butan‐1‐ol) (1) S‐1 
# 3 Diisopropylzinc (iPr2Zn)                                          ZnR2
# 4 (R)‐Zinc alcoholate                                           (2) R-2
# 5 (S)‐Zinc alcoholate                                           (2) S-2
# 6 (R,R)‐Homochiral dimeric zinc alcoholate                      (3) RR-3
# 7 (S,S)‐Homochiral dimeric zinc alcoholate                      (3) SS-3
# 8 (R,S)‐Heterochiral dimeric zinc alcoholate dissolved         d(3) RS-3d
# 9 (R,S)‐Heterochiral dimeric zinc alcoholate precipitated      s(3) RS-3s
#10 2-(ter-butylacetylene-1-yl)-pyrimidyl-5-carbaldehyde          (4) Cuatro
#11 (R,R)‐Major hemiacetal                                        (5) RR-5
#12 (S,S)‐Major hemiacetal                                        (5) SS-5
#13 (S,R)‐Minor hemiacetal - Epimerization.                       (5) SR-5
#14 (R,S)‐Minor hemiacetal - Epimerization.                       (5) RS-5
#15 (R,R)‐Major hemiacetal - Complex previous to trimer           (6) RR-6
#16 (S,S)‐Major hemiacetal - Complex previous to trimer           (6) SS-6
#17 (R,S)‐Minor hemiacetal - Epimer - Complex previous to trimer  (6) RS-6
#18 (S,R)‐Minor hemiacetal - Epimer - Complex previous to trimer  (6) SR-6
#19 (R,R,R)‐Major hemiacetal - Trimer                             (7) RRR-7
#20 (S,S,S)‐Major hemiacetal - Trimer                             (7) SSS-7
#21 (R,S,S)‐Minor hemiacetal - Trimer - Epimerization.            (7) RSS-7
#22 (S,R,R)‐Minor hemiacetal - Trimer - Epimerization.            (7) SRR-7
#23 (R,R,R,R)-Dimeric-Major-hemiacetal                            (8) RRRR-8
#24 (S,S,S,S)-Dimeric-Major-hemiacetal                            (8) SSSS-8
#25 (R,S,S,S)-Dimeric-Minor-hemiacetal - Epimerization            (8) RSSS-8
#26 (S,R,R,R)-Dimeric-Minor-hemiacetal - Epimerization            (8) SRRR-8

reactions = [
    'R-1 + ZnR2 -> R-2',       # 0
    'S-1 + ZnR2 -> S-2',       # 1
    'R-2 + R-2  <-> RR-3',      # 2 & 3.
    'S-2 + S-2  <-> SS-3',      # 4 & 5.
    'R-2 + S-2  <-> RS-3d',     # 6 & 7.
    'RS-3d      <-> RS-3s',     # 8 & 9.
    'R-2 + Cuatro     <-> RR-5',      # 10 & 11. RR-Hemiacetal.
    'S-2 + Cuatro     <-> SS-5',      # 12 & 13. SS-Hemiacetal.
    'S-2 + Cuatro     <-> SR-5',      # 14 & 15. Epimerization hemiacetal.
    'R-2 + Cuatro     <-> RS-5',      # 16 & 17. Epimerization hemiacetal.
    'RR-5 + Cuatro + ZnR2 -> RR-6', # 18
    'SS-5 + Cuatro + ZnR2 -> SS-6', # 19
    'SR-5 + Cuatro + ZnR2 -> SR-6', # 20. Epimerization hemiacetal.
    'RS-5 + Cuatro + ZnR2 -> RS-6', # 21. Epimerization hemiacetal.
    'RR-6           -> RRR-7', # 22
    'SS-6           -> SSS-7', # 23
    'RS-6           -> RSS-7', # 24
    'SR-6           -> SRR-7', # 25
    'RRR-7 + Cuatro     -> RRRR-8', # 26
    'SSS-7 + Cuatro     -> SSSS-8', # 27
    'RSS-7 + Cuatro     -> RSSS-8', # 28
    'SRR-7 + Cuatro     -> SRRR-8', # 29
    'RRRR-8      -> 2 RR-5',   # 30. Tretamer-Depolimerization.
    'SSSS-8      -> 2 SS-5',   # 31. Tretamer-Depolimerization.
    'RSSS-8   -> RS-5 + SS-5', # 32. Epimer - Tretamer-Depolimerization.
    'SRRR-8   -> SR-5 + RR-5'  # 33. Epimer - Tretamer-Depolimerization.
]
dual_pairs = [(0,1),(2,4),(3,5),(4,6),(5,7),(10,12),(11,13),(12,14),(13,15),(14,16),(15,17),(18,19),(19,20),(20,21),(22,23),(23,24),(24,25),(26,27),(27,28),(28,29),(30,31),(31,32),(32,33)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Trapp-SNA',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0,1),(2,3),(4,5),(6,7),(8,9),(10,11),(12,13)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Trapp-pseudoChiral',
    }
}
