# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Perfect-Without-A.py > Iwamoto-Perfect-Without-A.out

modelname = 'Iwamoto perfect without A' # A constant.
species = ["L", "D", "E-L", "E-D"]
reactions = [
    'L <-> 2 L',    # 0 & 1.
    'D <-> 2 D',    # 2 & 3.
    'L + E-L <-> ', # 4 & 5.
    'D + E-D <-> ', # 6 & 7.
    ' -> E-L',      # 8.
    ' -> E-D'       # 9.
]
dual_pairs = [(0, 2), (1, 3), (4, 6), (5, 7), (8, 9)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-Without-A-SNA',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-Without-A-pq',
    }
}
