# Trapp, O., Lamour, S., Maier, F., Siegle, A. F., Zawatzky, K., & Straub, B. F. (2020). In Situ Mass Spectrometric and Kinetic Investigations of Soai’s Asymmetric Autocatalysis. Chemistry - A European Journal, 26(68), 15871–15880. https://doi.org/10.1002/chem.202003260
# Run with: python -m listanalchem --model models/Soai-Trapp-et-al.py > outputs/Soai-Trapp-et-al.out
modelname = 'Soai-TLMSZS-2020 irreversible as the paper'
species = ['R-1','S-1','R-2','S-2','RR-3','SS-3','RR-5','SS-5','RR-6','SS-6','RRR-7','SSS-7','RRRR-8','SSSS-8','ZnR2','RS-3d','RS-3s','Cuatro','SR-5','RS-5','RS-6','SR-6','RSS-7','SRR-7','RSSS-8',
'SRRR-8']
#   Species                                                       (#) Abreviation
# 1 (R)‐2‐(tert‐butylacetylene‐1‐yl)-pyrimidyl‐5‐(iso‐butan‐1‐ol) (1) R‐1
# 2 (S)‐2‐(tert‐butylacetylene‐1‐yl)-pyrimidyl‐5‐(iso‐butan‐1‐ol) (1) S‐1 
# 3 Diisopropylzinc (iPr2Zn)                                          ZnR2
# 4 (R)‐Zinc alcoholate                                           (2) R-2
# 5 (S)‐Zinc alcoholate                                           (2) S-2
# 6 (R,R)‐Homochiral dimeric zinc alcoholate                      (3) RR-3
# 7 (S,S)‐Homochiral dimeric zinc alcoholate                      (3) SS-3
# 8 (R,S)‐Heterochiral dimeric zinc alcoholate dissolved         d(3) RS-3d
# 9 (R,S)‐Heterochiral dimeric zinc alcoholate precipitated      s(3) RS-3s
#10 2-(ter-butylacetylene-1-yl)-pyrimidyl-5-carbaldehyde          (4) Cuatro
#11 (R,R)‐Major hemiacetal                                        (5) RR-5
#12 (S,S)‐Major hemiacetal                                        (5) SS-5
#13 (S,R)‐Minor hemiacetal - Epimerization.                       (5) SR-5
#14 (R,S)‐Minor hemiacetal - Epimerization.                       (5) RS-5
#15 (R,R)‐Major hemiacetal - Complex previous to trimer           (6) RR-6
#16 (S,S)‐Major hemiacetal - Complex previous to trimer           (6) SS-6
#17 (R,S)‐Minor hemiacetal - Epimer - Complex previous to trimer  (6) RS-6
#18 (S,R)‐Minor hemiacetal - Epimer - Complex previous to trimer  (6) SR-6
#19 (R,R,R)‐Major hemiacetal - Trimer                             (7) RRR-7
#20 (S,S,S)‐Major hemiacetal - Trimer                             (7) SSS-7
#21 (R,S,S)‐Minor hemiacetal - Trimer - Epimerization.            (7) RSS-7
#22 (S,R,R)‐Minor hemiacetal - Trimer - Epimerization.            (7) SRR-7
#23 (R,R,R,R)-Dimeric-Major-hemiacetal                            (8) RRRR-8
#24 (S,S,S,S)-Dimeric-Major-hemiacetal                            (8) SSSS-8
#25 (R,S,S,S)-Dimeric-Minor-hemiacetal - Epimerization            (8) RSSS-8
#26 (S,R,R,R)-Dimeric-Minor-hemiacetal - Epimerization            (8) SRRR-8

reactions = [
    'R-1 + ZnR2  -> R-2',       # 0        k1 -> Table 2: 150, SM, Fig. 74: 488 => 150(488)
    'S-1 + ZnR2  -> S-2',       # 1                       150(488)
    'R-2 + R-2  <-> RR-3',     # 2 & 3. k2 -> Forward: 700(0.469); Backward: 8.6(0.00515); Keq = 81(91.1)
    'S-2 + S-2  <-> SS-3',     # 4 & 5.                700(0.469);           8.6(0.00515); Keq = 81(91.1)
    'R-2 + S-2  <-> RS-3d',    # 6 & 7.           k3 -> 700(269);   4.3(1.48); Keq = 162(182)
    'RS-3d      <-> RS-3s',     # 8 & 9.  k3a -> not-reported(166); not-reported(0.0166); Keq = not-reported(10000)
    'R-2 + Cuatro     <-> RR-5', # 10 & 11. RR-Hemiacetal.            k4 -> 0.0017(0.0015); 0.013(0.011); Keq = 0.136(0.136)
    'S-2 + Cuatro     <-> SS-5', # 12 & 13. SS-Hemiacetal.                  0.0017(0.0015); 0.013(0.011); Keq = 0.136(0.136)
    'S-2 + Cuatro     <-> SR-5', # 14 & 15. Epimerization hemiacetal.       0.0017(0.0015); 0.013(0.011); Keq = 0.136(0.136) # S=1e-3
    'R-2 + Cuatro     <-> RS-5', # 16 & 17. Epimerization hemiacetal.       0.0017(0.0015); 0.013(0.011); Keq = 0.136(0.136)
    'RR-5 + Cuatro + ZnR2 -> RR-6', # 18                              k5 -> 63(74)
    'SS-5 + Cuatro + ZnR2 -> SS-6', # 19                                    63(74)
    'SR-5 + Cuatro + ZnR2 -> SR-6', # 20. Epimerization hemiacetal.   ¿= k5? No son los mismos reactivos, entonces diferentes k.
    'RS-5 + Cuatro + ZnR2 -> RS-6', # 21. Epimerization hemiacetal.   ¿? o de pronto si, solo cambia la posición espacial, eso dif.
    'RR-6           -> RRR-7', # 22        k6 -> 0.11(0.14)
    'SS-6           -> SSS-7', # 23              0.11(0.14)
    'RS-6           -> RSS-7', # 24        ¿=k6?
    'SR-6           -> SRR-7', # 25        ¿=k6?
    'RRR-7 + Cuatro     -> RRRR-8', # 26   k7 -> 13.2(16.2)
    'SSS-7 + Cuatro     -> SSSS-8', # 27         13.2(16.2)
    'RSS-7 + Cuatro     -> RSSS-8', # 28   ¿=k7?
    'SRR-7 + Cuatro     -> SRRR-8', # 29   ¿=k7?
    'RRRR-8      -> 2 RR-5',   # 30. Tretamer-Depolimerization.          k8 -> 0.23(0.26)
    'SSSS-8      -> 2 SS-5',   # 31. Tretamer-Depolimerization.                0.23(0.26)
    'RSSS-8   -> RS-5 + SS-5', # 32. Epimer - Tretamer-Depolimerization. ¿=k8?
    'SRRR-8   -> SR-5 + RR-5', # 33. Epimer - Tretamer-Depolimerization. ¿=k8?
]
dual_pairs = [(0,1),(2,4),(3,5),(10,12),(11,13),(10,14),(11,15),(10,16),(11,17),(18,19),(20,21),(22,23),(24,25),(26,27),(28,29),(30,31),(32,33)] # Deben ser iguales también: (2,6),(3,7) ,(18,20) ,(22,24) ,(26,28) ,(30,32) # Al parecer no deben ser iguales porque si lo son se estabiliza, no en esta que es estable aun con estas diferentes, pero si en las otras que algo dan en 5A.
#dual_pairs = [(0,1),(2,4),(3,5),(10,12),(11,13),(10,14),(11,15),(10,16),(11,17),(18,19),(18,20),(18,21),(22,23),(22,24),(22,25),(26,27),(26,28),(26,29),(30,31),(30,32),(30,33),(2,6),(3,7)] # Igualando las anteriores y colocándola en el formato que lee bien.
analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # "characteristic-polynomial", "trace-determinant",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Soai-Trapp-et-al-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : False
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Soai-Trapp-et-al-5A', # or None
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0,1),(2,3),(4,5),(6,7),(8,9),(10,11),(12,13)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Soai-Trapp-et-al-6A',
    }
}
