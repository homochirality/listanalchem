# Calvin model: Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin.py > Calvin.out
modelname = "Calvin-Manuscript-Explicit-Enantiomers"
species = ['L-B', 'D-B', 'L-A', 'D-A']
reactions = [
    'L-A <-> D-A',          # 0 y 1
    'L-A <-> L-B',          # 2 y 3
    'D-A <-> D-B',          # 4 y 5
    'L-B + L-A <-> 2 L-B',  # 6 y 7
    'D-B + D-A <-> 2 D-B'   # 8 y 9
]
dual_pairs = [(0,1), (2, 4), (3, 5), (6, 8), (7, 9)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 1000,
        "num-samples": 10,
        "samples-folder": 'samples/calvin_manuscript',
    },
    "frank-pseudoquiral": {
        "enabled": False,
        "enantiomeric-pairs": [(0, 1), (2, 3)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 1000,
        "num-samples": 10,
        "samples-folder": 'samples/calvin_manuscript',
    }
}
