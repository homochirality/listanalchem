# Brusselator
modelname = 'Brusselator'
species = ['x', 'y']
reactions = [
    ' -> x',
    '2x + y -> 3x',
    'x -> y',
    'x -> ',
]
