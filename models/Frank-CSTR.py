# Frank, F. C. (1953) Biochim. Biophys. Acta, 11, 459–463.
# Frank model modified by the inclusion of the reverse reactions and added with an explicit product and one precursor in an open system, a CSTR reactor.
# Run with: python -m listanalchem --model models/Frank-CSTR.py > Frank-CSTR.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Frank-CSTR.py > outputs/Frank-CSTR.out

modelname = 'Frank reversible model plus explicit product and one precursor in a CSTR'
species   = ['L', 'D', 'A', 'P']
reactions = [
    '       -> A ',  # 0
    'A + A <-> L ',  # 1 & 2
    'A + A <-> D ',  # 3 & 4
    'A + L <-> 2L',  # 5 & 6    #     L -> 2L
    'A + D <-> 2D',  # 7 & 8    #     D -> 2D
    'L + D <-> P ',  # 9 & 10   # L + D ->
    '    A  ->   ',  # 11
    '    L  ->   ',  # 12
    '    D  ->   ',  # 13
    '    P  ->   ',  # 14
]
dual_pairs = [(0,11),(11,12),(12,13),(13,14),(1,3),(2,4),(5,7),(6,8)]

analyses = {
    "trace-determinant": {  	######### First algorithm.  #########
        "enabled": False,
        "2by2-jacobian": True
    },
    "sna": {		    	######### Second algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 3, # 3 short time. 4 about 3 hours. 5 more than 24 hours. Same result.
        "simplification-tries": 10,
        "num-samples": 10,
        "samples-folder": 'samples/Frank-CSTR-SNA',
    },
    "six-categories": {	    	######### Third algorithm.  #########
        "enabled": False
    },
    "frank-ineq-nonlinear": {	######### Fourth algorithm.  #########
        "enabled": False
    },
    "frank-ineq-linear": {	######### Fifth algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": True,
        "num-samples": 10,
        "samples-folder": None, #'samples/Frank-CSTR-5A',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {	######### Six algorithm.  #########
        "enabled": False,
        "enantiomeric-pairs": [(0, 1)],
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Frank-CSTR-6A',
    }
}
