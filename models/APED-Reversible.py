# Plasson, R., Bersini, H., & Commeyras, A. (2004). Recycling Frank: Spontaneous emergence of homochirality in noncatalytic systems. Proceedings of the National Academy of Sciences of the United States of America, 101(48), 16733–16738. https://doi.org/10.1073/pnas.0405293101
# Run with: python -m listanalchem --model models/APED-Reversible.py > outputs/APED-Reversible.out
modelname = 'APED reversible'
species = ['L', 'D', 'La', 'Da', 'LL', 'DD', 'DL', 'LD']
reactions = [
    '     L <-> La',    #  0 & 1  - a & b
    '     D <-> Da',    #  2 & 3  - a & b
    'La + L <-> LL',    #  4 & 5  - p
    'Da + L <-> DL',    #  6 & 7  - alfap *     Si bifurcación.
    'La + D <-> LD',    #  8 & 9  - alfap *     Si bifurcación.
    'Da + D <-> DD',    # 10 & 11 - p     * 
    '    LL <-> L + L', # 12 & 13 - h     **    
    '    DL <-> L + D', # 14 & 15 - betah **
    '    LD <-> L + D', # 16 & 17 - betah **
    '    DD <-> D + D', # 18 & 19 - h     **   
    '    LD <-> DD',    # 20 & 21 - e & gammae ***  
    '    DL <-> LL'     # 22 & 23 - e & gammae ***  
]
# According to reference.
dual_pairs = [(0,2),(1,3),(4,10),(5,11),(6,8),(7,9),(12,18),(13,19),(14,16),(15,17),(20,22),(21,23)] # Also must be equal ,(4,6),(5,7),(12,14),(13,15),(20,21)

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "characteristic-polynomial", # or "mineurs", "trace-determinant",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 2,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-Reversible-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-Reversible-5A', # None, #
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3), (4, 5), (6, 7)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/APED-Reversible-6A',
    }
}
