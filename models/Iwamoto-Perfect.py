# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Perfect.py > outputs/Iwamoto-Perfect.out

modelname = 'Iwamoto perfect'
species = ["L", "D", "E-L", "E-D", "A"]
reactions = [
    "  <-> A",        # 0 and 1. # P is constant.
    " A + L <-> 2L ", # 2 and 3. 
    " A + D <-> 2D ", # 4 and 5.
    " L + E-L <-> ",  # 6 and 7. # Z-L is constant. Imply [L] and [E-L] input and output.
    " D + E-D <-> ",  # 8 and 9. # Z-D is constant. Imply [D] and [E-D] input and output.
    "  -> E-L ",      # 10.      # Z-L is constant. Imply [E-L] input. Q is irrelevant.
    "  -> E-D "       # 11.      # Z-D is constant. Imply [E-D] input. Q is inert.
]
dual_pairs = [(2, 4), (3, 5), (6, 8), (7, 9), (10, 11)]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 3, #2: There are no negative terms. #5: Couln't solve equations.
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-perfect-SNA',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-perfect-5A', # None,
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Iwamoto-perfect-6A',
    }
}
