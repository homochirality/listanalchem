# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Imperfect-Closed-system.py > Iwamoto-Imperfect-Closed.out

modelname = 'Iwamoto in closed system'
species = ["L", "D", "E-L", "E-D", "A", "P", "Z-L", "Z-D", "Y-L", "Y-D", "Q"]
reactions = [
    "P <-> A",          # 0 & 1.
    "A + L <-> 2L",     # 2 & 3.
    "A + L <-> L + D",  # 4 & 5.
    "A + D <-> 2D",     # 6 & 7.
    "A + D <-> D + L",  # 8 & 9.
    "L + E-L <-> Z-L",     # 10 & 11.
    "D + E-D <-> Z-D",     # 12 & 13.
    "D + E-L <-> Y-L",     # 14 & 15.
    "L + E-D <-> Y-D",     # 16 & 17.
    "Z-L <-> E-L + Q",          # 18 & 19.
    "Z-D <-> E-D + Q",           # 20 & 21.
    "Y-L <-> E-L + Q",           # 22 & 23.
    "Y-D <-> E-D + Q",           # 24 & 25.    
]
dual_pairs = [(2,6),(4,8),(3,7),(5,9),(10,12),(14,16),(11,13),(15,17),(18,20),
              (20,22),(22,24),(19,21),(21,23),(23,25)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-imperfect-Closed-SNA',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3), (6, 7), (8, 9)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-imperfect-Closed-pq',
    }
}
