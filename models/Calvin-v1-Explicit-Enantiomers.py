modelname='Calvin-v1-Explicit-Enantiomers'
species = ["L-B", "D-B", "L-A", "D-A"]
reactions = [
    " L-A -> D-A",
    " D-A -> L-A",
    " L-A -> L-B",
    " L-B -> L-A",
    " D-A -> D-B ",
    " D-B -> D-A",
    " L-A + L-B  -> 2L-B ",
    " 2L-B -> L-A + L-B",
    " D-A + D-B  -> 2D-B ",
    " 2D-B -> D-A + D-B",
    ]
dual_pairs = [(0,1),(2, 4), (3, 5), (6, 8), (7, 9)]
