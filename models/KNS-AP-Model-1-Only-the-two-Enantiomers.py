# Kondepudi-Nelson-Strecker-Amino-acids-Production-KNS-AP-Model-1. Only the two Enantiomers.
# Run with: python -m listanalchem --model models/Kondepudi-Nelson-Strecker-Amino-acids-Production-KNS-AP-Model-1-Only-the-two-Enantiomers.py > KNS.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Nelson-Strecker-Amino-acids-Production-KNS-AP-Model-1-Only-the-two-Enantiomers.py > outputs/KNS.out

modelname = 'KNS-AP: Amino acids Production. No cross inhibition. Only the two Enantiomers.'
species = ['L-CN', 'D-CN']
reactions = [
    "     <-> L-CN",   # 0 & 1  
    "     <-> D-CN",   # 2 & 3
    "L-CN <-> 2 L-CN", # 4 & 5
    "D-CN <-> 2 D-CN", # 6 & 7
    "L-CN  -> ",       # 8       # Ignores this two reactions given that they're already in the list
    "D-CN  -> "        # 9       # as the reverse reactions of the first two ( 1 and 3).
    # El efecto en la matriz es adicionar -1 0 y 0 -1 en las respectivas filas.
    # ¿Qué implica esto para la matriz?
]

analyses = {
    "trace-determinant": {	######### First algorithm.  #########
        "enabled": True,
        "2by2-jacobian": True,
        "num-samples": 10,
        "plot": True
    },
    "sna": {			######### Second algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Kondepudi-Nelson-SNA',
    },
    "six-categories": {		######### Third algorithm.  #########
        "enabled": True,
        "num-samples": 10,
        "samples-folder": 'samples/Kondepudi-Nelson-6c'
    },
    "frank-ineq-nonlinear": {	######### Fourth algorithm.  #########
        "enabled": False
    },
    "frank-ineq-linear": {	######### Fifth algorithm.  #########	
        "enabled": False,
        "dual-pairs-in-ec": True,
        "num-samples": 10,
        "samples-folder": 'samples/Kondepudi-Nelson-5A',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {	######### Six algorithm.  #########	
        "enabled": False,
        "enantiomeric-pairs": [(0, 1)],
        # "dual-pairs-in-ec": True,
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Kondepudi-Nelson-pq',
    }
}
