# Calvin, M. (1969). Chemical evolution: molecular evolution towards the origin of living systems on the earth and elsewhere. Oxford University Press. https://books.google.com.co/books?id=LU64AAAAIAAJ
# Run with: python -m listanalchem --model models/Calvin-1-CSTR-LES.py > outputs/Calvin-1-CSTR-LES.out
modelname = "Calvin version 1 CSTR with LES (limited enantioselectivity)"
species = ['L-B', 'D-B', 'L-A', 'D-A']
reactions = [
    '           -> L-A + D-A + L-B + D-B', # 0
    '      L-A <-> D-A',                   # 1 & 2  #     S' <-> R'
    '      L-A <-> L-B',                   # 3 & 4  #     S' -> S
    '      D-A <-> D-B',                   # 5 & 6  #     R' -> R 
    'L-A + L-B <-> 2 L-B',                 # 7 & 8  # S' + S -> 2S
    'D-A + D-B <-> 2 D-B',                 # 9 & 10 # R' + R -> 2R
    '      L-A  -> ',                      # 11
    '      D-A  -> ',                      # 12
    '      L-B  -> ',                      # 13
    '      D-B  -> ',                      # 14
    'L-A + L-B <-> L-B + D-B',             # 15 & 16
    'D-A + D-B <-> L-B + D-B'              # 17 & 18
]
dual_pairs = [(11,12),(11,13),(11,14), (1,2),(3,5),(4,6),(7,9),(8,10),(15,17),(16,18),(7,15)] # (11,12),(12,13),(13,14) # Must be equals (9,15), (10,16) ### Con (7,15) se iguala el cuartero ... pendiente verificar si es suficiente o mejor usar la secuencia empleada en el caso de igualar los flujos de salida de un CSTR.

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True
    },    
    "sna"                 : { ######### Second algorithm. #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or:  "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1-CSTR-LES',
    },
    "six-categories"      : { ######### Third algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm. #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1-CSTR-LES', #None
        "samples-for-proportion": 10000
    },    
    "frank-pseudoquiral"  : { ######### Sixth algorithm.  #########
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1), (2, 3)],
        "dual-pairs-in-ec"      : True,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Calvin-1-CSTR-LES',
    }
}
