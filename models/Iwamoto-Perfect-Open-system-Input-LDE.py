# Iwamoto, K. (2003). Spontaneous appearance of chirally asymmetric steady states in a reaction model including Michaelis-Menten type catalytic reactions. Phys. Chem. Chem. Phys., 5(17), 3616–3621. https://doi.org/10.1039/B303363E
# Run with: python -m listanalchem --model models/Iwamoto-Perfect-Open-system-Input-LDE.py > Iwamoto-Perfect-Open-Input-LDE.out

modelname = 'Iwamoto perfect in open system input L, D and E.'
species = ["L", "D", "E-L", "E-D", "A", "Z-L", "Z-D", "Q"]
reactions = [
    " <-> A",       # 0 & 1.  # P constant, as assume by author, is the same as a constant input of A.
    "A + L <-> 2L",    # 2 & 3. # Initial [L] and [D] must be different from 0, but it is not
    "A + D <-> 2D",    # 4 & 5. # necessary an entrance of them.
    "L + E-L <-> Z-L", # 6 & 7. # The same apply to [E-L] and [E-D].
    "D + E-D <-> Z-D", # 8 & 9.
    "Z-L <-> E-L + Q", # 10 & 11.
    "Z-D <-> E-D + Q", # 12 & 13.
    "L <-> ",           # 14 & 15.
    "D <-> ",           # 16 & 17.
    "E-L <-> ",         # 18 & 19.
    "E-D <-> ",         # 20 & 21.
    "Z-L -> ",         # 22.
    "Z-D -> ",         # 23.
    "Q -> ",           # 24.
]
dual_pairs = [(2, 4), (3, 5), (6, 8), (7, 9), (10, 12), (11,13),(1,14),
              (14,16),(16,18),(18,20),(20,22),(22,23),(23,24),
              (15,17),(19,21)]

analyses = {
    "sna": {
        "enabled": True,
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-Open-Input-LDE-pq',
    },
    "frank-pseudoquiral": {
        "enabled": True,
        "enantiomeric-pairs": [(0, 1), (2, 3), (5, 6)],
        "dual-pairs-in-ec": True,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Iwamoto-perfect-Open-Input-LDE-pq',
    }
}
