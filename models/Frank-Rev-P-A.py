# Frank, F. C. (1953) Biochim. Biophys. Acta, 11, 459–463.
# Frank model modified by the inclusion of the reverse reactions and added with an explicit product and one precursor.
# Run with: python -m listanalchem --model models/Frank-Rev-P-A.py > Frank-Rev-P-A.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Frank-Rev-P-A.py > outputs/Frank-Rev-P-A.out

modelname = 'Frank reversible model plus explicit product and one precursor'
species   = ['L', 'D', 'A', 'P']
reactions = [
    'A + A <-> L ', # 0 & 1
    'A + A <-> D ', # 2 & 3
    'A + L <-> 2L', # 4 & 5 # Frank original # L     -> 2L
    'A + D <-> 2D', # 6 & 7                  # D     -> 2D
    'L + D <-> P '  # 8 & 9                  # L + D ->
]
# dual_pairs = [(0, 1)]

analyses = {
    "trace-determinant": {  	######### First algorithm.  #########
        "enabled": False,
        "2by2-jacobian": True
    },
    "sna": {		    	######### Second algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Frank-Rev-P-A-SNA',
    },
    "six-categories": {	    	######### Third algorithm.  #########
        "enabled": False
    },
    "frank-ineq-nonlinear": {	######### Fourth algorithm.  #########
        "enabled": False
    },
    "frank-ineq-linear": {	######### Fifth algorithm.  #########
        "enabled": True,
        "dual-pairs-in-ec": True,
        "num-samples": 1,
        "samples-folder": 'samples/Frank-Rev-P-A-5A', #None,
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral": {	######### Six algorithm.  #########
        "enabled": False,
        "enantiomeric-pairs": [(0, 1)],
        "dual-pairs-in-ec": False,
        # "instability-heuristic": "trace-determinant",
        # "instability-heuristic": "characteristic-polynomial",
        "instability-heuristic": "mineurs",
        "sum-mineurs": True,
        "max-mineur-search-stop": 5,
        "simplification-tries": 10000,
        "num-samples": 10,
        "samples-folder": 'samples/Frank-Rev-P-A-6A',
    }
}
