# -*- coding: utf-8 -*-

# Copyright 2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import sys
import sympy

from sympy import oo

from functools import reduce
import operator

if 'typing' in sys.modules:
    from typing import TYPE_CHECKING

    if TYPE_CHECKING:
        from typing import Any, Union, List, Tuple, Dict, Optional, Set  # noqa: F401
        import sympy as sp  # noqa: F401

        PolyOptList = Union[sp.Poly, List[sp.Poly]]
        VarRepls = Dict[sp.Symbol, float]


# This variable indicates how far to sample from an interval that goes to infinity
# For example: Interval(2, oo)
end_oo = 2


def nonlineal(pol):
    # type: (PolyOptList) -> bool
    if isinstance(pol, list):
        for p in pol:
            if nonlineal(p):
                return True
        return False
    else:
        # sum(g[0]) determines how many variables has that component of the polynomial
        # e.g., j1*j3 is 2, j1*j3*j4 is 3, 5 is 0, and j5 is 1
        return any([sum(g[0]) > 1 for g in pol.terms()])


def singlevar(pol):
    # type: (PolyOptList) -> bool
    if isinstance(pol, list):
        genstotal_ = set()
        for p in pol:
            genstotal_.update(set(p.gens))
            # print(p.gens)
        return len(genstotal_) == 1
    else:
        return len(pol.gens) == 1


def select_var(pol, verbose=False):
    # type: (PolyOptList, bool) -> sp.Symbol
    if isinstance(pol, list):
        genstotal_ = set()
        for p in pol:
            genstotal_.update(set(p.gens))
            # print(p.gens)
        genstotal = list(genstotal_)
        freqs_ = {g: 0 for g in genstotal}
        for p in pol:
            shape = (-1, len(p.gens))
            freqs_p = np.array(
                [g[0] for g in p.terms()
                 if sum(g[0]) > 1],
                dtype=int
            ).reshape(shape).sum(axis=0)
            # print("here")
            # print(p.gens)
            # print(freqs_p)
            for g, freq in zip(p.gens, freqs_p):
                freqs_[g] += freq
        freqs = [freqs_[g] for g in genstotal]
    else:
        freqs = np.array(
            [g[0] for g in pol.terms() if sum(g[0]) > 1]
        ).sum(axis=0)
        genstotal = pol.gens

    idxs = np.argwhere(freqs == np.amax(freqs)).reshape((-1,))
    i = np.random.choice(idxs)
    if verbose:
        print("variables in polynomial: {}".format(genstotal))
        print("list of frequencies: {}".format(freqs))
        print("idxs with highest frequency: {}".format(
            [genstotal[i] for i in idxs]))
        print("selected: {}".format(genstotal[i]))
    return genstotal[i]


def replace_var_in_pol(
        var,        # type: sp.Symbol
        pol,        # type: PolyOptList
        lsteqs,     # type: Optional[List[Tuple[int, sp.Poly]]]
        xreplaced,  # type: VarRepls
        value=None  # type: Optional[float]
):
    # type: (...) -> Tuple[PolyOptList, Optional[List[sp.Poly]], VarRepls]

    # replacing by 1 sometimes "breaks" the polynomial simplication. E.var., when we
    # replace y for 1 in the polynomial `x*y**2 - x` we get 0, which is truth only for
    # y == 0, but not in other cases. Therefore, we don't simplify by 1 but by a
    # random number (and hope nothing goes awry, ie, here a heuristic ;) )
    # xrep = {var: 1}
    if value is None:
        xreplaced.update({var: sympy.Float(np.random.rand())})
    else:
        xreplaced.update({var: sympy.Float(value)})

    if isinstance(pol, list):
        pol = [p.xreplace(xreplaced) for p in pol
               if not (len(p.gens) == 1 and p.gens[0] == var)]  # don't add single variable polys
    else:
        pol = pol.xreplace(xreplaced)

    if lsteqs is not None:
        lsteqs = [(i, eq.xreplace(xreplaced)) for i, eq in lsteqs
                  if (len(eq.gens) > 0  # don't add empty polys
                  and not (len(eq.gens) == 1 and eq.gens[0] == var))]

    return pol, lsteqs, xreplaced


def find_constr_vars_to_rep(lsteqs):
    # type: (List[Tuple[int, sp.Poly]]) -> List[Tuple[sp.Symbol, float, int]]
    vars_ = []  # type: List[sp.Symbol]
    for i, eq in lsteqs:
        if len(eq.gens) == 1:
            var = eq.gens[0]
            value = sympy.solve_linear(eq)[1]
            vars_.append((var, value, i))
    return vars_


def gens2replace(
        pol,           # type: Union[sp.Poly, List[sp.Poly]]
        lsteqs_=None,   # type: Optional[List[sp.Poly]]
        verbose=False  # type: bool
):
    # type: (...) -> Tuple[List[sp.Symbol], List[sp.Symbol], Optional[List[int]]]
    """
    Given a sympy polynomial (or list of polynomials) it returns which variables must be
    replaced by numbers to make the polynomial solvable by Reduce CAS.

    :param pol:     Sympy Polynomial (or list of polynomials)
    :param lsteqs:  Optional list of lineal equations that the solution should take into
                    account. Each restriction is a polynomial that should be equal to 0.
                    E.g., x = y + 1 is represented by x - y - 1
    :param verbose: Shows some debugging info if set
    :return:  Which variables must be replaced to make the polynomial solvable by
              ReduceCAS
    """
    toreplace = []
    notrep = set()  # type: Set[sp.Symbol]
    xreplaced = {}  # type: VarRepls

    if lsteqs_:
        lsteqs = list(enumerate(lsteqs_))  # type: Optional[List[Tuple[int, sp.Poly]]]
        total_lsteqs = set(range(len(lsteqs_)))
    else:
        lsteqs = None

    if verbose:
        print("pol:", pol)
        print("lsteqs:", lsteqs)
        print()

    # Reduce (CAS) can only find the solution to simple system equations. It can find the
    # solutions for a set of lineal equations (and inequations), and systems with only one
    # variable
    while nonlineal(pol) and not singlevar(pol):
        # Replacing all variables that must be replaced given the list of restrictions
        # `lsteqs`

        # Finding variable that should be replaced
        var = select_var(pol, verbose=verbose)
        pol, lsteqs, xreplaced = replace_var_in_pol(var, pol, lsteqs, xreplaced)

        toreplace.append(var)

        if verbose:
            print("pol:", pol)
            print("lsteqs:", lsteqs)
            # print(var)
            print()

        if lsteqs is not None:
            constrained_vars = find_constr_vars_to_rep(lsteqs)
            if len(constrained_vars) > 0:
                for var, value, i in constrained_vars:
                    pol, lsteqs, xreplaced = \
                        replace_var_in_pol(var, pol, lsteqs, xreplaced, value)
                notrep.update(set([var for var, _, _ in constrained_vars]))

                if verbose:
                    print("pol and lsteqs simplified by restrictions")
                    print("pol:", pol)
                    print("lsteqs:", lsteqs)
                    # print(var)
                    print()

    if verbose:
        print("Values used to replace: ", xreplaced)

    if lsteqs is None:
        deleted_eqs = None  # type: Optional[List[int]]
    else:
        deleted_eqs = list(total_lsteqs - set([i for i, _ in lsteqs]))

    if isinstance(pol, list):
        genstotal_ = set()
        for p in pol:
            genstotal_.update(set(p.gens))
        return toreplace + list(notrep), list(genstotal_), deleted_eqs
    else:
        return toreplace + list(notrep), list(pol.gens), deleted_eqs


def isFloating(n):
    # type: (Any) -> bool
    return (isinstance(n, sympy.Number) and n.is_Number) \
        or isinstance(n, (np.float64, float))


def sample_eqs_and_ranges(
        eqs_and_ranges,  # type: List[Tuple[sympy.Symbol, Union[sympy.Interval, sympy.Poly, float]]]
        n_samples=1      # type: int
):
    # type: (...) -> Any
    sample_js = [{} for _ in range(n_samples)]  # type: List[Dict[sympy.Symbol, float]]

    for (j, value_or_range) in eqs_and_ranges:
        # cheking if j is defined as a range
        if isinstance(value_or_range, sympy.Interval):
            start_ = [value_or_range.start.xreplace(s_js) for s_js in sample_js]
            end_ = [value_or_range.end.xreplace(s_js) for s_js in sample_js]
            # print(start_)
            # print(type(start_))
            assert all(isFloating(s) for s in start_)
            start = np.array([float(s) for s in start_], dtype=np.float64)
            if end_[0] == oo:
                end = start*2 + end_oo
            else:
                assert all(isFloating(e) for e in end_)
                end = np.array([float(e) for e in end_], dtype=np.float64)

            value = np.random.uniform(start, end)
            # print("value {}: {}".format(j, value))
            # assert value_or_range.xreplace( sample_js[0] ).contains( value ), \
            #       "something went wrong, the range {} doesn't contain the sampled"
            #       " number {}".format(value_or_range, value)
            for i in range(n_samples):
                sample_js[i][j] = value[i]
        elif isFloating(value_or_range):
            for i in range(n_samples):
                sample_js[i][j] = value_or_range
        else:  # it's a value
            for i in range(n_samples):
                sample_js[i][j] = value_or_range.xreplace(sample_js[i])  # type: ignore

    return sample_js


def __mul(xs, unity=1):
    # type: (List[Union[sympy.Symbol, int]], int) -> sympy.Mul
    return reduce(operator.mul, xs, unity)


def poly_to_inequality(poly, check_satisfability=False):
    # type: (sympy.Poly, bool) -> Union[bool, sympy.Relation]
    """
    Receives a sympy polynomial and creates an unequality from it.

    E.g, `a + b` (assuming `a + b < 0`) returns False

    """
    nGens = len(poly.gens)
    # negative and positive expressions of poly (a principal determinant of VJ)
    negative, positive = [], []  # type: Tuple[List[sympy.Mul], List[sympy.Mul]]
    for exps, coeff in poly.terms():
        # poly.gens contains all Js, e.g., (J1, J2, J3)
        # exps contains the exponents for each Jn
        term = abs(coeff) * __mul([poly.gens[j]**exps[j] for j in range(nGens)])
        # if term is negative, then add it to negative terms, otherwise to positive
        (negative if coeff < 0 else positive).append(term)

    if check_satisfability:
        if len(positive) == 0:
            return True
        elif len(negative) == 0:
            return False

    return sum(negative) - sum(positive) > 0


if __name__ == '__main__':
    # How to run:  python -m listanalchem.poly_tools
    # print("Hi")
    pol = [e.as_poly() for e in sympy.sympify("[x**2*y**3, z + x+y*z]")]
    lsteqs = [e.as_poly() for e in sympy.sympify("[x-y-1]")]
    torep = gens2replace(pol, lsteqs, verbose=True)
    # torep = gens2replace(pol, None, verbose=True)
    print()
    print(torep)
