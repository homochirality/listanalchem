# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Frank inequality on pseudoquiral Networks (those with two or more enantiomeric pairs),
see reference:

Montoya, A., Cruz, E., & Ágreda, J. (2019). Computing the Parameter Values for the Emergence of Homochirality in Complex Networks. Life, 9(3), 74.
https://doi.org/10.3390/life9030074.
"""

from __future__ import print_function, division

from listanalchem.tools import extreme_currents  # type: ignore
# from listanalchem.tools import get_ranges_for_inequalities, ranges_and_eqs_to_strs
import sympy
import numpy as np
# from listanalchem.reduce_cas import ReduceCASNotInstalledException
import sys

from listanalchem.analyses._02_sna import stability_analysis

import listanalchem.mypprint as lpprint


# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
def parse_enantiomers(enan):
    # type: (str) -> List[Tuple[int, int]]

    no_enan = False
    try:
        import ast
        enantiomers = ast.literal_eval(enan)  # type: List[Tuple[int, int]]
    except (ValueError, SyntaxError):
        no_enan = True
        # error = SyntaxError(e.msg, ('<cmd-line-argument>', e.lineno, e.offset, e.text))

    # Verifying that enantiomers is a list of pairs of ints
    if not no_enan:
        if not isinstance(enantiomers, (list, tuple)) \
                or not all(
                    isinstance(pair, (list, tuple))
                    and len(pair) == 2
                    and isinstance(pair[0], int)
                    and isinstance(pair[1], int)
                    for pair in enantiomers):
            no_enan = True

    if no_enan:
        print("*** Error reading the lists of enantiomers. The parameter must be a pair\n"
              "***  of tuples of integers. The first tuple indicates all L enantiomers.\n"
              "***  The second list indicates all D enantiomers.")
        print("*** Expected something like: [(0,1), (1,2), (2,3)]")
        print("***   but got: {}".format(repr(enan)))
        print("*** Please make sure the expression is enclosed in quotes. All indices must\n"
              "***   fall in between 0 and number of species minus one. The number of L\n"
              "***   species must be the same as of D species\n")

        import argparse
        raise argparse.ArgumentTypeError('List of integer pairs expected.')

    return enantiomers


name = 'Frank on Pseudoquiral Networks'
enabled = False
analysis_config = {
    'enantiomeric-pairs': {
        'type_input': parse_enantiomers,
        'default': [(0, 1)],
        'description': 'Determine pairs of enantiomeric reactions',
        'metavar': 'LIST_OF_INTS'
    },
    'dual-pairs-in-ec': {
        'type_input': bool,
        'default': False,
        'description': 'Add rate constant restrictions to Extreme Currents calculation'
    },
    'instability-heuristic': {
        'type_input': str,
        'default': "mineurs",
        'description': 'Checking when a symbolic Jacobian becomes unstable can only be '
                       'done by the use of heuristics. Chose which heuristic to use to '
                       "check for unstability. The options are: 'mineurs', "
                       "'characterist-polynomial', and 'trace-determinant'."
    },
    'sum-mineurs': {
        'type_input': bool,
        'default': False,
        'description': 'Determines whether to sum all mineurs of the same size or to use'
                       'each indivual mineur independently to check for inestability'
    },
    'max-mineur-search-stop': {
        'type_input': int,
        'default': 5,
        'description': 'Determines the maximum size of a mineur to check for instability'
    },
    'simplification-tries': {
        'type_input': int,
        'default': 1000,
        'description': 'Determines how many trials to make in search for a state that '
                       'satisfies the restrictions'
    },
    'num-samples': {
        'type_input': int,
        'default': 1,
        'description': 'Number of states to sample',
        'metavar': 'INT'
    },
    'samples-folder': {
        'type_input': str,
        'default': None,
        'description': 'Directory to save all sampled states',
        'metavar': 'dir'
    }
}  # type: Dict[str, Any]
additional_description = None
# ===== ANALYSIS CONFIGURATION DESCRIPTION --- END =====


if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from typing import Optional, Tuple, List, Union, Dict, Any  # noqa: F401
        from listanalchem.reactions_parser import ReactionsDetails  # noqa: F401


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    if hasattr(rd, 'reactions_six_categories'):
        print("The Frank Inequality Analysis on chemical mechanisms with more than one "
              "enantiomeric pair (the sixth algorithm) cannot be performed together with "
              "the third algorithm (six categories). Therefore, please deactivate the "
              "third algorithm (six categories) to work with the sixth algorithm "
              "(models with more than one enantiomeric pairs).")
        return

    enantiomeric_pairs     = config['enantiomeric-pairs']      # type: List[Tuple[int, int]]
    dual_pairs_in_EC       = config['dual-pairs-in-ec']        # type: bool
    instability_heuristic  = config['instability-heuristic']   # type: str
    sum_mineurs            = config['sum-mineurs']             # type: bool
    max_mineur_search_stop = config['max-mineur-search-stop']  # type: int
    simplification_tries   = config['simplification-tries']    # type: int
    num_samples            = config['num-samples']             # type: int
    samples_folder         = config['samples-folder']          # type: Optional[str]

    # S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    S, K, xs, species = rd.S, rd.K, rd.xs, rd.species
    nS, nR = S.shape  # number of species and number of reactions

    latex = output['latex']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=output['no_pretty'])

    enantiomers = (
        tuple(pair[0] for pair in enantiomeric_pairs),
        tuple(pair[1] for pair in enantiomeric_pairs)
    )

    duals, not_duals = rd.get_duals()

    print("\n=== Algorithm based on the Frank inequality,"
          " according to references [7, 8] in README.md file ===")
    print()
    print("*** Solving semialgebraic problem (sampling from a non-linear inequalities indirectly)"
          " using Clarke factorization according to the Stoichiometric Network Analysis (SNA) ***")
    print()

    # printing enantiomers by their names
    print("Enantiomers divided into L's and D's groups")
    # print(enantiomers)
    print(tuple(tuple(species[e] for e in enan) for enan in enantiomers))
    print()

    # duals = {ks[s]: ks[f] for (f, s) in duals}
    # R_duals_replaced = R.xreplace(duals).xreplace({xs[1]: xs[0]})

    if len(xs) < 2:
        print("Error, only one or none species given!")
        print()
        return
    elif len(xs) < 4:
        print("Attention: This analysis was designed for chemical mechanisms "
              "(networks) with more than one pair of enantiomeric species.\n"
              "You may want to use the frank-ineq-linear analysis, which performs the same "
              "instability analysis (applies the frank inequality) but only for one pair of "
              "enantiomers.\n"
              "Also, you can try the basic SNA analysis, which searches negative terms in the "
              "determinant of mineurs of the current matrix V(J). The latter can generate "
              "extremely long polynomials depending on the size of your model, and then a long "
              "time of computation and even the computer can be blocked.")
        return

    # diff_eqs = S*R
    #
    # if len(xs) == 2:
    #     jacob = diff_eqs.jacobian(xs)
    # else:
    #     jacob = diff_eqs[:-1, :].jacobian(xs[:-1])

    #
    # jacob_duals_replaced = jacob.xreplace(duals).xreplace({xs[1]: xs[0]})
    # print("Jacobian from differential equations (ignoring last species, and replacing duals)")
    # sympy_pprint(jacob_duals_replaced)
    # print()

    # eigenvals = list(jacob_duals_replaced.eigenvals().keys())
    # print("Jacobian eigenvalues")
    # print(eigenvals)
    # print()

    if S.shape[0] != np.linalg.matrix_rank(S):
        print("Stoichiometric Matrix is SINGULAR!")
        print()

    if dual_pairs_in_EC:
        print("Adding rows to the Stoichiometric Matrix encoding the dual pairs conditions "
              "in order to make the cone defined by the Extreme Currents smaller and "
              "'more precise'\n")
        # Adding new columns (restrictions) to Stoichiometric Matrix
        newrows = [S]
        for l, r in duals:
            row = np.zeros((1, nR))
            row[0, l] = 1
            row[0, r] = -1
            newrows.append(row)

        S_dual = np.concatenate(newrows, axis=0)
        print("Extended Stoichiometric matrix with rows indicating dual pairs")
        sympy_pprint(S_dual)
        print()

        E = extreme_currents(S_dual, verbose=True)
    else:
        E = extreme_currents(S, verbose=True)

    if E is None:
        print("*******")
        print("Stoichiometric Matrix is square,"
              " therefore there are no extreme currents for this system")
        print("*******")
        return

    E = E.astype('int32')
    nE = E.shape[1]  # number of extreme currents

    print("Extreme Currents Matrix (computed from Stoichiometric Matrix)")
    sympy_pprint(E)

    J = sympy.symbols('j0:%d' % nE)  # convex parameters
    E_omega_col = sum([(E[:, i]*J[i]).reshape((nR, 1)) for i in range(nE)], sympy.zeros(nR, 1))
    E_omega = sympy.diag(*E_omega_col)

    print("\nE_omega column")
    sympy_pprint(E_omega_col)
    print()

    VJ = S * E_omega * K.T

    print("V(J) Matrix")
    sympy_pprint(VJ)
    print()

    print("V(J) matrix reshaped to show clearly symetry [A B; B A] in Matrix")
    nonenantiomers = [i for i in range(nS) if (i not in enantiomers[0] and i not in enantiomers[1])]
    new_indices = list(enantiomers[0]) + list(enantiomers[1]) + nonenantiomers
    VJ = VJ[new_indices, :]
    VJ = VJ[:, new_indices]
    sympy_pprint(VJ)
    print()

    if any(e.is_zero for e in E_omega_col):
        for i in range(nR):
            if E_omega_col[i].is_zero:
                print("Warning: The reaction '{}' must be zero according to the SNA analysis"
                      " (the E_omega column is zero for that specific reaction (row))"
                      .format(rd.get_reactions()[i]))
        print()
        # print("Warning: No further processing can be done with the model")
        # exit(1)

    num_enan = len(enantiomers[0])
    A = VJ[0:num_enan, 0:num_enan]
    B = VJ[0:num_enan, num_enan:2*num_enan]
    print("'A' Matrix")
    sympy_pprint(A)
    print()
    print("'B' Matrix")
    sympy_pprint(B)
    print()

    # ineq = (VJ[0, 0] - VJ[0, 1]) > 0
    # print("a - b > 0 criteria")
    # sympy_pprint(ineq)
    # print()

    # New V(J)
    VJnew = A - B
    print("New V(J) Matrix (A - B)")
    sympy_pprint(VJnew)
    print()

    if not dual_pairs_in_EC:
        print_equations_from_dual_pairs(E_omega_col, duals, latex)

    stability_analysis(rd, duals, not_duals, E, E_omega_col, J, VJnew, output,
                       modelname + "_frank_pseudoquiral",
                       enantiomers=enantiomers,
                       instability_heuristic=instability_heuristic,
                       sum_mineurs=sum_mineurs,
                       max_mineur_search_stop=max_mineur_search_stop,
                       simplification_tries=simplification_tries,
                       num_samples=num_samples,
                       samples_folder=samples_folder)


def print_equations_from_dual_pairs(
        E_omega_col,  # type: Any
        duals,  # type: List[Tuple[int, int]]
        latex,  # type: bool
):
    # type: (...) -> None
    eqs_js = [str(E_omega_col[f])+" = "+str(E_omega_col[s]) for (f, s) in duals]
    print("Equations from duals pairs")
    if latex:
        print(r"\begin{align*}")
        for (f, s) in duals:
            print(sympy.latex(E_omega_col[f]), "&=", sympy.latex(E_omega_col[s]), r"\\")
        print(r"\end{align*}")
    else:
        print("[", end="")
        for e in eqs_js:
            print("{},".format(e))
            print(" ", end="")
        print("]")
