# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Frank inequality - non linear - unable to sample, see reference:

Ágreda, J., Mejía, C., & Montoya, J. A. (2018). On the linear algebra of biological
homochirality. Journal of Mathematical Chemistry, 56(6), 1782–1810.
https://doi.org/10.1007/s10910-018-0893-6.
"""

from __future__ import print_function, division

import sympy
import listanalchem.mypprint as lpprint
from listanalchem.tools import get_and_print_eigenvects  # type: ignore

import sys


# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
name = 'Frank inequality Non linear'
enabled = False
analysis_config = {}  # type: Dict[str, Any]
additional_description = None


if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from listanalchem.reactions_parser import ReactionsDetails  # noqa: F401
        from typing import Dict, Any  # noqa: F401


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    S, R, xs, = rd.S, rd.R, rd.xs
    nS, nR = S.shape  # number of species and number of reactions

    latex, no_pretty = output['latex'], output['no_pretty']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=no_pretty)
    sympy_pretty = lpprint.get_sympy_pretty(latex=latex, no_pretty=no_pretty)

    print("\n=== The algorithm base on the Frank inequality according to "
          "reference [7] in the README.md file ===")

    if len(xs) < 2:
        print("Error, only one or none species given")
        print()
        return
    elif len(xs) > 2:
        print("Careful, last species will be removed in the process of creating "
              "jacobian for this reaction network. The jacobian is used to know "
              "if a sample state is hyperbolic or not")
        print()

    print("*** NOTE: semialgebraic problem unsolved, i.e., code stucked at sampling "
          "of non-linear inequlities. Task left for future work!! ***")
    print()

    # 1.
    print("Differential equations:")
    eqs = S*R
    sympy_pprint(eqs)
    print()

    # 2.
    if len(xs) == 2:
        jacob = eqs.jacobian(xs)
    else:
        jacob = eqs[:-1, :].jacobian(xs[:-1])

    # 3. replace x2 for x1 (in paper (ref [7]) i2 for i1), in equations and J
    print("Jacobian from eqs (excluding last eq, which should correspond to an equation "
          "that contains the product which doesn't make part of this system)")
    J = jacob.xreplace({xs[1]: xs[0]})
    sympy_pprint(J)
    print()
    eqs = eqs.xreplace({xs[1]: xs[0]})

    # 3a. Step added, not really meant to make part in the analysis,
    #     just for the researcher to know a little more
    print("Some aditional work with the Jacobian")
    print("  Jacobian's trace")
    sympy_pprint(J.trace().expand())
    print()
    print("  Jacobian's determinant")
    determinant = J.det(method='berkowitz').expand()
    sympy_pprint(determinant)
    print()

    DiscriminantJ = J.trace()**2 - 4*determinant
    print("  Jacobian's discriminant")
    sympy_pprint(DiscriminantJ)
    print()

    discr_simplified = sympy.simplify(DiscriminantJ)
    if DiscriminantJ != discr_simplified:
        print("  Jacobian's discriminant simplified")
        sympy_pprint(discr_simplified)
        print()

    print("  Jacobian's eigenvalues and eigenvectors")
    print()

    get_and_print_eigenvects(J, output)

    # 4. create list of equations and inequations eq1 == 0 and eq2 == 0 and eq3 == 0 ...
    # and det(J) != 0 and (J[0,0] - J[0,1] > 0)
    print("List of equations and inequalities that describe the homochirality")
    print()

    print("* Differential equations in a stationary state:")
    for e in eqs[:-1, 0]:
        print(u"{} = 0".format(sympy_pretty(e)))
    print()

    print("* Jacobian determinant different to zero (det(J) != 0):")
    Jdet = J.det(method='berkowitz')
    print(u"{} != 0".format(sympy_pretty(Jdet)))
    print()

    print("* Frank inequality (J[0,0] - J[0,1] > 0):")
    print(u"{} > 0".format(sympy_pretty(J[0, 0] - J[0, 1])))
    print()

    # 5. look if the set {(x,r) in reals^(n-1+r) : (x,r) fulfills the equations and
    # inequalities above} take samples of the set

    # 6. sample the set (eqs == 0 and det(J)!=0) (i.e., no frank inequality), and
    # calculate (# states to sample that satisfy frank inequality / # of samples)
