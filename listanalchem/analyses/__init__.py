import sys
from os import path
from collections import OrderedDict
from importlib import import_module
import glob
import re

if 'typing' in sys.modules:
    from typing import List, Any  # noqa: F401

__all__ = ['analyses']  # type: List[str]
analyses = OrderedDict()  # type: OrderedDict[str, Any]

dirpath = path.dirname(path.abspath(__file__))
remodulename = re.compile(r"(_[0-9]{2}_(\w*))\.py")

for m in sorted(glob.glob("{}/*.py".format(dirpath))):
    basename = path.basename(m)
    modnameopt = remodulename.match(basename)

    if modnameopt:
        modname = modnameopt.group(1)
        analysis_name = modnameopt.group(2).replace('_', '-')
        analyses[analysis_name] = globals()[modname] = \
            import_module("listanalchem.analyses.{}".format(modname))
        __all__.append(modname)
