# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Frank inequality - SNA - linear sampling, see reference:

Ágreda, J., Mejía, C., & Montoya, J. A. (2018). On the linear algebra of biological
homochirality. Journal of Mathematical Chemistry, 56(6), 1782–1810.
https://doi.org/10.1007/s10910-018-0893-6.
"""

from __future__ import print_function, division

import sympy
import numpy as np
import sys

from listanalchem.tools import (  # type: ignore
    extreme_currents, get_ranges_for_inequalities, ranges_and_eqs_to_strs)
from listanalchem.reduce_cas import ReduceCASNotInstalledException

import listanalchem.mypprint as lpprint
# from listanalchem.poly_tools import sample_eqs_and_ranges

from listanalchem.sampler import save_sample, sample_point_js

from listanalchem.tools import interact # Para ver como van las cosas...******* .

# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
name = 'Frank inequality SNA linear sample'
enabled = False
analysis_config = {
    'dual-pairs-in-ec': {
        'type_input': bool,
        'default': False,
        'description': 'Add rate constant restrictions to Extreme Currents calculation'
    },
    'num-samples': {
        'type_input': int,
        'default': 1,
        'description': 'Number of states to sample',
        'metavar': 'INT'
    },
    'samples-for-proportion': {
        'type_input': int,
        'default': 1000,
        'description': 'Number of states to samples when calculating proportion of frank states',
        'metavar': 'INT'
    },
    'samples-folder': {
        'type_input': str,
        'default': None,
        'description': 'Directory to save all sampled states',
        'metavar': 'dir'
    }
}  # type: Dict[str, Any]
additional_description = None


if 'typing' in sys.modules:
    from typing import Optional, Tuple, List, Union, Dict, Any  # noqa: F401
    from listanalchem.reactions_parser import ReactionsDetails  # noqa: F401


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    n_samples        = config['num-samples']  # type: int
    m_samples        = config['samples-for-proportion']  # type: int
    samples_folder   = config['samples-folder']  # type: Optional[str]
    dual_pairs_in_EC = config['dual-pairs-in-ec']        # type: bool

    S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    nS, nR = S.shape  # number of species and number of reactions

    latex = output['latex']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=output['no_pretty'])

    print("\n=== Algorithm based on the Frank inequality,"
          " according to reference [7] in README.md file ===")
    print()
    print("*** Solving semialgebraic problem (sampling from a non-linear inequalities indirectly)"
          " using Clarke factorization according to the Stoichiometric Network Analysys (SNA) ***")
    print("+++ Now the inequalities are all linear, therefore the sampling is doable."
          " The output below includes the sampling. +++")
    print()

    duals_, not_duals = rd.get_duals()
    duals = {ks[s]: ks[f] for (f, s) in duals_}
    R_duals_replaced = R.xreplace(duals).xreplace({xs[1]: xs[0]})

    if len(xs) < 2:
        print("Error, only one or none species given")
        print()
        return
    elif len(xs) > 2:
        print("Careful, last species will be removed in the process of creating jacobian for"
              " this reaction network. The jacobian is used to know if a sample state is"
              " hyperbolic or not")
        print()

    diff_eqs = S*R

    if len(xs) == 2:
        jacob = diff_eqs.jacobian(xs)
    else:
        jacob = diff_eqs[:-1, :].jacobian(xs[:-1])

    jacob_duals_replaced = jacob.xreplace(duals).xreplace({xs[1]: xs[0]})
    print("Jacobian from differential equations (ignoring last species, and replacing duals)")
    sympy_pprint(jacob_duals_replaced)
    print()

    # eigenvals = list(jacob_duals_replaced.eigenvals().keys())
    # print("Jacobian eigenvalues")
    # print(eigenvals)
    # print()

    if S.shape[0] != np.linalg.matrix_rank(S):
        print("Stoichiometric Matrix is SINGULAR!")
        print()

    if dual_pairs_in_EC:
        print("Adding rows to the Stoichiometric Matrix encoding the dual pairs conditions "
              "in order to make the cone defined by the Extreme Currents smaller and "
              "'more precise'\n")
        # Adding new columns (restrictions) to Stoichiometric Matrix
        newrows = [S]
        for l, r_ in duals_:
            row = np.zeros((1, nR))
            row[0, l] = 1
            row[0, r_] = -1
            newrows.append(row)

        S_dual = np.concatenate(newrows, axis=0)
        print("Extended Stoichiometric matrix with rows indicating dual pairs")
        sympy_pprint(S_dual)
        print()

        E = extreme_currents(S_dual, verbose=True)
    else:
        E = extreme_currents(S, verbose=True)

    if E is None:
        print("*******")
        print("Stoichiometric Matrix is square,"
              " therefore there are no extreme currents for this system")
        print("*******")
        return

    E = E.astype('int32')
    nE = E.shape[1]  # number of extreme currents

    print("Extreme Currents Matrix (computed from Stoichiometric Matrix)")
    sympy_pprint(E)

    J = sympy.symbols('j0:%d' % nE)  # convex parameters
    E_omega_col = sum([(E[:, i]*J[i]).reshape((nR, 1)) for i in range(nE)], sympy.zeros(nR, 1))
    E_omega = sympy.diag(*E_omega_col)

    print("\nE_omega column")
    sympy_pprint(E_omega_col)
    print()

    VJ = S * E_omega * K.T

    print("V(J) Matrix")
    sympy_pprint(VJ)
    print()

    if any(e.is_zero for e in E_omega_col):
        for i in range(nR):
            if E_omega_col[i].is_zero:
                print("Warning: The reaction '{}' must be zero according to the SNA analysis"
                      " (the E_omega column is zero for that specific reaction (row))"
                      .format(rd.get_reactions()[i]))
        print()
        # print("Warning: No further processing can be done with the model")
        # exit(1)

    ineq = (VJ[0, 0] - VJ[0, 1]) > 0
    print("a - b > 0 criteria")
    sympy_pprint(ineq)
    print()

    eqs_js = [str(E_omega_col[f])+" = "+str(E_omega_col[s]) for (f, s) in duals_]
    print("Equations from duals pairs")
    if latex:
        print(r"\begin{align*}")
        for (f, s) in duals_:
            print(sympy.latex(E_omega_col[f]), "&=", sympy.latex(E_omega_col[s]), r"\\")
        print(r"\end{align*}")
    else:
        print("[", end="")
        for e in eqs_js:
            print("{},".format(e))
            print(" ", end="")
        print("]")

    print("\nCalculanting valid values for js")
    # this uses redlog for calculations
    try:
        eqs_and_ranges = get_ranges_for_inequalities(
            [str(ineq)]
            + [str(j)+">0" for j in J]
            + eqs_js
            #      + ['j11 = 3']
            #    , verbose=True
        )
    except ReduceCASNotInstalledException as msg:
        print(msg, file=sys.stderr)
        return

    if eqs_and_ranges is None:
        print()
        print("No further calculations can be done!!")
        print("There are NOT valid values for js")
        print("The equations are non-satisfiable")
        print()
        return

    # converting ranges and equalities into strings
    ranges = ranges_and_eqs_to_strs(eqs_and_ranges)

    print("Values that j's can take to satisfy the a-b inequality (assuming j all positive)")
    if latex:
        print(r"\begin{align*}")
        for r in ranges:
            var, ran = map(sympy.sympify, r.split('='))
            print(sympy.latex(var), "&=", sympy.latex(ran), r"\\")
        print(r"\end{align*}")
    else:
        print("[", end="")
        for r in ranges:
            print("{}".format(r))
            print(" ", end="")
        print("]")
    print()

    if samples_folder:
        print("Sampling {} points and saving them in `{}`".format(n_samples, samples_folder))
        import os
        if not os.path.isdir(samples_folder):
            os.makedirs(samples_folder)

    for sample_i in range(1, n_samples+1):
        if samples_folder:
            print(".", end="")
            sys.stdout.flush()
        else:
            print("Sampling point {} of {}\n".format(sample_i, n_samples))

        sample_xs, sample_ks = \
            sample_point_js(rd, duals_, not_duals, R_duals_replaced, J, eqs_and_ranges,
                            E_omega_col, n_samples=1, verbose=not samples_folder)

        hyperbolic = is_point_hyperbolic(rd, jacob_duals_replaced, duals, sample_xs, sample_ks,
                                         verbose=not samples_folder)

        if samples_folder:
            save_sample(rd, sample_i, sample_xs, sample_ks, hyperbolic, samples_folder,
                        modelname + "_frank_linear")
        else:
            print("ATTENTION: If you want to save the sampled point in a file, please"
                  " add --frank-ineq-linear-samples-folder argument when calling listanalchem")
            print()

    if samples_folder:
        print('\n')

    print("Calculating probability of frank state to occur (using {} sample points)"
          .format(m_samples))
    try:
        eqs_and_ranges_not_only_frank = get_ranges_for_inequalities(
            [str(j)+">0" for j in J]
            + eqs_js
            #    , verbose=True
        )
    except ReduceCASNotInstalledException as msg:
        print(msg, file=sys.stderr)
        return
    print("  js restrictions without frank state calculated")

    sample_xs, sample_ks = \
        sample_point_js(rd, duals_, not_duals, R_duals_replaced, J,
                        eqs_and_ranges_not_only_frank,
                        E_omega_col, n_samples=m_samples, verbose=False)

    print("  {} points sampled".format(m_samples))
    ineq_frank = jacob_duals_replaced[0, 0] - jacob_duals_replaced[0, 1]

    total_frank = 0
    total_hyperbolic = 0
    for i in range(m_samples):
        sample_xs_i = {x: value[i] for x, value in sample_xs.items()}
        sample_ks_i = {x: value[i] for x, value in sample_ks.items()}
        frankvalue = ineq_frank.xreplace(sample_xs_i).xreplace(sample_ks_i)
        if frankvalue > 0:
            total_frank += 1

        hyperbolic = is_point_hyperbolic(rd, jacob_duals_replaced, duals,
                                         sample_xs_i, sample_ks_i, verbose=False)
        if hyperbolic:
            total_hyperbolic += 1

    print("{} sampled points are frank".format(total_frank))
    print("%{} of sampled points are frank states".format(100.*total_frank/m_samples))
    print("{} sampled points are hyperbolic".format(total_hyperbolic))
    print("%{} of sampled points are hyperbolic".format(100.*total_hyperbolic/m_samples))

# TODO: accept an arbitrary number sampled states


def is_point_hyperbolic(
        rd,  # type: ReactionsDetails
        jacob_duals_replaced,  # type: sympy.Matrix
        duals,      # type: Dict[sympy.Symbol, sympy.Symbol]
        sample_xs,  # type: Dict[sympy.Symbol, np.array[float]]
        sample_ks,  # type: Dict[sympy.Symbol, np.array[float]]
        verbose=False  # type: bool
):
    # type: (...) -> bool
    duals_, not_duals = rd.get_duals()

    # eigenvals = list(jacob_duals_replaced.eigenvals().keys())
    # print(eigenvals)

    from numpy import linalg as LA

#    interact(locals(), globals()) # Para ver como van las cosas...*******
#    sample_jacob = jacob_duals_replaced.xreplace(sample_ks).xreplace(sample_xs) # JAAB: June 12th, 2021. Insert [2.345] instead of 2.345 ¿? Solved using .subs instead of .xreplace. Any other problem in the following code (future)? I don't know.
# Also requires a sequence like this (12,13),(12,14),(12,15),(12,16) for those models in a CSTR and to make all the output flows equals. A sequence like this (12,13),(13,14),(14,15),(15,16) doesn't work because 16 is not defined (in python left always recive).
    sample_ks = {k: sympy.Float(v[0] if isinstance(v, np.ndarray) else v) for k, v in sample_ks.items()}
    sample_xs = {k: sympy.Float(v[0] if isinstance(v, np.ndarray) else v) for k, v in sample_xs.items()}
    sample_jacob = jacob_duals_replaced.xreplace(sample_ks).xreplace(sample_xs)

    sj = sympy.matrix2numpy(sample_jacob).astype(np.float64)
    eigvals = LA.eigvals(sj.astype(np.complex))

    if verbose:
        print("Sampled jacobian")
        print(sj)
        print()

        print("Eigenvalues for sampled point")
        print(eigvals)
        print()

    hyperbolic = True
    for eigval in eigvals:
        if abs(eigval) < 1e-10:  # smaller than 1e-10 means that the value is close to 0.
            hyperbolic = False

    if verbose:
        print("Sampled point is{} hyperbolic".format('' if hyperbolic else ' NOT'))
        print()

    return hyperbolic
