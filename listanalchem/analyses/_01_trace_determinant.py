# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Trace Determinant Algorithm, see references:

Hirsch, Morris W.; Smale, Stephen; Devaney, Robert L. 2004. 'Pure and Applid
Mathematics Series: Differential Equations, Dynamical Systems, and an Introduction to
Chaos'. Elsevier Academic Press. Amsterdam. Pag. 63.
https://www.math.upatras.gr/~bountis/files/def-eq.pdf

Gray, Peter; Scott, Stephen K. 1994. 'Chemical oscillations and instabilities :
non-linear chemical kinetics'. Clarendon Press. Oxford. Pag. 66.
"""

from __future__ import print_function, division
import sys

import sympy
from sympy import solve
import listanalchem.mypprint as lpprint
from listanalchem.tools import get_and_print_eigenvects  # type: ignore

# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
name = 'Trace-Determinant'
enabled = False
analysis_config = {
    '2by2-jacobian': {
        'type_input': bool,
        'default': True,
        'description': 'Enable|Disable usage of a reduced jacobian matrix'
                       ' (2x2, left- and uppermost)'
    },
    'num-samples': {
        'type_input': int,
        'default': 1,
        'description': 'Number of states to sample',
        'metavar': 'INT'
    },
    'plot': {
        'type_input': bool,
        'default': True,
        'description': 'Shows the plot'
    },
    'time-to-show-plot': {
        'type_input': int,
        'default': 1,
        'description': 'Time to show the plot',
        'metavar': 'INT'
    }
}  # type: Dict[str, Any]
additional_description = None


if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from listanalchem.reactions_parser import ReactionsDetails  # noqa: F401
        from typing import Dict, Any  # noqa: F401


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    jacobWithOnly2Species = config['2by2-jacobian']  # Type: bool
    num_samples           = config['num-samples']    # type: int
    plot                  = config['plot']           # type: bool
    time_to_show_plot     = config['time-to-show-plot']    # type: int

    sympy_pprint = lpprint.get_sympy_pprint(latex=output['latex'], no_pretty=output['no_pretty'])

#    S, R, xs, = rd.S, rd.R, rd.xs # El antiguo sin las ks. Comentado en sept. 13 2020.
    S, R, xs, ks = rd.S, rd.R, rd.xs, rd.ks # Es el mismo anterior, solo se aumenta ks. Sept.13,2020.
    nS, nR = S.shape  # number of species and number of reactions.

    duals, not_duals = rd.get_duals() # Sept. 19 2020. The equals ks.

    print("\n=== Linear stability analysis for models with only two variables: ===")
    print("*** Trace-Determinant plane. See references [1] and [2] in README.md ***")
    print()
    print("Jacobian matrix according to references [1] and [2]")

    if jacobWithOnly2Species:
        # taking only the first two equations corresponding to the isomers in
        # andrescarolina notation
        diferential_system = (S*R)[:2, :]
        Jacob = diferential_system.jacobian(xs[:2])
    else:
        Jacob = (S*R).jacobian(xs)

    sympy_pprint(Jacob)
    print()
    print("Jacobian's trace")
    sympy_pprint(Jacob.trace().expand())
    print()
    print("Jacobian's determinant")
    sympy_pprint(Jacob.det().expand())
    print()

    DiscriminantJ = Jacob.trace()**2 - 4*Jacob.det()
    print("Jacobian's discriminant")
    sympy_pprint(DiscriminantJ)
    print()
    discr_simplified = sympy.simplify(DiscriminantJ)
    if DiscriminantJ != discr_simplified:
        print("Jacobian's discriminant simplified")
        sympy_pprint(discr_simplified)
        print()

    print("Jacobian's eigenvalues and eigenvectors")

    get_and_print_eigenvects(Jacob, output)
    
######################################################################
    # JAAB: Added JAAB: September 20th, 2020.
######################################################################
    
    import matplotlib.pyplot as plt
    import numpy as np
    from numpy import random

    print("*******************")
    print(" Some numerical calculations using pseudo-random values for ks and 0.01 for xs.")
#    print("*** Added JAAB: September 20th, 2020. ***")
    print("*******************")

    if nS < 2:
      print("This model has only one variable, at least two are needed.")
    elif nS > 2:
      print("This model has more than two variables. This algorithm works only with two variables. Try one of the other five algorithms. We recommend the fifth for one pair of enantiomers or the sixth for two or more enantiomeric pairs.")
    else:
      x0E = 0.01 # float(input('x0 ='))
      x1E = x0E  # float(input('x1 ='))
      x0, x1 = sympy.symbols('x0 x1') 
      ksE = [0]*(len(ks))

      for i in range(num_samples): # num_samples differents random sets of ks are generated.
       print(' ')
       print('Calculation number = ', i+1)
       print('Pseudo-random values are assigned to the rate constants.')

       # The steady state condition:
       eeS1 = diferential_system[0].subs(x0,x0E).subs(x1,x1E)
       eeS2 = diferential_system[1].subs(x0,x0E).subs(x1,x1E)

       for cont1 in range(0,len(duals)-1): # Set random values for the dual pairs rate constants minus one.
         ksE[duals[cont1][0]] = random.rand()*pow(10,random.rand()*7) # float(input('k0 ='))
         ksE[duals[cont1][1]] = ksE[duals[cont1][0]] # This is the dual pair. float(input('k0 ='))
         eeS1 = eeS1.subs(ks[duals[cont1][0]],ksE[duals[cont1][0]]).subs(ks[duals[cont1][1]],ksE[duals[cont1][1]])
         eeS2 = eeS2.subs(ks[duals[cont1][0]],ksE[duals[cont1][0]]).subs(ks[duals[cont1][1]],ksE[duals[cont1][1]])
#         print(ksE)
       for cont1 in range(len(not_duals)): # Set random values for the non dual rate constants.
         ksE[not_duals[cont1]] = random.rand()*pow(10,random.rand()*7) # float(input('k0 ='))
         eeS1 = eeS1.subs(ks[not_duals[cont1]],ksE[not_duals[cont1]])
         eeS2 = eeS2.subs(ks[not_duals[cont1]],ksE[not_duals[cont1]])

       ksE[duals[len(duals)-1][0]] = solve(eeS1)[0]
       ksE[duals[len(duals)-1][1]] = solve(eeS2)[0]

       if ksE[duals[len(duals)-1][0]] != ksE[duals[len(duals)-1][1]]:
         print("*****************************************************************************")
         print("Something was wrong: The rate constants of the {} dual pair are not equal.".format(len(duals)))
         print("*****************************************************************************")
         print(ksE)
         continue
       elif ksE[duals[len(duals)-1][0]] <= 0.0:
         print("*****************************************************************************")
         print("The sampling has produced negative or zero rate constants for the {} dual pair, which is not a reasonable solution. A new calculation will be done.".format(len(duals)))
         print("*****************************************************************************")
         print(ksE)
         continue

       TraceNum2 = Jacob.trace() # Trace.
       DetNum2   = Jacob.det()   # Determinant.
       DisNum    = DiscriminantJ # Discriminant.

       for cont in range(len(ks)):                       # Change the symbolic trace, determinant, and discriminant
         print('k'+str(cont)+' = ',ksE[cont])            # into numerics, replacing one k at a time.
         TraceNum2 = TraceNum2.subs(ks[cont],ksE[cont])  # Trace.
         DetNum2   =   DetNum2.subs(ks[cont],ksE[cont])  # Determinant.
#         DisNum    =    DisNum.subs(ks[cont],ksE[cont]) # Discriminant.

       # Numerical values of trace, determinant and discriminant.
       TraceNum2 = TraceNum2.subs(x0,x0E).subs(x1,x1E)
       DetNum2   =   DetNum2.subs(x0,x0E).subs(x1,x1E)
       DisNum2   = TraceNum2**2 - 4*DetNum2 # Equal to: DisNum = DisNum.subs(x0,x0E).subs(x1,x1E).

       print("Trace = ",        Jacob.trace(), " = ", TraceNum2)
       print("Determinant = ",  Jacob.det(),   " = ", DetNum2)
       print("Discriminant = ", DiscriminantJ, " = ", DisNum2) # DisNum, 

       # TODO: complete. ### Quedan pendientes los iguales a cero ¿? ... Que igual aparecerán muy pocas veces (poco probables).
       if DetNum2 < 0:
        print("************************************")
        print("*** The model has a saddle point ***")
        print("************************************")
       elif TraceNum2 > 0:
        print("******************************")
        print("*** The model is unstable  ***")
        print("******************************")
       else:
        print("****************************")
        print("*** The model is stable  ***")
        print("****************************")

# The plot:
       if plot:
        expEsc = 2 # Scale expansion factor.
        if modelname == 'Frank model':
           tlinf=-10*expEsc*abs(float(DetNum2)); tlsup=10*expEsc*abs(float(DetNum2)) # Abscissa. 
        else:
           tlinf=-expEsc*abs(float(TraceNum2)); tlsup=expEsc*abs(float(TraceNum2)) # Abscissa.
        dlinf=-expEsc*abs(float(DetNum2)); dlsup=expEsc*abs(float(DetNum2))   # Ordinate.
        if TraceNum2 == 0.0:
         tlinf = -0.1; tlsup = 0.1
        if DetNum2 == 0.0:
         dlinf = -0.1; dlsup = 0.1 
        x=np.linspace(tlinf,tlsup,500)
        y=x**2/4
        fig, ax = plt.subplots()
        ax.plot(x,y,color='r')
        ax.hlines(y=0, xmin=tlinf, xmax=tlsup, color='b')
        ax.vlines(x=0, ymin=0.0, ymax=dlsup, color='b')
        plt.xlim([tlinf, tlsup])
        plt.ylim([dlinf, dlsup])
        plt.xlabel("Trace (T)")
        plt.ylabel("Determinant (D)")
        plt.scatter(float(TraceNum2),float(DetNum2)) # Draw the point.
        plt.text(tlinf/6,    3*dlinf/4, "Saddle points",  size=10)
        plt.text(tlsup/2,      dlsup/7, "Unstable nodes", size=10)
        plt.text(tlsup/10,     0.65*dlsup, "Unstable focus", size=10, rotation= 90)
        plt.text(tlinf/8,      0.65*dlsup, "Stable focus",   size=10, rotation=-90)
        plt.text(-2.9*tlsup/3, dlsup/7, "Stable nodes",   size=10)

#       plt.text(tlinf/6,         dlsup*0.9, "T^2 - 4 D < 0", size=10)
        ax.annotate('T^2 - 4 D < 0', 
            xy=(tlinf/6,       0.85*dlsup), xycoords='data',
            size=10, # The box text size.
            bbox=dict(boxstyle="round", fc="lime", ec="none"))

#       plt.text(tlinf/3,         dlinf/5, "T^2 - 4 D > 0", size=10)
        ax.annotate('T^2 - 4 D > 0', 
            xy=(tlinf/6,       dlinf*0.25), xycoords='data',
            size=10, # The box text size.
            bbox=dict(boxstyle="round", fc="#87CEFA", ec="none"))

# # , rotation=180*np.arctan(dlsup/(2*np.sqrt(dlsup)))/np.pi # No me funcionó el cálculo ¿?

#       plt.text(2*np.sqrt(dlsup),dlsup*0.9,"T^2 - 4 D = 0", size=10)
        ax.annotate('T^2 - 4 D = 0', 
            xy=(2*np.sqrt(0.9*dlsup),0.9*dlsup), xycoords='data',  
            xytext=(3*tlsup/4, dlsup/2), textcoords='data',
            size=10, # The box text size.
            bbox=dict(boxstyle="round", fc="magenta", ec="none"),
            arrowprops=dict(arrowstyle="fancy",
                            fc="0.8", ec="none",
                            connectionstyle="angle3,angleA=90,angleB=0"))

        plt.show(block=False) # This one shows the image and continues. Without "block=False" it is neccesary to close the window by hand.
        plt.pause(time_to_show_plot) # The image will be showed X seconds. # 0.5
        plt.close()    # It is important close the graphical mode.
