# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Stoichiometric network analysis (SNA), according to one of the two algorithms presented in references:

Schmitz, Guy, Ljiljana Z. Kolar-Anić, Slobodan R. Anić, and Željko D. Čupić. 2008.
‘Stoichiometric Network Analysis and Associated Dimensionless Kinetic Equations.
Application to a Model of the Bray−Liebhafsky Reaction’. The Journal of Physical Chemistry
A 112 (51):13452–57. <https://doi.org/10.1021/jp8056674>.

Hoops, S., Gauges, R., Lee, C., Pahle, J., Simus, N., Singhal, M., … Kummer, U. (2006). COPASI - A COmplex PAthway SImulator. Bioinformatics, 22(24), 3067–3074.
https://doi.org/10.1093/bioinformatics/btl485.
"""

from __future__ import print_function, division

import numpy as np
import sympy
from sympy import oo
import pprint
import listanalchem.mypprint as lpprint

from listanalchem.tools import (  # type: ignore
    extreme_currents
    # interact
)
from itertools import combinations
from listanalchem.poly_tools import isFloating, poly_to_inequality
from listanalchem.sampler import save_sample, nonlinear_to_linear, sample_point_js

import sys
from os import path, makedirs

from listanalchem.tools import interact # Para ver como van las cosas...******* .
#import code

# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
name = 'SNA'
enabled = False
analysis_config = {
    'dual-pairs-in-ec': {
        'type_input': bool,
        'default': False,
        'description': 'Add rate constant restrictions to Extreme Currents calculation'
    },
    'instability-heuristic': {
        'type_input': str,
        'default': "mineurs",
        'description': 'Checking when a symbolic Jacobian becomes unstable can only be '
                       'done by the use of heuristics. Chose which heuristic to use to '
                       "check for unstability. The options are: 'mineurs', "
                       "'characteristic-polynomial', and 'trace-determinant'."
    },
    'sum-mineurs': {
        'type_input': bool,
        'default': False,
        'description': 'Determines whether to sum all mineurs of the same size or to use '
                       'each indivual mineur independently to check for inestability'
    },
    'max-mineur-search-stop': {
        'type_input': int,
        'default': 5,
        'description': 'Determines the maximum size of a mineur to check for instability'
    },
    'simplification-tries': {
        'type_input': int,
        'default': 1000,
        'description': 'Determines how many trials to make in search for a linear region '
                       'that satisfies the restrictions. From this region, (unstable, '
                       'simetry-breaking) states will be sampled'
    },
    'num-samples': {
        'type_input': int,
        'default': 1,
        'description': 'Number of states to sample',
        'metavar': 'INT'
    },
    'samples-folder': {
        'type_input': str,
        'default': None,
        'description': 'Directory to save all sampled states',
        'metavar': 'dir'
    }
}  # type: Dict[str, Any]
additional_description = None

if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from listanalchem.reactions_parser import ReactionsDetails
        from typing import Union, List, Tuple, Dict, Any, Optional


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    # S, K, R, xs, ks = rd.S, rd.K, rd.R, rd.xs, rd.ks
    S, K = rd.S, rd.K
    nS, nR = S.shape  # number of species and number of reactions

    duals_, not_duals = rd.get_duals()
    # print(duals_, not_duals)

    dual_pairs_in_EC       = config['dual-pairs-in-ec']        # type: bool
    instability_heuristic  = config['instability-heuristic']   # type: str
    sum_mineurs            = config['sum-mineurs']             # type: bool
    max_mineur_search_stop = config['max-mineur-search-stop']  # type: int
    simplification_tries   = config['simplification-tries']    # type: int
    num_samples            = config['num-samples']             # type: int
    samples_folder         = config['samples-folder']          # type: Optional[str]

    latex = output['latex']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=output['no_pretty'])

    print("*** Stoichiometric Network Analysis using the algorithm"
          " from reference [4] or [5] in README.md ***\n")

    if dual_pairs_in_EC:
        print("Adding rows to the Stoichiometric Matrix encoding the dual pairs conditions "
              "in order to make the cone defined by the Extreme Currents smaller and "
              "'more precise'\n")
        # Adding new columns (restrictions) to Stoichiometric Matrix
        newrows = [S]
        for l, r in duals_:
            row = np.zeros((1, nR))
            row[0, l] = 1
            row[0, r] = -1
            newrows.append(row)

        S_dual = np.concatenate(newrows, axis=0)
        print("Extended Stoichiometric matrix with rows indicating dual pairs")
        sympy_pprint(S_dual)
        print()

        E = extreme_currents(S_dual, verbose=True)
    else:
        E = extreme_currents(S, verbose=True)

    if E is None:
        print("*******")
        print("Stoichiometric Matrix is square, therefore there are no extreme"
              " currents for this system")
        print("*******")
        return

    E = E.astype('int32')
    nE = E.shape[1]  # number of extreme currents

    print("Extreme Currents Matrix (computed from Stoichiometric Matrix)")
    sympy_pprint(E)
    print()

    J = sympy.symbols('j0:%d' % nE)  # convex parameters
    # E_omega = sum([sympy.diag(*E.T[i])*J[i] for i in range(nE)], sympy.zeros( nR, nR ))
    E_omega_prim = sum([E[:, i].reshape(nR, 1)*J[i] for i in range(nE)], sympy.zeros(nR, 1))

    print("E_omega_prim matrix")
    sympy_pprint(E_omega_prim)
    print()

    # VJ_ = S * E_omega * K.T
    VJ = S * sympy.diag(*E_omega_prim) * K.T

    print("V(J) Matrix")
    sympy_pprint(VJ)
    print()

    stability_analysis(rd, duals_, not_duals, E, E_omega_prim, J, VJ, output,
                       modelname + "_sna",
                       instability_heuristic=instability_heuristic,
                       sum_mineurs=sum_mineurs,
                       max_mineur_search_stop=max_mineur_search_stop,
                       simplification_tries=simplification_tries,
                       num_samples=num_samples,
                       samples_folder=samples_folder)

def stability_analysis(
        rd,            # type: ReactionsDetails
        duals_,        # type: List[Tuple[int,int]]
        not_duals,     # type: List[int]
        E,             # type: Any
        E_omega_prim,  # type: Any
        J,             # type: Any
        VJ,            # type: Any
        output,        # type: Dict[str, bool]
        modelname,     # type: str
        enantiomers=None,           # type: Optional[Tuple[Tuple[int, ...], Tuple[int, ...]]]
        instability_heuristic="mineurs",  # type: str
        sum_mineurs=False,          # type: bool
        max_mineur_search_stop=5,   # type: int
        simplification_tries=1000,  # type: int
        num_samples=1,              # type: int
        samples_folder=None,        # type: Optional[str]
):
    # type: (...) -> None

    latex = output['latex']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=output['no_pretty'])

    R = rd.R
    S = rd.S
    ks = rd.ks
    # print("Nutreontureotn urntoreu ntrn ntr=)$=(=)·(=)", R.shape)
    nR = S.shape[1]
    nE = E.shape[1]  # number of extreme currents

    # print("\n==== SNA Analysis ====")

    all_restrictions_str = [sympy.sympify("j{} > 0".format(i)) for i in range(nE)]
    restrictions_poly = []

    # Adding dual restrictions to `all_restrictions_str` and `restrictions_poly`
    for l, r in duals_:
        js_l = E_omega_prim[l, 0]
        js_r = E_omega_prim[r, 0]
        # print(js_l, js_r)
        eq = js_l - js_r
        if not (isFloating(eq) and float(eq) < 0.00000001):
            all_restrictions_str.append("{} = {}".format(js_l, js_r))
            restrictions_poly.append((js_l - js_r).as_poly())

    if instability_heuristic == "mineurs":
        print("\n---- Mineur Analysis ----")
        restrictions = mineur_restrictions(VJ, sum_mineurs, max_mineur_search_stop, sympy_pprint)
        if restrictions is None:
            return
        print("---- --------------- ----")

    elif instability_heuristic == "trace-determinant":
        print('\n---- Analysis using Trace-Determinant inequalities ----\n')
        restrictions = tracedet_restrictions(VJ)
        if restrictions is None:
            return

        print('---- --------------------------------------------- ----\n')

    elif instability_heuristic == "characteristic-polynomial":
        print('\n---- Analysis using Characteristic Polynomial ----\n')
        restrictions = charpoly_restrictions(VJ, sympy_pprint)
        if restrictions is None:
            return

        print('---- ---------------------------------------- ----\n')
    else:
        print("*** ERROR: Sorry, but the heuristic '{}' does not exist! ***".format(
            instability_heuristic
        ))
        return

    inequalities = []
    for res in restrictions:
        res_ = poly_to_inequality(res.as_poly(), True)
        if isinstance(res_, bool):
            if res_ is False:
                print("The restriction: `{} < 0` can never be satisfied".format(res.as_expr()))
                return None
        else:
            inequalities.append(res.as_poly())
            all_restrictions_str.append(res_)

    print("Restrictions to solve")
    # print("Note: all the calculations that follow may be wrong, we don't ever use the "
    #       "restriction equations that result of dual pairs. Some j's may need to be equal "
    #       "to others")
    if latex:
#        all_restrictions_str = sympy.Matrix(all_restrictions_str) # Junio 7, 2.021. Este parece ser el problema, lo comenté y por ahora funciona ...
        sympy_pprint(all_restrictions_str)
    else:
        pprint.pprint(all_restrictions_str)
    print()

    sample = nonlinear_to_linear(
        inequalities,
        restrictions_poly,
        vars_=J,
        tries=simplification_tries,
        latex=latex,
        sympy_pprint=sympy_pprint,
        verbose=1
    )

    if sample is None:
        return

    eqs_and_ranges, js_replace, independent_vars, failure = sample

    if eqs_and_ranges is None:
        print("\nCouldn't find a solution after trying to sample from {} different "
              "simplified configurations".format(simplification_tries))

        if failure:
            print()
            print("Sorry, I couldn't calculate any valid value for js, something went wrong! T_T")
            print()
        else:
            print()
            print("No valid values for js were found. Try running the code again.")
            print("The equations seem not to be non-satisfiable (using the heuristic simplification)")
            print()

    else:
        # print("Vars that are independent to the restrictions")
        # print(independent_vars)
        if len(independent_vars) > 0:
            for v in independent_vars:
                eqs_and_ranges.append(
                    (v, sympy.Interval.open(0, oo))
                )

        print("Sampling {} points from js intervals".format(num_samples))

        E_omega_col = sum([(E[:, i]*J[i]).reshape((nR, 1))
                           for i in range(nE)], sympy.zeros(nR, 1))

        explicit_enantiomers = (
            True if enantiomers is None else enantiomers
        )  # type: Union[bool, Tuple[Tuple[int, ...], Tuple[int, ...]]]

        sample_xs, sample_ks = \
            sample_point_js(
                rd, duals_, not_duals,
                R, J, eqs_and_ranges + list(js_replace.items()),
                E_omega_col, n_samples=num_samples,
                explicit_enantiomers=explicit_enantiomers,
                verbose=samples_folder is None)

        if samples_folder is not None:
            if not path.isdir(samples_folder):
                makedirs(samples_folder)

            print("Saving sampled states to `{}`".format(samples_folder))
            for l, r in duals_:
                sample_ks[ks[r]] = sample_ks[ks[l]]

            for i in range(num_samples):
                save_sample(
                    rd, i+1,
                    {x: fs[i] for x, fs in sample_xs.items()},
                    {k: fs[i] for k, fs in sample_ks.items()},
                    samples_folder=samples_folder, modelname=modelname)

def mineur_restrictions(
        VJ,            # type: Any
        sum_mineurs,   # type: bool
        limit,         # type: int
        sympy_pprint,  # type: Any
):
    # type: (...) -> Optional[List[sympy.expr]]
    print("Limiting mineur analysis to mineurs of size smaller or equal to", limit)
    print()

    ms = mineurs(VJ, limit, sum_mineurs=sum_mineurs)

    if len(ms) == 0:
        print("There are no negative terms in the determinant of any mineur of size <=", limit)
        print("According to this criteria the system is stable.")
        return None

    for mineur in ms:
        print("Mineur:")
        sympy_pprint(mineur["mineur"])
        # print("Negative coefficients in mineur:")
        # sympy.pprint(mineur["negative coefficients"])
        print("Compounds number:", mineur["index"])
        print()

    return [mineur['mineur'] for mineur in ms]


def tracedet_restrictions(
        VJ,  # type: Any
):
    # type: (...) -> Optional[List[sympy.expr]]
    if VJ.shape[0] != 2:
        print("*** ERROR: Sorry, but the analysis 'trace-determinant' only works "
              "with V(j) matrices of size 2. ***")
        return None

    trace = VJ.trace().expand()
    determinant = VJ.det(method='berkowitz').expand()
    Discriminant = (trace**2 - 4*determinant)

    print("Trace:", trace)
    print("Determinant:", determinant)
    print("Discriminant:", Discriminant)
    print()
    print("Trace-determinant determines instability when: `determinant < 0` or `determinant > 0` and `trace > 0`") # Todo: Verificar que se esté cumpliendo la segunda condición. Al parecer solo se está considerando la primera `determinant < 0`. May 12th, 2021.
    print()

    return [determinant]
    return [trace] # ... May 12th, 2021.

def charpoly_restrictions(
        VJ,  # type: Any
        sympy_pprint,  # type: Any
):
    # type: (...) -> Optional[List[sympy.expr]]
    lambda_ = sympy.symbols('l')
    charpoly = VJ.charpoly(lambda_).simplify()

    print("Characteristic Polynomial of V(J)")
    sympy_pprint(charpoly)
    print()

    return [charpoly]


def mineurs(VJ, limit=0, sum_mineurs=False):
    # type: (sympy.Matrix, int, bool) -> List[sympy.Poly]
    VJ = -VJ
    sizeVJ = VJ.shape[0]
    if limit < 1:
        limit = sizeVJ
    mineurs = []
    # searching in determinants of principal matrices for negative terms
    for n in range(1, min(limit, sizeVJ)+1):
        mineurs_n = 0  # type: sympy.core.expr
        indices_n = []
        indices = list(combinations(range(sizeVJ), n))
        for i, index in enumerate(indices):
            mineur = VJ[index, index].det(method='berkowitz').expand()
            # print("index:", index)
            # print("mineur mat:", VJ[index, index])
            if mineur == 0:
                # don't even waste your time trying to find any negative
                # coefficients on this mineur
                continue
            mineur = mineur.as_poly()

            if sum_mineurs:
                mineurs_n += mineur
                indices_n.append(index)

                if i + 1 != len(indices):
                    continue
            else:
                mineurs_n = mineur

            negative_coefficients = {gens: coeff for gens, coeff in mineurs_n.terms() if coeff < 0}

            if len(negative_coefficients) > 0:
                mineurs.append({
                    'mineur': mineurs_n,
                    # 'mineur': mineurs_n * coeff,
                    # 'negative coefficients': negative_coefficients,
                    'index': indices_n if sum_mineurs else index
                })
    return mineurs
