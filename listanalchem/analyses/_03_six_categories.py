# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Six Categories (categorizing all reactions into 6 categories), see reference:

Montoya, J. A., Mejía, C., Bourdón, R. D., Cruz, E., & Ágreda, J. (2018). On the
Stability Analysis of Chiral Networks and the Emergence of Homochirality. MATCH Commun.
Math. Comput. Chem, 80(2), 311–344.
http://match.pmf.kg.ac.rs/electronic_versions/Match80/n2/match80n2_311-344.pdf
"""

from __future__ import print_function, division

import sympy
import pprint
import sys
from os import path, makedirs
import listanalchem.mypprint as lpprint

from listanalchem.tools import (  # type: ignore
    get_ranges_for_inequalities, ranges_and_eqs_to_strs, columns_console
)
from listanalchem.poly_tools import sample_eqs_and_ranges
from listanalchem.sampler import save_sample
from listanalchem.reduce_cas import ReduceCASNotInstalledException


# ===== ANALYSIS CONFIGURATION DESCRIPTION =====
name = 'Six Categories'
enabled = False
analysis_config = {
    'num-samples': {
        'type_input': int,
        'default': 1,
        'description': 'Number of states to sample',
        'metavar': 'INT'
    },
    'samples-folder': {
        'type_input': str,
        'default': None,
        'description': 'Directory to save all sampled states',
        'metavar': 'dir'
    }
}  # type: Dict[str, Any]
additional_description = \
    'Caution: Activating this analysis modifies the order of the reactions ' \
    'for all other analyses'


if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from listanalchem.reactions_parser import ReactionsDetails  # noqa: F401
        from typing import Tuple, Dict, List, Optional, Any  # noqa: F401
        import numpy as np  # noqa: F401
        TupExpr = Tuple[sympy.Expr, ...]


def xreplace_to_constant(obj, xs, c):
    # type: (sympy.Expr, Tuple[sympy.Symbol, ...], float) -> sympy.Expr
    return obj.xreplace({x: c for x in xs})


def obtain_sixcategies_vars(reactions, S, ks):
    # type: (Dict[str, List[Tuple[str, ...]]], np.array, Tuple[sympy.Symbol, ...]) -> TupExpr
    i = 0
    P = []
    for r in reactions['synthesis']:
        P.append(ks[i])
        i += 2
    D = []
    for r in reactions['fo-decomposition']:
        D.append(ks[i])
        i += 2
    A = []
    for r in reactions['autocatalytic']:
        A.append(ks[i])
        i += 2
    DE = []
    for r in reactions['so-decomposition']:
        DE.append(ks[i])
        i += 2
    E = []
    for r in reactions['no-enantioselective']:
        E.append(ks[i])
        i += 2
    C = []
    for r in reactions['inhibition']:
        if len(r) == 2:  # r is a dual pair
            Si = S[0, i] + S[1, i]  # equal in paper to Ci1 + Ci2 - 2
            C.append(int(Si)*ks[i])
            i += 2
        else:  # r is self-dual
            Si = S[0, i]  # equal in paper to Ci1 - 1
            C.append(int(Si)*ks[i])
            i += 1
    return sum(P), sum(D), sum(A), sum(DE), sum(E), -sum(C)


def print_reactions_sixcategories(reactions):
    # type: (Dict[str, List[Tuple[str, ...]]]) -> None
    print('{')
    for type, reacts in reactions.items():
        print("  '{}': [".format(type), end='')
        for reaction in reacts:
            print('\n      {},'.format(reaction), end='')
        print('],' if len(reacts) == 0 else '\n  ],')
    print('}')


def run_analysis(
        rd,      # type: ReactionsDetails
        config,  # type: Dict[str, Any]
        modelname,  # type: str
        output={'latex': False, 'no_pretty': False}  # type: Dict[str, bool]
):
    # type: (...) -> None

    reactions_andrescarolina = config['reactions-in-categories']
    num_samples    = config['num-samples']     # type: int
    samples_folder = config['samples-folder']  # type: Optional[str]

    S, R, xs, ks = rd.S, rd.R, rd.xs, rd.ks
    nS, nR = S.shape  # number of species and number of reactions

    latex = output['latex']
    sympy_pprint = lpprint.get_sympy_pprint(latex=latex, no_pretty=output['no_pretty'])

    # detecting if reactions were all of the right type (synthesis, ...) and had dual pairs
    isAndresCarolina = len(reactions_andrescarolina['others']) == 0

    print("*** Reactions grouped by the scheme of six types, according to reference [6] in"
          " the README.md file: ***")
    print_reactions_sixcategories(reactions_andrescarolina)
    print()

    if not isAndresCarolina:
        print("*** Failure for Six Categories algorithm ***")
        print("Some reactions don't have a dual pair or aren't of one of the known types:")
        print("synthesis, fo-decomposition, autocatalytic, so-decomposition, no-enantioselective, "
              "and inhibition")
        print()
        return

    print("*** Analysis according to reference [6] ***")
    print()

    # getting variables defined in Andrés & Carolina paper
    P, D, A, DE, E, C = obtain_sixcategies_vars(reactions_andrescarolina, S, ks)

    if len(reactions_andrescarolina['autocatalytic']) == 0:
        print("The network does not admit Frank states !!!")
        print("There are no autocatalytic reactions in the model, and they are required for "
              "the network to admit Frank states")
        print()
        return

    # Detecting if there aren't any negative stoichiometric coefficients from the
    # inhibition pairs Note that we are checking here for the contrary condition, that
    # there should be at least one positive coefficient, this is due C is defined as
    # -sum(stoichiometric * rate)
    if C == 0 or all([coeff < 0 for (ks_, coeff) in C.as_poly().terms()]):
        print("The network does not admit Frank states !!!")
        print("There needs to be at least one negative stoichiometric coefficient for an "
              "inhibition reaction so that the network admits Frank states")
        print()
        return

    # taking only the first two equations corresponding to the isomers
    diferential_system = (S*R)[:2, :]
    Jacob = diferential_system.jacobian(xs[:2])

    # getting number of self-dual reactions in inhibition group reactions
    self_duals_collision = len(
        [None for r in reactions_andrescarolina['inhibition'] if len(r) == 1])
    assert self_duals_collision <= 1, \
        "Number of self-dual pairs (of inhibition type) is bigger than 1, " \
        "this is not yet supported"
    # replacing all variables that are meant to be equal
    ks_for_andrescarolina = {
        ks[2*i+1]: ks[2*i]
        # TODO: this isn't considering the case there is more than one self-dual
        # pair (look assert up)
        for i in range(nR//2)
    }
    Jacob = Jacob.xreplace({xs[1]: xs[0]})  # replacing all x1 appearances with x0
    Jacob = Jacob.xreplace(ks_for_andrescarolina)

    print("Jacobian matrix (with dual pairs reaction rates equaled):")
    sympy_pprint(Jacob)

    assert Jacob[0, 0] == Jacob[1, 1] and Jacob[0, 1] == Jacob[1, 0], \
        "Jacobian matrix should be symetric of the form [[a,b],[b,a]]"

    a = Jacob[0, 0]
    b = Jacob[0, 1]

    def latexif(x):
        # type: (sympy.Expr) -> str
        return sympy.latex(x) if latex else str(x)  # type: ignore

    print()
    print("a =", latexif(a))
    print("b =", latexif(b))
    print("P, D, A, DE, E, C = {}, {}, {}, {}, {}, {}".format(P, D, A, DE, E, C))

    ineq1 = xreplace_to_constant(a - b > 0, xs, 1)
    ineq2 = xreplace_to_constant(a + b < 0, xs, 1)

    print()
    print("Inequalities (assuming concentrations equal to 1)")
    if latex:
        print(r"\begin{array}{ccc}")
        print(r"a - b > 0  &  \Rightarrow & ", sympy.latex(ineq1))
        print(r"a + b < 0  &  \Rightarrow & ", sympy.latex(ineq2))
        print(r"\end{array}")
    else:
        print("a - b > 0   =>  ", ineq1)
        print("a + b < 0   =>  ", ineq2)

    # testing the use of variables also works: P, E, D, DE, A, C to find inequalities
    assert ineq1 == (A - (D + 2*DE + E) > 0) \
        and ineq2 == (A + E - (D + 2*DE + 2*C) < 0), \
        "Jacobian method and 6 categories methods should yield the same"

    # creating equality as defined in paper
    eq = D + DE + C - E - A - P

    print("Equality:")
    print(latexif(eq), "= 0")
    print()

    # this uses redlog for calculations
    try:
        ranges_and_eqs = get_ranges_for_inequalities(
            [str(ineq1), str(ineq2), str(eq)+"=0"]
            + [str(k)+">0" for k in ks_for_andrescarolina.values()]
        )
    except ReduceCASNotInstalledException as msg:
        print(msg, file=sys.stderr)
        raise

    # Adding removed dual pairs
    ranges_and_eqs.append((ks[1], ks[0]))
    ranges_and_eqs.extend(ks_for_andrescarolina.items())

    # converting ranges and equalities into strings
    ranges = ranges_and_eqs_to_strs(ranges_and_eqs)

    print("Values that k's can take to satisfy inequalities and equality")
    if latex:
        print(r"\begin{align*}")
        for ra in ranges:
            var, ran = map(sympy.sympify, ra.split('='))
            print(sympy.latex(var), "&=", sympy.latex(ran), r"\\")
        print(r"\end{align*}")
    else:
        print("[", end="")
        for r_ in ranges:
            pprint.pprint(r_, width=columns_console)
            print(" ", end="")
        print("]")
    print()

    # Sampling
    sample_ks = sample_eqs_and_ranges(ranges_and_eqs, num_samples)
    sample_xs = {x: 1.0 for x in xs}

    if samples_folder is None:
        print("ks samples")
        print("  [", end="")
        for ki in sample_ks[0]:
            print("{} = {}".format(ki, [smp[ki] for smp in sample_ks]))
            print("   ", end="")
        print("]\n")

        print("xs samples")
        print("  [{} = {}\n   ".format(xs[0], sample_xs[xs[0]]), end="")
        for x in xs[1:]:
            print("{} = {}".format(x, sample_xs[x]))
            print("   ", end="")
        print("]\n")

    else:
        print("Saving sampled states to `{}`".format(samples_folder))

        if not path.isdir(samples_folder):
            makedirs(samples_folder)

        for i in range(num_samples):
            save_sample(
                rd, i+1,
                sample_xs, sample_ks[i],
                samples_folder=samples_folder,
                modelname=modelname + '_six_categories')
