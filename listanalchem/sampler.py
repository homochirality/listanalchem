# -*- coding: utf-8 -*-

# Copyright 2019 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

from collections import OrderedDict
import json
from os import path
import pprint
import sys

import sympy
import numpy as np

from listanalchem.poly_tools import sample_eqs_and_ranges, isFloating, poly_to_inequality
from listanalchem.tools import (  # type: ignore
    get_ranges_for_inequalities, ranges_and_eqs_to_strs,
)
from listanalchem.reduce_cas import \
    ReduceCASNotInstalledException, ParsingCASException, ReduceCASErrorSignalException
import listanalchem.poly_tools as poly_tools

from listanalchem.tools import interact # Para ver como van las cosas...******* .
import time


__all__ = ['sample_point_js', 'save_sample', 'nonlinear_to_linear']


if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from listanalchem.reactions_parser import ReactionsDetails
        from typing import Dict, Any, Optional, List, Callable, Tuple, Set, Union


def sample_point_js(
        rd,                # type: ReactionsDetails
        duals_,        # type: List[Tuple[int,int]]
        not_duals,     # type: List[int]
        R_duals_replaced,  # type: sympy.Matrix
        J,                 # type: Tuple[sympy.Symbol, ...]
        eqs_and_ranges,    # type: List[Tuple[sympy.Symbol, Union[float, sympy.Interval]]]
        E_omega_col,       # type: sympy.Matrix
        n_samples=1,       # type: int
        explicit_enantiomers=False,   # type: Union[bool, Tuple[Tuple[int, ...], Tuple[int, ...]]]
        verbose=True       # type: bool
):
    # type: (...) -> Tuple[Dict[sympy.Symbol, np.array[float]], Dict[sympy.Symbol, np.array[float]]]

    # print("rd: {} {}".format(type(rd), rd))
    # print("R_duals_replaced: {} {}".format(type(R_duals_replaced), R_duals_replaced))
    # print("J: {} {}".format(type(J), J))
    # print("eqs_and_ranges: {} {}".format(type(eqs_and_ranges), eqs_and_ranges))
    # print("E_omega_col: {} {}".format(type(E_omega_col), E_omega_col))
    # print("n_samples: {} {}".format(type(n_samples), n_samples))
    # print("verbose: {} {}".format(type(verbose), verbose))

    xs, S = rd.xs, rd.S
    nS, nR = S.shape
    # duals_, not_duals = rd.get_duals()

    # print("xs: {} {}".format(type(xs), xs))
    # print("S: {} {}".format(type(S), S))
    # print("nS: {} {}".format(type(nS), nS))
    # print("nR: {} {}".format(type(nR), nR))
    # print("duals_: {} {}".format(type(duals_), duals_))
    # print("not_duals: {} {}".format(type(not_duals), not_duals))

    # sampling js
    sample_js = sample_eqs_and_ranges(eqs_and_ranges, n_samples)

    # print("sample_js:", sample_js)

    if verbose:
        print("  Random valid sample for j's")
        print("  [", end="")
        for j in J:
            # TODO: Reconstructing a arrays that were already calculated
            s_j = np.array([sample_js[i][j] for i in range(n_samples)], dtype=np.float64)
            print("{} = {}".format(j, s_j))
            print("   ", end="")
        print("]\n")

    R_values = [None for _ in range(n_samples)]  # type: List[Any]
    for i in range(n_samples):
        R_values[i] = np.array([E_omega_col.xreplace(sample_js[i])], dtype='float').reshape((nR, 1))

    sample_xs = {}
    for x in xs:
      # sample_xs[x] = 0.01 # Se deja fijo en 0,01, en agosto 20 de 2.020. No funciona.
        sample_xs[x] = 0.01 - np.random.uniform(0.001, 0.01, size=n_samples) # Entre 0.001 y 0.01
      # sample_xs[x] = 1 - np.random.uniform(0, 1, size=n_samples) # Antiguo.
    if explicit_enantiomers:
        if explicit_enantiomers is True:
            sample_xs[xs[1]] = sample_xs[xs[0]]
        elif isinstance(explicit_enantiomers, (tuple, list)):
            for il, ir in zip(*explicit_enantiomers):
                sample_xs[xs[il]] = sample_xs[xs[ir]]
    else:
        del sample_xs[xs[1]]

    if verbose:
        print("  xs samples")
        print("  [{} = {}\n   ".format(xs[0], sample_xs[xs[0]]), end="")
        start = 1 if explicit_enantiomers else 2
        for x in xs[start:]:
            print("{} = {}".format(x, sample_xs[x]))
            print("   ", end="")
        print("]\n")

    # NOTICE how these equations can be solved by hand now, all xs are independent
    # here, no need to apply the trick of the logarithm!
    R_xs_sampled = [None for _ in range(n_samples)]  # type: List[Any]
    for i in range(n_samples):
        R_xs_sampled[i] = R_duals_replaced.xreplace({x: s_x[i] for x, s_x in sample_xs.items()})

    if verbose:
        print("  Equations to solve to get a ks sample")

    sample_ks = {}
    for i in [i for (i, j) in duals_] + not_duals:
        R_values_i_np = np.array(
            [R_values[k][i, 0] for k in range(n_samples)],
            dtype=np.float64)
        if verbose:
            print("  {} = {}".format(R_duals_replaced[i, 0], R_values_i_np))

        if isinstance(R_xs_sampled[0][i, 0], sympy.Mul):
            factor = np.zeros(n_samples)
            _, ki = R_xs_sampled[0][i, 0].args
            # print("R_xs_sampled: ", R_xs_sampled)
            for k in range(n_samples):
                factor[k], _ = R_xs_sampled[k][i, 0].args
            sample_ks[ki] = R_values_i_np / factor
        elif isinstance(R_xs_sampled[0][i, 0], sympy.Symbol):
            ki = R_xs_sampled[0][i, 0]
            sample_ks[ki] = R_values_i_np

    if verbose:
        print()
        print("  ks samples")
        print("  [", end="")
        for ki, value in sample_ks.items():
            print("{} = {}".format(ki, value))
            print("   ", end="")
        print("]\n")

    return sample_xs, sample_ks


def save_sample(
        rd,  # type: ReactionsDetails
        sample_i,   # type: int
        sample_xs,  # type: Dict[sympy.Symbol, np.array[float]]
        sample_ks,  # type: Dict[sympy.Symbol, np.array[float]]
        hyperbolic=None,  # type: Optional[bool]
        samples_folder='samples',  # type: str
        modelname='model'  # type: str
):
    # type: (...) -> None
    sample_file = "{}_{}".format(modelname, str(sample_i).rjust(3, '0'))
    if samples_folder:
        sample_file = path.join(samples_folder, sample_file)
    if hyperbolic is not None:
        sample_file += "_{}hyperbolic".format('' if hyperbolic else 'no-')
    sample_file += '.simu.json'

    xs, ks, S = rd.xs, rd.ks, rd.S
    nS, nR = S.shape
    species, reactions = rd.species, rd.get_reactions()
    duals_, not_duals = rd.get_duals()

    content = {
        "ATOL": 1e-15, "Delta": 0.1, "RTOL": 1e-15, "T_0": 0, "Tmax": 2000, "version": 0.1
    }  # type: Dict[str, Any]

    content['concentrations'] = OrderedDict()
    content['concentrations'][species[0]] = float(sample_xs[xs[0]])

    sample_x1 = sample_xs[xs[1 if xs[1] in sample_xs else 0]]
    content['concentrations'][species[1]] = float(sample_x1)

    for i in range(2, nS):
        content['concentrations'][species[i]] = float(sample_xs[xs[i]])

    content['reactions'] = OrderedDict()
    for i, j in duals_:
        content['reactions']['R{}'.format(i+1)] = reactions[i]
        content['reactions']['R{}'.format(j+1)] = reactions[j]
    for i in not_duals:
        content['reactions']['R{}'.format(i+1)] = reactions[i]

    content['rates'] = OrderedDict()
    for i, j in duals_:
        value_ki = sample_ks[ks[i]]
        content['rates']['R{}'.format(i+1)] = float(value_ki)
        content['rates']['R{}'.format(j+1)] = float(value_ki)

    for i in not_duals:
        content['rates']['R{}'.format(i+1)] = float(sample_ks[ks[i]])

    with open(sample_file, 'w') as f:
        json.dump(content, f, indent=2)


def nonlinear_to_linear(
        inequalities,       # type: List[sympy.Poly]
        restrictions,       # type: List[sympy.Poly]
        vars_,              # type: List[Any]
        tries,              # type: int
        latex=False,        # type: bool
        sympy_pprint=None,  # type: Optional[Callable[[Any], None]]
        verbose=False,      # type: int
):
    # type: (...) -> Optional[Tuple[Optional[List[Any]], Dict[Any, Any], Set[Any], bool]]
    """
    Given a set of polynomial inequalities with variables x1, x2, ..., xn, this function
    finds values for some variables (xi) such that the inequalities are satisfied and are
    lineal. Then it computes the intervals where the variables satisfy the inequalities.

    For example, given::

        - a + b*c < 0
        -c**2 < 0

    `nonlinear_to_linear` finds a value for `c`::

        c = 0.2

    Which transforms the set of inequalities into::

        - a + 0.2*b < 0
        -0.04 < 0

    And, from it computes the following intervals::

        a = (0, oo)
        b = (0, a/0.2)

    Note: All variables are assumed to be positive

    :param inequalities: List of polynomials we wish to be negative
    :param restrictions: Linear restrictions on the inequalities
    :param vars_:        List of variables in the polynomial list
    """

    if verbose > 2:
        print("inequalities:", [ineq.as_expr() < 0 for ineq in inequalities])
        print("restrictions:", ["{} = 0".format(res.as_expr()) for res in restrictions])

    if verbose:
        print("Trying to find a solution with the restrictions")

    failure = False

    for i in range(tries):  # tries several times the same procudere
        # if it was able to find a solution, we don't need any more tries :D
        if verbose and i > 0 and i % 100 == 0:
            print('.', end='')
            sys.stdout.flush()

        # Try with a different simplification of the restrictions every 100 iterations
        if i % 100 == 0:
            simplified = \
                __simplify_ineqs(inequalities, restrictions, vars_, verbose)

            if simplified is None:
                return None

            vars_to_replace, vars_to_solve, eqs_and_ranges_stage_1 = simplified

        # Random uniform sample of variables trying to simplify set of equations
        if eqs_and_ranges_stage_1 is not None:
            js_replace = sample_eqs_and_ranges(eqs_and_ranges_stage_1)[0]
        else:
            end = poly_tools.end_oo
            js_replace = {j: end-np.random.uniform(0, end) for j in vars_to_replace}

        if verbose > 2:
            print("Trying to find solution with values")
            print("js_replace:", js_replace)
            print()

        vars_in_ineqs = set()  # type: Set[sympy.symbol]
        all_restrictions_str = []
        for res in restrictions:
            res_new = res.as_expr().xreplace(js_replace)
            # print("res_new:", res_new)
            if not (isFloating(res_new) and float(res_new) < 0.00000001):
                all_restrictions_str.append("{} = 0".format(res_new))
                vars_in_ineqs.update(res_new.free_symbols)

        for ineq in inequalities:
            # print("ineq:", ineq)
            # print("js_replace:", js_replace)
            m_replaced = ineq.as_expr().xreplace(js_replace)
            if not isFloating(m_replaced):
                m_replaced = m_replaced.as_poly()
                # print("print ineq:", poly_to_inequality(m_replaced))
                ineq = poly_to_inequality(m_replaced)
                # if isinstance(ineq, bool):
                #     if ineq is False:
                #         continue
                # else:
                #     all_restrictions_str.append(ineq)
                all_restrictions_str.append(str(ineq))
                vars_in_ineqs.update(ineq.free_symbols)

        all_restrictions_str.extend([str(v > 0) for v in vars_in_ineqs])

        # Calculating solutions
        # this uses redlog for calculations
        try:
            if all_restrictions_str:
                eqs_and_ranges = get_ranges_for_inequalities(
                    [str(inq) for inq in all_restrictions_str]
                    # , verbose=True
                )
            else:
                eqs_and_ranges = []
            # print("eqs_and_ranges", eqs_and_ranges)
        except ReduceCASNotInstalledException as msg:
            print(msg, file=sys.stderr)
            raise

        except ParsingCASException:  # as msg:
            # print(msg)
            eqs_and_ranges = None
            failure = True

        except ReduceCASErrorSignalException:  # as msg:
            # print(msg)
            eqs_and_ranges = None
            failure = True

        if eqs_and_ranges is not None:
            # Checking if some variable must be zero, which can never be
            # (This may happen because Reduce can only solve inequalities with >= and never >, :S)
            # This should already been checked in `get_ranges_for_inequalities`
            if all(r[1] != 0 for r in eqs_and_ranges):
                break

    print()

    if eqs_and_ranges is not None:
        if verbose:
            print("Found a solution on try number {} of {}".format(i+1, tries))
            print()
            print("(Randomly) Chosen variable values:")
            if latex:
                print(r"\begin{align*}")
                for j, v in js_replace.items():
                    print(sympy.latex(j), "&=", sympy.latex(v), r"\\")
                print(r"\end{align*}")
            else:
                print("[", end="")
                for j, v in js_replace.items():
                    print("{}: {},".format(j, v), end="\n ")
                print("]", end="\n\n")

        if verbose > 1:
            print("SNA inequalities (simplified)")
            if latex:
                all_restrictions_str = sympy.Matrix(all_restrictions_str)
                if sympy_pprint is not None:
                    sympy_pprint(all_restrictions_str)
            else:
                pprint.pprint(all_restrictions_str)
            print()

        if verbose:
            ranges = ranges_and_eqs_to_strs(eqs_and_ranges)

            print("The values that the remaining variables can take, given the already chosen (see above)"
                  " values for variables {}".format(vars_to_replace))

            if latex:
                print(r"\begin{align*}")
                for r_ in ranges:
                    var, ran = map(sympy.sympify, r_.split('='))
                    print(sympy.latex(var), "&=", sympy.latex(ran), r"\\")
                print(r"\end{align*}")
            else:
                print("[", end="")
                for r_ in ranges:
                    print("{}".format(r_))
                    print(" ", end="")
                print("]")
            print()

    independent_vars = set(vars_to_solve) - set(vars_in_ineqs)
    return eqs_and_ranges, js_replace, independent_vars, failure


def __simplify_ineqs(
        inequalities,  # type: List[sympy.Poly]
        restrictions,  # type: List[sympy.Poly]
        vars_,         # type: List[sympy.Symbol]
        verbose,       # type: int
):
    # type: (...) -> Optional[Tuple[List[Any], List[Any], Optional[List[Tuple[Any, Any]]]]]

    if inequalities:
        if verbose > 1:
            print("Trying to find variables to replace for real values in order to simplify "
                  "the inequalities.")
        vars_to_replace, _, deleted_eqs = poly_tools.gens2replace(
            inequalities,
            restrictions,
            verbose=verbose > 1)
    else:
        vars_to_replace = []
        deleted_eqs = []

    vars_to_solve = list(set(vars_).difference(set(vars_to_replace)))

    if verbose > 1:
        print("Variables to replace for real values in restrictions")
        print(vars_to_replace)
        print()
        print("Deleted restrictions from list:", deleted_eqs)
        print('"Free" variables:', vars_to_solve)
        print()

    if len(vars_to_solve) == 0:
        print("I couldn't simplify the set of inequalities :(")
        print("inequalities:", [ineq.as_expr() < 0 for ineq in inequalities])
        print("restrictions:", ["{} = 0".format(res.as_expr()) for res in restrictions])
        print()
        return None

    # if one of the equations was used in the simplification process, then the first
    # sample of js (random sample) is restricted by that equation
    if deleted_eqs:
        sample_stage_1 = []
        for i in deleted_eqs:
            sample_stage_1.append(restrictions[i])

        if verbose > 1:
            print("We need to take some restrictions into account for the first stage of the\n"
                  "heuristic sampling.")
            print("The restrictions are:")

            print("[")
            for eq in sample_stage_1:
                print(' ', eq.as_expr(), '= 0 ,')
            print("]")

        # Calculating solutions
        # this uses redlog for calculations
        try:
            eqs_and_ranges_stage_1 = get_ranges_for_inequalities(
                ['{} = 0'.format(eq.as_expr()) for eq in sample_stage_1]
                + ['{} > 0'.format(var) for var in vars_to_replace]
                # , verbose=True
            )
        except ReduceCASNotInstalledException as msg:
            print(msg, file=sys.stderr)
            raise

        if eqs_and_ranges_stage_1 is None:
            print("The system has no solutions :S. The lineal restrictions are unsolvable")
            return None
        elif verbose > 1:
            print("Values for variables in restrictions:")
            print("[")
            for eq in eqs_and_ranges_stage_1:
                print(' ', eq, ',')
            print("]")

    return vars_to_replace, vars_to_solve, (eqs_and_ranges_stage_1 if deleted_eqs else None)
