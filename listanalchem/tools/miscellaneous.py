# -*- coding: utf-8 -*-

# Copyright 2017-2019 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import listanalchem.mypprint as lpprint
import sympy
from distutils.version import LooseVersion
import stopit
import sys
import os

# from functools import reduce
# from sympy.parsing.sympy_parser import parse_expr
import listanalchem.reduce_cas as redcas

# Type imports for type checking with MYPY
if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from typing import Any, List, Tuple, Dict, Optional, Union, Iterable  # noqa: F401
        EigenType = List[Tuple[sympy.Expr, int, Any]]
        OptionalRange = Union[sympy.Interval, sympy.Poly, float]


if os.name == 'posix':
    try:
        columns_console = int(os.popen('stty size', 'r').read().split()[1])
    except:  # noqa: E722
        columns_console = 80
else:
    columns_console = 80


def get_ranges_for_inequalities(ineqs_list, verbose=False):
    # type: (List[str], bool) -> Optional[List[Tuple[sympy.Symbol, OptionalRange]]]
    with redcas.ReduceCAS(verbose=verbose) as red:
        result = red.get_ranges_for_inequalities(ineqs_list)
    return result


def ranges_and_eqs_to_strs(eqs_and_ranges):
    # type: (List[Tuple[sympy.Symbol, Any]]) -> List[str]
    return [
        str(ks) + " = " + str(e)
        for ks, e in eqs_and_ranges
    ]


# `error_when_incomplete` was added to sympy in commit
# 88bca19948a9f937be21de14e72069017ab5cec2, for more info look at
# https://github.com/sympy/sympy/commit/88bca19948a9f937be21de14e72069017ab5cec2
if LooseVersion(sympy.__version__) < LooseVersion("1.1.1"):
    eigen_kargs = {}  # type: Dict[str, Any]
else:
    eigen_kargs = {"error_when_incomplete": False}


@stopit.threading_timeoutable(default=None)  # type: ignore
def get_eigenvects(matrix):
    # type: (sympy.Matrix) -> EigenType
    return matrix.eigenvects(**eigen_kargs)  # type: ignore


@stopit.threading_timeoutable(default=None)  # type: ignore
def get_eigenvals(matrix):
    # type: (sympy.Matrix) -> Dict[sympy.Expr, int]
    return matrix.eigenvals(**eigen_kargs)  # type: ignore


def get_and_print_eigenvects(
        matrix, output={'latex': False, 'no_pretty': False}, timeout=300):
    # type: (sympy.Matrix, Dict[str, bool], int) -> Optional[EigenType]
    sympy_pprint = lpprint.get_sympy_pprint(latex=output['latex'], no_pretty=output['no_pretty'])

    # trying to get eigenvectors, if it fails (times out) it will try to find only the eigenvalues
    eigenvects_exception = False
    try:
        eigenvects = get_eigenvects(matrix, timeout=timeout)  # type: Optional[EigenType]
    except Exception as e:
        print("Couldn't calculate eigenvectors. Some internal exception occurred :S",
              file=sys.stderr)
        print(e, file=sys.stderr)
        eigenvects = None
        eigenvects_exception = True

    if eigenvects is None:
        if not eigenvects_exception:
            print("Error: Eigenvectors calculation took more than {} secs!".format(timeout))
        print("Trying to calculate only eigenvalues now")
        eigenvals = get_eigenvals(matrix, timeout=timeout)
        if eigenvals is None:
            print("I'm really sorry, but I couldn't calculate the Eigenvectors")
            print()
            return None
        else:
            eigenvects = [(eig, mul, None) for (eig, mul) in eigenvals.items()]
            print()

    # making sure the sum of all eigenvectors' algebraic_mul is the same as the size of the matrix
    if matrix.shape[0] != sum(e[1] for e in eigenvects):
        print("Warning: not all eigenvectors could be calculated :S\n")
    for i, (eigenvalue, algebraic_mul, eigenvector) in enumerate(eigenvects):
        print("Eigenvalue", i+1, end=" ")
        print("(with algebraic multiplicity of:", algebraic_mul, end=")\n")
        sympy_pprint(eigenvalue)
        if eigenvector is not None:
            print("Eigenvector of eigenvalue", i+1)
            sympy_pprint(eigenvector)
    print()

    return eigenvects


def interact(locals, globals):
    # type: (Dict[str, Any], Dict[str, Any]) -> None
    locals.update(globals)
    import code
    code.InteractiveConsole(locals=locals).interact()
