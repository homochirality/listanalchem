# -*- coding: utf-8 -*-

# This Python code is a traslation from the original C++ COPASI code to
# calculate Flux Modes. The original code was released under the
# Artistic License which is reproduced in its totality in the
# LICENSE.artistic.txt file

# Copyright (C) 2017 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., University of Heidelberg, and University of
# of Connecticut School of Medicine.
# All rights reserved.
#
# Copyright (C) 2010 - 2016 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., University of Heidelberg, and The University
# of Manchester.
# All rights reserved.
#
# Copyright (C) 2008 - 2009 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc., EML Research, gGmbH, University of Heidelberg,
# and The University of Manchester.
# All rights reserved.
#
# Copyright (C) 2002 - 2007 by Pedro Mendes, Virginia Tech Intellectual
# Properties, Inc. and EML Research, gGmbH.
# All rights reserved.
#
# Created for Copasi by Stefan Hoops 2002-05-08
# (C) Stefan Hoops 2002

from __future__ import print_function, division

import numpy as np
from math import floor
import sys
# from functools import total_ordering

if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from typing import Tuple, Optional, List, Iterator, Any

int_types = (int, long) if sys.version[0] == 2 else (int,)


inf = float('inf')


# Total ordering makes the calculations slightly slower
# @total_ordering
class FluxScore:
    def __init__(self, fluxModes):
        # type: (np.ndarray) -> None
        assert len(fluxModes.shape) == 1
        self.score = fluxModes != 0
        assert isinstance(self.score, np.ndarray)
        assert self.score.dtype == np.bool

    def __eq__(self, other):
        # type: (object) -> bool
        if isinstance(other, FluxScore):
            return np.all(self.score == other.score)  # type: ignore
        return NotImplemented

    def __le__(self, other):
        # type: (FluxScore) -> bool

        a = self.score
        b = other.score
        # source: https://stackoverflow.com/a/38052618
        if np.any(a != (a & b)):
            return False
        return True

    def __str__(self):
        # type: () -> str
        return ''.join(('1' if v else '0') for v in self.score)


class TableauLine:
    def __init__(self, reaction, reactionCounter=None, reactionNumber=None, fluxMode=None):
        # type: (np.ndarray, Optional[int], Optional[int], Optional[np.ndarray]) -> None
        self.reaction = reaction

        if fluxMode is None:
            assert isinstance(reactionCounter, int_types)
            assert isinstance(reactionNumber, int_types)
            self.fluxMode = np.zeros((reactionNumber,))
            self.fluxMode[reactionCounter] = 1
        else:
            self.fluxMode = fluxMode
            assert reactionCounter is None
            assert reactionNumber is None

        self.score = FluxScore(self.fluxMode)

    def __eq__(self, value):
        # type: (Any) -> bool
        if isinstance(value, TableauLine):
            return (  # type: ignore
                np.all(self.reaction == value.reaction)
                and np.all(self.score == value.score)
                and np.all(self.fluxMode == value.fluxMode)
            )
        return NotImplemented

    def getMultiplier(self, idx):
        # type: (int) -> float
        return self.reaction[-idx-1]  # type: ignore

    def __str__(self):
        # type: () -> str
        return str(self.reaction) + '\t' + str(self.fluxMode)

    def __reduce(self, r1, r2):
        # type: (float, float) -> Tuple[float, float]
        if floor(r1) != r1 or floor(r2) != r2:
            return r1, r2

        gcd1 = int(abs(r1))
        gcd2 = int(abs(r2))

        while gcd1 != gcd2:
            if gcd1 > gcd2:
                gcd1 %= gcd2
                if gcd1 == 0:
                    gcd1 = gcd2
            else:
                gcd2 %= gcd1
                if gcd2 == 0:
                    gcd2 = gcd1

        return r1 / gcd1, r2 / gcd2

    def mixWithNewTableauLine(self, m1, other, m2):
        # type: (float, TableauLine, float) -> TableauLine
        m1, m2 = self.__reduce(m1, m2)

        reaction = m1 * self.reaction + m2 * other.reaction
        fluxMode = m1 * self.fluxMode + m2 * other.fluxMode

        return TableauLine(reaction, fluxMode=fluxMode)


class TableauMatrix:
    def __init__(self, stoi=None):
        # type: (Optional[np.ndarray]) -> None
        self.line = []  # type: List[TableauLine]
        if stoi is None:
            return

        assert len(stoi.shape) == 2
        nR = stoi.shape[0]
        for rc, reaction in enumerate(stoi):
            self.line.append(
                TableauLine(reaction, reactionCounter=rc, reactionNumber=nR))

    def __len__(self):
        # type: () -> int
        return len(self.line)

    def __iter__(self):
        # type: () -> Iterator[TableauLine]
        return self.line.__iter__()

    def __getitem__(self, key):
        # type: (int) -> TableauLine
        return self.line[key]

    def addLine(self, src, check=True):
        # type: (TableauLine, bool) -> None
        if not check or self.isValid(src):
            self.line.append(src)

    def popLine(self, idx):
        # type: (int) -> None
        self.line.pop(idx)

    def removeLine(self, src):
        # type: (TableauLine) -> None
        self.line.remove(src)

    def isValid(self, src):
        # type: (TableauLine) -> bool
        for l in self.line:
            if l.score <= src.score:
                # print("this happened")
                # print("l:", l)
                # print("src:", src)
                return False

        i = 0
        while i != len(self.line):
            if src.score <= self.line[i].score:
                self.line.pop(i)
            else:
                i += 1

        return True

    def __str__(self):
        # type: () -> str
        ss = ['Tableau Matrix (size of {})'.format(len(self.line))]
        for l in self.line:
            ss.append(str(l))
        return '\n'.join(ss)


# class FluxMode:
#     def __init__(self, line):
#         # type: (TableauLine) -> None
#         self.reactions = {}  # type: Dict[int, float]
#
#         for i, m in enumerate(line.fluxMode):
#             if m:
#                 self.reactions[i] = float(m)
#         self.size = i + 1
#
#     def __str__(self):
#         # type: () -> str
#         return str(self.reactions)
#
#     def __repr__(self):
#         # type: () -> str
#         return "FluxMode{}".format([
#             self.reactions[i] if i in self.reactions else 0 for i in range(self.size)])


class CFAlgorithm:
    def __init__(self, StoiMatrix):
        # type: (np.ndarray) -> None
        assert len(StoiMatrix.shape) == 2
        self.stoi = StoiMatrix
        # self.fluxModes = []  # type: List[FluxMode]
        self.step = 0
        self.maxStep = StoiMatrix.shape[1]  # num of cols

    def calculateFluxModes(self):
        # type: () -> None
        if self.stoi.shape[0]:
            self.currTableau = TableauMatrix(self.stoi)

            self.indexSet = list(range(self.maxStep))
            # print("indexSet", self.indexSet)
            # print("currTableau", self.currTableau)

            while self.findMinimalCombinationIndex():
                # print("=== Inside next tableau ===")
                # print("self.step:", self.step)
                self.calculateNextTableau()
                # print("=== Outside next tableau ===\n")
                # print(self.currTableau)

            self.buildFluxModes()

    def calculateNextTableau(self):
        # type: () -> None
        nextTableau = TableauMatrix()

        i = 0
        while i < len(self.currTableau):
            line = self.currTableau[i]
            if line.getMultiplier(self.step) == 0.0:
                nextTableau.addLine(line, False)
                self.currTableau.popLine(i)
            else:
                i += 1

        # print("currTableau", self.currTableau)
        # print("nextTableau", nextTableau)

        while len(self.currTableau) != 0:
            j = 1
            linea = self.currTableau[0]
            mb = linea.getMultiplier(self.step)

            if mb < 0.0:
                mb *= -1
                sign = 1
            else:
                sign = -1

            while j < len(self.currTableau):
                lineb = self.currTableau[j]
                ma = sign * lineb.getMultiplier(self.step)
                if ma > 0.0:
                    nextTableau.addLine(linea.mixWithNewTableauLine(ma, lineb, mb))

                j += 1

            self.currTableau.popLine(0)

        # print("currTableau", self.currTableau)
        # print("nextTableau", nextTableau)

        self.currTableau = nextTableau

    def buildFluxModes(self):
        # type: () -> None
        # self.fluxModes = [FluxMode(a) for a in self.currTableau]
        self.fluxModes = np.concatenate(
            [a.fluxMode.reshape((-1, 1)) for a in self.currTableau],
            axis=1)
        self.fluxModes = self.fluxModes[:, np.lexsort(self.fluxModes)]

    def findMinimalCombinationIndex(self):
        # type: () -> bool
        minCombine = inf
        combine = 0.0
        minIdx = 0

        idxSize = len(self.indexSet)
        if idxSize == 0:
            return False
        elif idxSize == 1:
            self.step = self.indexSet[0]
            self.indexSet.pop()
            return True

        for i, idx in enumerate(self.indexSet):
            combine = self.calculateCombinations(idx)
            if combine < minCombine:
                minCombine = combine
                minIdx = i

            if combine == 0:
                break

        self.step = self.indexSet[minIdx]
        self.indexSet.pop(minIdx)
        return True

    def calculateCombinations(self, idx):
        # type: (int) -> float
        posIrr = 0
        negIrr = 0

        for line in self.currTableau:
            mult = line.getMultiplier(idx)
            if mult < 0:
                negIrr += 1
            elif mult > 0:
                posIrr += 1

        return posIrr + negIrr


def extreme_currents(SC, verbose=False):
    # type: (np.ndarray, bool) -> np.ndarray
    alg = CFAlgorithm(SC.T)
    alg.calculateFluxModes()
    return alg.fluxModes


if __name__ == '__main__':
    score1 = FluxScore(np.array([0, 0, 1, 1, 1]))
    score2 = FluxScore(np.array([0, 1, 1, 1, 1]))
    print(score1)
    print(score2)
    print("score1 < score2:", score1 <= score2)
    # print(score1 > score2)

    tl1  = TableauLine(np.array([1, 0, 2, 1]), reactionCounter=2, reactionNumber=7)
    tl2  = TableauLine(np.array([-2, 2, 0, 0]), reactionCounter=5, reactionNumber=7)
    tlMix = tl1.mixWithNewTableauLine(6, tl2, 2)
    print("tl1:", tl1)
    print("multiplier 3:", tl1.getMultiplier(3))
    print("tl2:", tl2)
    print("tlMix:", tlMix)
    print("tlMix score:", tlMix.score)

    tm1 = TableauMatrix(np.array([
        [1, 0, 2, 1],
        [-1, 0, 1, 0],
        [1, 1, 0, 3]
    ]))

    print("tm1:")
    print(tm1)

    alg = CFAlgorithm(np.array([
        # Frank
        # [1,  0],
        # [0,  1],
        # [-1, -1]

        # Brusselator
        # [1, 0],
        # [1, -1],
        # [-1, 1],
        # [-1, 0],

        # Replicator - with rate constant restrictions (dual pairs)
        # [+1,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0],
        # [-1,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0],
        # [+0,  1,  0,  0, -1, -1,  0,  0,  0,  0,  0],
        # [+0, -1,  0,  0,  1,  0, -1,  0,  0,  0,  0],
        # [+0,  0,  1,  0, -1,  0,  0,  1,  0,  0,  0],
        # [+0,  0, -1,  0,  1,  0,  0,  0,  1,  0,  0],
        # [+0,  0,  0,  1, -1,  0,  0, -1,  0,  0,  0],
        # [+0,  0,  0, -1,  1,  0,  0,  0, -1,  0,  0],
        # [-1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0],
        # [+0,  0, -1,  0,  0,  0,  0,  0,  0, -1,  1],
        # [+0, -1,  0,  0,  0,  0,  0,  0,  0,  0, -1],
        # [+0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0],
        # [+0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0],
        # [+0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0]

        # APED - with rate constant restrictions (dual pairs)
        # [-1,  1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0],
        # [+0,  0, -1,  1,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0],
        # [+1, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0],
        # [+0,  0,  1, -1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0],
        # [-1, -1,  0,  0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0],
        # [-1,  0,  0, -1,  0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0],
        # [+0, -1, -1,  0,  0,  0,  1,  0,  0,  0,  0, -1,  0,  0,  0,  0],
        # [+0,  0, -1, -1,  0,  0,  0,  1,  0,  0, -1,  0,  0,  0,  0,  0],
        # [+2,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0],
        # [+1,  0,  1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0],
        # [+1,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  0],
        # [+0,  0,  2,  0,  0,  0,  0, -1,  0,  0,  0,  0, -1,  0,  0,  0],
        # [+0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  1,  0],
        # [+0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  1],
        # [+0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1],
        # [+0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0]

        # Iwamoto Iwamoto-Imperfect-Without-A-Explicit-Enantiomers-a6 - with rate constant
        # restrictions
        [+1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0],
        [-1,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0],
        [+0,  1,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0],
        [+0, -1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0],
        [+0,  1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0],
        [+0, -1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0],
        [+1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0],
        [-1,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0],
        [-1,  0, -1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0],
        [+1,  0,  1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0],
        [-1,  0,  0, -1,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0],
        [+1,  0,  0,  1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0],
        [+0, -1,  0, -1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0],
        [+0,  1,  0,  1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0],
        [+0, -1, -1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0],
        [+0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0],
        [+0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0],
        [+0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1],
        [+0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0],
        [+0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1]
    ]))

    print("Stoichiometric matrix")
    print(alg.stoi)

    print("Start calculating flux modes")
    alg.calculateFluxModes()
    print("Finished calculating flux modes")
    print(alg.fluxModes)
    # print(alg.currTableau)
