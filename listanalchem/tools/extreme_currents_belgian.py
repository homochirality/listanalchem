# TODO(helq): ADD COPYRIGHT NOTICE!!!

import sys
import numpy as np

# Type imports for type checking with MYPY
if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from typing import Any, List, Tuple, Dict, Optional, Union, Iterable  # noqa: F401


def extreme_currents(SC_, verbose=False):
    # type: (np.ndarray, bool) -> np.ndarray
    Tiny = 1e-10
    Nr = SC_.shape[1]  # number of reactions
    Ra = np.linalg.matrix_rank(SC_)

    # SC_singular = False
    # Checking if stoichiometric matrix is singular
    # if it is, some repeated rows are eliminated from the matrix
    if Ra < SC_.shape[0]:
        # SC_singular = True
        non_singular_rows, removed_rows = __rows_to_keep(SC_)
        SC = SC_[non_singular_rows]
        if verbose:
            print("NOTE!: Extreme currents of stoichiometric matrix will be computed "
                  "from reduced stoichiometric matrix")
            eliminated_rows = SC_.shape[0] - SC.shape[0]
            print("Stoichiometric matrix reduced from:")
            print(SC_)
            print("into ({} row{} eliminated):".format(
                eliminated_rows, 's' if eliminated_rows != 1 else ''))
            print(SC)
            print()
            # This should always, always happen. If it doesn't happen it should probably
            # raise an Exception.
            if len(removed_rows) > 0:
                print("Rows eliminated ({}):".format(removed_rows))
                print(SC_[removed_rows])
                print()
    else:
        SC = SC_

    if Ra == SC.shape[1]:  # SC is a non-singular square matrix
        return None

    N1 = Nr-SC.shape[0]-1

    B = np.zeros((Nr, 1))
    B[-1] = 1
    Nsol = 0
    E = []  # type: List[np.ndarray]
    # searching for all extreme currents
    for N1_ones_array in __unique_permutations([0]*(Nr-N1) + [1]*N1):
        # number represented by N1_ones_array
        # k = reduce( lambda acc, bit: 2*acc+bit, N1_ones_array, 0 )
        indeces = [i for (i, num) in enumerate(N1_ones_array) if num == 1]

        A1 = np.zeros((N1, Nr))
        for i in range(N1):
            A1[i, indeces[i]] = 1

        A2 = np.zeros((1, Nr))
        for colA in range(Nr):
            A2[0, colA] = 1
            A = np.concatenate((SC, A1, A2), axis=0)
            if np.linalg.matrix_rank(A) == Nr:
                Ej = np.linalg.solve(A, B)
                # print(A)
                # print()
                if np.amin(Ej) > -Tiny:
                    # print(A)
                    # print()
                    to_add = True
                    for colE in range(Nsol):
                        if (Ej - E[colE] < Tiny*np.ones((Nr, 1))).all():
                            to_add = False  # solution already in the list of solutions
                            break
                    if to_add:
                        E.append(Ej)
                        # print("K =", k, " \tSolution #", Nsol+1)
                        Nsol += 1
                break

    # Scaling each extreme current to integer numbers
    for i in range(Nsol-1):
        # first, find the smallest number in each column of E that is not zero (bigger than Tiny)
        # then, divide each column by the smallest number found
        E[i] /= min([Eij for Eij in E[i].T.tolist()[0] if Eij > Tiny])
        # TODO: dividing by the smallest doesn't necessary gives all numbers as reals
        # possible better approach, divide by all numbers smaller than one, and divide by
        # gcd of them all

    # creating np.array from list of E[i] np.arrays (of sizes (1,Nr))
    # return {
    #    'E': np.concatenate(E, axis=1).round(),
    #    'SC_singular': SC_singular
    # }
    null_array = np.array([]).reshape((Nr, 0))
    toRet = np.concatenate(E + [null_array], axis=1).round()
    # returning lexicographically sorted extreme currents
    # return toRet[:, np.lexsort(toRet)]
    return toRet

# From https://stackoverflow.com/a/30558049


def __unique_permutations(elements):
    # type: (List[int]) -> Iterable[Tuple[int,...]]
    if len(elements) == 1:
        yield (elements[0],)
    else:
        unique_elements = set(elements)
        for first_element in unique_elements:
            remaining_elements = list(elements)
            remaining_elements.remove(first_element)
            for sub_permutation in __unique_permutations(remaining_elements):
                yield (first_element,) + sub_permutation


def __rows_to_keep(A_):
    # type: (np.ndarray) -> Tuple[List[int], List[int]]
    """
    Using gaussian elimination to know which columns to keep to make Ran(A_) == A_.shape[0],
    ie. which columns to preserve to make the matrix A_ non-singular
    A_ is a np.array
    """
    n, m = A_.shape
    A = A_.copy()  # .astype( np.float64 ) # we only work with integers in this project
    indeces = np.array(range(n))

    # print(indeces)
    current_column = -1
    k = 0
    while k < n and current_column < m-1:
        current_column += 1
        maxindex = abs(A[k:, current_column]).argmax() + k
        # print("1")
        # print(A)
        if A[maxindex, current_column] == 0:  # the whole column is zero
            continue
        # Swap rows
        if maxindex != k:
            temp = A[maxindex, :].copy()
            A[maxindex, :] = A[k, :]
            A[k, :] = temp
            # print(indeces[k])
            # print(indeces[maxindex])
            indeces[k], indeces[maxindex] = indeces[maxindex], indeces[k]
            # print("2")
            # print(indeces)
            # print(A)
            # exit(0)
        # Deleting k row (dependency) from all the other below
        for row in range(k+1, n):
            # A[k,current_column] is non zero
            multiplier = A[row, current_column]/A[k, current_column]
            A[row, :]  -= (multiplier*A[k, :]).astype(np.int64)
        # print("3")
        # print(A)

        k += 1

    # print(k, current_column)

    # If the loop above finished with this condition, then some rows (n-k) are dependent of the rest
    if current_column == m-1:
        kept    = sorted(indeces[:k])
        removed = sorted(indeces[k:])
    # There is no row to remove, all rows are independent of each other
    else:
        kept    = sorted(indeces)
        removed = np.array([])

    # print(A)
    # print(indeces)

    return kept, removed  # , A
