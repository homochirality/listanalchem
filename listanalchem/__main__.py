#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2017-2018 Universidad Nacional de Colombia
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division

import sys
import argparse
import time # JAAB: To measure time taken by program execution.
from collections import namedtuple

from listanalchem.reactions_parser import ReactionsDetails, ReactionDetailsException
from listanalchem.reduce_cas import ReduceCASNotInstalledException

# from listanalchem.tools import interact # JAAB: To see how the things are working.

sStartTime = time.time() # Store starting time.

if 'typing' in sys.modules:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from typing import Union, Tuple, Any, TextIO, Callable, Dict, List, Optional  # noqa: F401

# import random
# import numpy
# random.seed(123)
# numpy.random.seed(123)

def main(config):
    # type: (Config) -> None

    print("Welcome to Listanalchem!")
    print("Remember to check the model you're loading, it must:")
    print("* Have a name `modelname`")
    print("* Have a list of species `species`. Note: the first two species will be considered"
          " enantiomers, all other species do not. This apply to analysis 1 to 5. In the"
          " frank_pseudochiral analysis (option 6), you must specify the enantiomeric species."
          " In this latter option, it is suggested to introduce the enantiomeric pairs in"
          " order [(0, 1), (2, 3), ...].")
    print("* Have a non-empty list of reactions")
    print()

    # # Sympy pretty printing, used to print results in LaTeX format
    # #from sympy import init_printing
    # #init_printing(use_latex=True)

    modelname  = config.modelname   # type: str
    species    = config.species     # type: List[str]
    reactions  = config.reactions   # type: List[str]
    dual_pairs = config.dual_pairs  # type: Optional[List[Tuple[int, int]]]

    if dual_pairs is not None and config.analyses['six-categories']['enabled']:
        print('The model has an additional parameter "dual_pairs"!')
        print('Disabling "Six Categories Analysis". This analysis may not work properly'
              ' with the dual pairs given in the model.')
        config.analyses['six-categories']['enabled'] = False

    if config.analyses['six-categories']['enabled']:
        rd = ReactionsDetails(reactions, species, sort_by='six-categories')

        reactions_six_categories = rd.get_reactions_six_categories()
        config.analyses['six-categories']['reactions-in-categories'] = reactions_six_categories
        # detecting if reactions were all of the right type (synthesis, ...) and had dual pairs
        sixCategoriesAnaly = len(reactions_six_categories['others']) == 0

        if sixCategoriesAnaly:
            print("!! Attention! The order of the entered reactions"
                  " has changed to follow the order:")
            print("!! synthesis, fo-decomposition, autocatalytic, so-decomposition,")
            print("!! no-enantioselective and inhibition")
        else:
            print("!! Attention! Six Categories Algorithm will not be"
                  " executed (look below for more info)")

        print()

    # creating matrices without six categories analysis or the list of reactions doesn't fulfill an
    # six categories chemical network
    if not config.analyses['six-categories']['enabled'] or not sixCategoriesAnaly:
        # # creating all matrices, leaving reactions list in the same order it was entered
        # #S, K, R, xs, ks = sna_matrices(reactions, species)
        rd = ReactionsDetails(reactions, species, sort_by=None, dual_pairs=dual_pairs)

    print_modelname(modelname)
    print()
    print_model_details(rd, config.output)
    output = config.output

    from listanalchem.analyses import analyses
    i = 0
    for analysis_name, analysis in analyses.items():
        i += 1
        if config.analyses[analysis_name]['enabled']:
            print(("\n" + 30*"=" + " ( - {} - ) " + 30*"=").format(i))

            config_to_pass = config.analyses[analysis_name].copy()
            analysis.run_analysis(rd, config_to_pass, modelname, output)

    print_modelname(modelname)


def print_modelname(modelname):
    # type: (str) -> None
    print("*"*(len(modelname)+10))
    print("*** ", modelname, " ***")
    print("*"*(len(modelname)+10))


def print_model_details(rd, output={'latex': False, 'no_pretty': False}):
    # type: (ReactionsDetails, Dict[str, bool]) -> None

    import pprint
    import numpy as np
    from listanalchem.mypprint import get_sympy_pprint

    sympy_pprint = get_sympy_pprint(latex=output['latex'], no_pretty=output['no_pretty'])

    S, K, R, species, reactions = rd.S, rd.K, rd.R, rd.species, rd.get_reactions()
    nS, nR = S.shape  # number of species and number of reactions

    # ### Printing model's details and matrices ###
    print("Species:")
    print(species)
    print("Reactions list:")
    pprint.pprint(reactions)

    duals, not_duals = rd.get_duals()
    print("Dual pair equations (equations are numbered from 0 to {}):".format(len(reactions)-1))
    pprint.pprint(duals)

    if not_duals:
        print("Regular reactions (not dual)")
        print(not_duals)
        print()

    print("\nStoichiometric Matrix:")
    sympy_pprint(S)
    print("\nReactions Order Matrix:")
    sympy_pprint(K)
    print("\nVelocity Function:")
    sympy_pprint(R)
    print()

    print("Differential equations functions (polynomials) vector:")
    sympy_pprint(S*R)
    print()

    if S.shape[0] != np.linalg.matrix_rank(S):
        print("Warning: Stoichiometric Matrix is SINGULAR!")
        print()

# code to use with args parser, gotten from https://stackoverflow.com/a/43357954


def str2bool(v):
    # type: (str) -> bool
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


Config = namedtuple(
    'Config',
    ['modelname', 'species', 'reactions', 'dual_pairs', 'analyses', 'output'])


class ListanalchemParser(argparse.ArgumentParser):
    def __init__(self):
        # type: () -> None
        from .tools import columns_console

        super(ListanalchemParser, self).__init__(  # type: ignore
            description='Stability analyses of chemical networks. '
                        'Several analyses focused solely in homochirality symetry breaking.',
            formatter_class=lambda prog: argparse.HelpFormatter(
                prog, max_help_position=40, width=columns_console)
        )

        self.options = {}  # type: Dict[str, Dict[str, Any]]

        self.add_argument(
            '--model',
            type=argparse.FileType('r'),
            help='Location of file holding the model of the chemical network.',
            metavar='file',
            required=True)

        from listanalchem.analyses import analyses

        for analysis_name, analysis in analyses.items():

            help_analysis = 'Enable|Disable {} Analysis (default: {})'.format(
                analysis.name, analysis.enabled)
            if analysis.additional_description:
                help_analysis += " " + analysis.additional_description

            self.add_argument(
                '--{}-analysis'.format(analysis_name),
                type=str2bool,
                # default=analysis.enabled,
                default=None,
                help=help_analysis,
                metavar='(t|f)')

            assert analysis_name not in self.options
            self.options[analysis_name] = {
                'enabled': analysis.enabled
            }

            for option, config in analysis.analysis_config.items():
                # option = option.replace('-', '_')

                assert option not in {'enabled'}, \
                    "A parameter for an analysis cannot be named `{}`".format(option)

                params = {
                    'help': '{} for {} Analysis (default: {})'.format(
                        config['description'], analysis.name, config['default']
                    )
                }  # type: Dict[str, Any]

                if config['type_input'] is bool:
                    params['type'] = str2bool
                    params['metavar'] = '(t|f)'
                else:
                    params['type'] = config['type_input']

                if 'metavar' in config:
                    params['metavar'] = config['metavar']

                self.add_argument('--{}-{}'.format(analysis_name, option), default=None, **params)

                # Adding default parameter to options
                params['default'] = config['default']
                self.options[analysis_name][option] = params

        self.add_argument(
            '--latex',
            type=str2bool,
            default=False,
            help='Enable|Disable LaTeX output of formulas and matrices (default: false).',
            metavar='(t|f)')
        self.add_argument(
            '--no-pretty',
            type=str2bool,
            default=False,
            help='Enable|Disable Pretty output (it is overrided by LaTeX output) (default: false).',
            metavar='(t|f)')

    def parse_config(self):
        # type: () -> Config
        """Parsing arguments to define the configuration of the file."""
        args = self.parse_args()
        # This defines variables modelname, species, reactions, dual_pairs, and model_analyses
        self.load_model(args.model)

        analyses = {}  # type: Dict[str, Any]

        for analysis_name, opts in self.options.items():
            opts_from_parser = {}  # type: Dict[str, Any]
            for opt, params in opts.items():
                if opt == 'enabled':
                    continue

                val = getattr(args, "{}_{}".format(
                    analysis_name.replace('-', '_'),
                    opt.replace('-', '_')))

                # Checking whether the user set the value when running listanalchem. If
                # the user didn't, we must check whether the parameter was defined in the
                # file. If the user didn't overwrite any option when calling the
                # application or in the file that defines the model, then we use the
                # default value defined by each analysis.
                if val is None:
                    if self.model_analyses is not None \
                            and analysis_name in self.model_analyses \
                            and opt in self.model_analyses[analysis_name]:
                        opts_from_parser[opt] = \
                            self.model_analyses[analysis_name][opt]
                    else:
                        opts_from_parser[opt] = params['default']
                else:
                    opts_from_parser[opt] = val

            val = getattr(args, "{}_analysis".format(analysis_name.replace('-', '_')))
            if val is None:
                if self.model_analyses is not None \
                        and analysis_name in self.model_analyses \
                        and 'enabled' in self.model_analyses[analysis_name]:
                    opts_from_parser['enabled'] = \
                        self.model_analyses[analysis_name]['enabled']
                else:
                    opts_from_parser['enabled'] = opts['enabled']
            else:
                opts_from_parser['enabled'] = val

            analyses[analysis_name] = opts_from_parser

        return Config(
            modelname=self.modelname,
            species=self.species,
            reactions=self.reactions,
            dual_pairs=self.dual_pairs,
            analyses=analyses,
            output={
                'latex': args.latex,
                'no_pretty': args.no_pretty
            }
        )

    def load_model(self, model_file):
        # type: (TextIO) -> None
        # Opening file where the model is saved
        model_str = model_file.read()

        # Loading model
        try:
            model = {}  # type: Dict[str, Any]
            exec(model_str, model)
        except Exception as e:
            print("Model file is faulty. Please check carefully the file.\n"
                  "It must follow the Python syntax.")
            print(e)
            exit(1)

        # Verifying model's consistency
        def allstr(l):
            # type: (Any) -> bool
            return all(isinstance(s, str) for s in l)
        assert "modelname" in model, "`modelname` is missing in the model file"
        assert   "species" in model, "`species` is missing in the model file"  # noqa: E271
        assert "reactions" in model, "`reactions` is missing in the model file"
        assert isinstance(model["modelname"], str), "`modelname` must be a string"
        assert isinstance(model["species"], list) and allstr(model["species"]), \
            "`species` must be a list of strings"
        assert isinstance(model["reactions"], list) and allstr(model["reactions"]), \
            "`reactions` must be a list of strings"

        # extracting model's data
        self.modelname  = model["modelname"]
        self.species    = model["species"]
        self.reactions  = model["reactions"]

        if 'dual_pairs' in model:
            self.dual_pairs = model['dual_pairs']
            # assert allstr(model["dual_pairs"]), "`dual_pairs` must be a list of pairs of integers"
        else:
            self.dual_pairs = None

        if 'analyses' in model or 'analysis' in model:
            if 'analyses' in model:
                analyses = model['analyses']
            else:
                analyses = model['analysis']

            assert isinstance(analyses, dict), \
                "The variable `analyses` must be a dictionary with all the options to run the model"
            assert allstr(analyses), \
                "All keys in the `analyses` dictionary must be strings"
            self.model_analyses = analyses  # type: Optional[Dict[str, Dict[str, Any]]]

            for analysis, opts in analyses.items():
                assert analysis in self.options, \
                    "The analysis `{}` defined in the model file does not exist"
                for opt in opts:
                    assert opt in self.options[analysis], \
                        "No option `{}` for analysis `{}` exists".format(opt, analysis)

        else:
            self.model_analyses = None


if __name__ == '__main__':
    parser = ListanalchemParser()
    config = parser.parse_config()

    try:
        main(config)
    except ReactionDetailsException as msg:
        print("Listanalchem stopped!", file=sys.stderr)
        print("The model has a problem, please check the model's file.", file=sys.stderr)
        print("  ", msg, file=sys.stderr)
        exit(1)
    except ReduceCASNotInstalledException:
        print("Reduce is not installed")
        print("Please, make sure you've installed it correctly")
    except Exception as msg:
        print("An unexpected error occurred when running an analysis", file=sys.stderr)
        print("  Please open an issue in https://gitlab.com/homochirality/listanalchem",
              file=sys.stderr)
        print("  include the model you are using that is causing trouble.", file=sys.stderr)
        print(file=sys.stderr)
        print("  The error message return by the program was:", file=sys.stderr)
        print("   ", msg, file=sys.stderr)
        print(file=sys.stderr)
        raise

sEndTime = time.time() # Store end time.
timeOfExecution = sEndTime - sStartTime # Calculate the time elapsed in seconds.
print("The total runtime of the program was ", timeOfExecution, "s")
