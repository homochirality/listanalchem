# LISTANALCHEM - LInear STability ANALysis of CHEmical Mechanisms #

Listanalchem is a software written in python to perform several linear stability analysis
of chemical models intended to produce homochirality. It includes six algorithms to analyze
chemical networks for homochirality behaviour:

1. Linear stability analysis for models with only two variables: Trace-Determinant plane.
   See references [1] and [2].
2. Stoichiometric Network Analysis (SNA) [3], using the algorithm from reference [4] and/or reference [5].
3. Reactions grouped by a particular scheme of six categories, according to reference [6].
4. Frank inequality according to reference [7]. Semialgebraic problem unsolved, i.e., unable
   to sample non-linear inequalities.
5. Frank inequality according to reference [7]. Semialgebraic problem solved (sampling
   datapoints/reaction networks from inequalities), using Clarke factorization according to
   SNA. Now the inequalities are all linear, therefore the sampling is doable.
6. Frank inequality on chemical mechanisms with two or more enantiomeric pairs,
   see reference [8].

Listanalchem is the main tool used by the research group on homochirality
(<http://ciencias.bogota.unal.edu.co/germina>) at the UNAL
(Universidad Nacional de Colombia - <http://unal.edu.co/>).

## How to run ##

### Requirements ###

- Python 2, or Python 3.
- NumPy, SymPy, and matplotlib.
- Pip.
- Additional python libs: Pyparsing, [stopit](https://pypi.python.org/pypi/stopit).
- Reduce Computer Algebra System (Necessary for one type of analysis, it isn't mandatory).

#### In debian ####

In a root session:

```ssh
apt-get install python-numpy python-sympy python-pyparsing python-pip python-matplolib gnuplot-x11
```

If you want to use python 3, change `python-numpy python-sympy python-pyparsing apt-get install python-matplotlib` for
`python3-numpy python3-sympy python3-pyparsing python3-matplotlib`.

The package `stopit` isn't yet in the official debian repositories, therefore you must install it
using `pip`. In your user session (not root) enter in console `pip install --user stopit`,
or in python3 `pip3 install --user stopit`.

To be able to use `reduce` you need to install `gnuplot-x11`, and the prepackaged reduce
executable from [here][reduce].

[reduce]: https://sourceforge.net/projects/reduce-algebra/files/snapshot_2018-01-17/linux-deb/

We recomend to download the packages:

```ssh
reduce-common_2018-01-17_all.deb
reduce-csl_2018-01-17_amd64.deb
reduce-addons_2018-01-17_amd64.deb
```

to install those packages run in the terminal:

```ssh
dpkg -i reduce-common_2018-01-17_all.deb reduce-csl_2018-01-17_amd64.deb reduce-addons_2018-01-17_amd64.deb
```

#### In Windows ####

Install python (recommended from [here][pythonexe]), then install prerequisites using pip
with:

[pythonexe]: https://www.python.org/downloads/windows/

```ssh
pip install numpy sympy pyparsing stopit matplotlib
```

or install all python packages inside an environment:

```ssh
python3 -m venv venv-env3
source venv-env3/bin/activate
pip install -r requirements.txt
```

Install reduce from [here (official download page)][reducewin]. Download the file called
`Reduce-Setup_2018-01-17.exe` and run it.

[reducewin]: https://sourceforge.net/projects/reduce-algebra/files/snapshot_2018-01-17/

### Running test ###

Open the console and navigate (`cd`) to the folder created during the decompression
process, for example /home/user/listanalchem-master, and write:

```ssh
python -m listanalchem --help
```

## Example of analyzing a model (Kondepudi-Nelson)

The Kondepudi-Nelson model, see reference [9], will be used below to illustrate a typical execution of Listanalchem.

### Structure of Chemical Mechanism Files (input files) ##

A chemical network (mecahnism) is defined in a file with python formating. Below an example:

```python
# Kondepudi, D. K., & Nelson, G. W. (1983). Chiral symmetry breaking in nonequilibrium systems. Physical Review Letters, 50(14), 1023–1026. https://doi.org/10.1103/PhysRevLett.50.1023
# Run with: python -m listanalchem --model models/Kondepudi-Nelson.py > Kondepudi-Nelson.out
# The previous command save the output file inside the folder .../listanalchem
# You can make a folder, inside .../listanalchem to save your outputs, e.g. outputs then use:
# python -m listanalchem --model models/Kondepudi-Nelson.py > outputs/Kondepudi-Nelson.out

modelname = 'Kondepudi-Nelson'
species = ['L', 'D']
reactions = [
    "      <-> L  ", # 0 & 1.
    "      <-> D  ", # 2 & 3.
    "L     <-> 2 L", # 4 & 5.
    "D     <-> 2 D", # 6 & 7.
    "L + D  ->    ", # 8.
]

analyses = {
    "trace-determinant"   : { ######### First algorithm.  #########
        "enabled"               : True,
        "2by2-jacobian"         : True,
        "num-samples"           : 10,
        "plot"                  : True,
	"time-to-show-plot"     : 0.3
    },
    "sna"                 : { ######### Second algorithm.  #########
        "enabled"               : True,
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Nelson-SNA',
    },
    "six-categories"    : { ######### Third algorithm.  #########
        "enabled"               : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Nelson-3A-6c'
    },
    "frank-ineq-nonlinear": { ######### Fourth algorithm.  #########
        "enabled"               : True
    },
    "frank-ineq-linear"   : { ######### Fifth algorithm.  #########	
        "enabled"               : True,
        "dual-pairs-in-ec"      : True,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Nelson-5A',
        "samples-for-proportion": 10000
    },
    "frank-pseudoquiral"  : { ######### Six algorithm.  #########	
        "enabled"               : True,
        "enantiomeric-pairs"    : [(0, 1)],
        "dual-pairs-in-ec"      : False,
        "instability-heuristic" : "mineurs", # or: "trace-determinant", "characteristic-polynomial",
        "sum-mineurs"           : True,
        "max-mineur-search-stop": 5,
        "simplification-tries"  : 10000,
        "num-samples"           : 10,
        "samples-folder"        : 'samples/Kondepudi-Nelson-6A',
    }
}
```

Most of the actual examples available on the models' folder are more complex than the presented above, but this is the minimal structure needed to run a model. You can open another example file and see its structure to compare and build your model, starting from that example.

### Command line to execute Listanalchem ##

You want to analyze the Kondepudi-Nelson model [9], the one presented above, and you want to run all analyses except for the
sna-analysis. Then, the command you need to execute is:

```ssh
python -m listanalchem --sna-analysis f --model models/Kondepudi-Nelson.py
```

Observe that the input file example has the parameter "enabled" with the option "True" for all six algorithms. Then when you use "--sna-analysis f" in the command line, that option for that algorithm turns to "False". The latter is a way to work, but it can be easier to modify the option directly in the input file; where, also, the user can change the other options.

Observe, also, that the Frank Inequality Analysis on chemical mechanisms with more than one enantiomeric pair (the sixth algorithm) cannot be performed together with the third algorithm (six categories). Therefore, please deactivate the third algorithm (six categories) to work with the sixth algorithm (models with more than one enantiomeric pairs).

Listanalchem comes with a folder called "models", with several of the most common models proposed to explain the origin of biological homochirality. At the start of each file, there are commented lines (usually with the reference where the model was published) and one with a particular command, similar to the previous one, to run the respective example.

## References ##

[1] Hirsch, Morris W.; Smale, Stephen; Devaney, Robert L. 2004.
'Pure and Applid Mathematics Series: Differential Equations, Dynamical Systems, and an Introduction to Chaos'. Elsevier Academic Press. Amsterdam. Pag. 63.
https://www.math.upatras.gr/~bountis/files/def-eq.pdf

[2] Gray, Peter; Scott, Stephen K. 1994.'Chemical oscillations and instabilities : non-linear chemical kinetics'. Clarendon Press. Oxford. Pag. 66.

[3] Clarke, B. L. (1988). Stoichiometric network analysis. Cell Biophysics, 12(1), 237–253.
https://doi.org/10.1007/BF02918360.

[4] Schmitz, Guy, Ljiljana Z. Kolar-Anić, Slobodan R. Anić, and Željko D. Čupić. 2008.
‘Stoichiometric Network Analysis and Associated Dimensionless Kinetic Equations.
Application to a Model of the Bray−Liebhafsky Reaction’. The Journal of Physical Chemistry
A 112 (51):13452–57. <https://doi.org/10.1021/jp8056674>.

[5] Hoops, S., Sahle, S., Gauges, R., Lee, C., Pahle, J., Simus, N., Singhal, M., Xu, L., Mendes, P., & Kummer, U. (2006). COPASI - A COmplex PAthway SImulator. Bioinformatics, 22(24), 3067–3074. https://doi.org/10.1093/bioinformatics/btl485

[6] Montoya, J. A., Mejía, C., Bourdón, R. D., Cruz, E., & Ágreda, J. (2018). On the
Stability Analysis of Chiral Networks and the Emergence of Homochirality. MATCH Commun.
Math. Comput. Chem, 80(2), 311–344.
http://match.pmf.kg.ac.rs/electronic_versions/Match80/n2/match80n2_311-344.pdf

[7] Ágreda, J., Mejía, C., & Montoya, J. A. (2018). On the linear algebra of biological
homochirality. Journal of Mathematical Chemistry, 56(6), 1782–1810.
https://doi.org/10.1007/s10910-018-0893-6.

[8] Montoya, A., Cruz, E., & Ágreda, J. (2019). Computing the Parameter Values for the Emergence of Homochirality in Complex Networks. Life, 9(3), 74.
https://doi.org/10.3390/life9030074.

[9] Kondepudi, D. K., Nelson, G. W. 1985. ‘Weak neutral currents and the origin of biomolecular chirality’.
Nature, 314: 438-441. <http://dx.doi.org/10.1038/314438a0>.

## LICENSE ##

This code is released under Apache 2.0

Copyright 2017-2021 - Universidad Nacional de Colombia.
